"""
This scripts redacts personal information (other than ORCID) from
the "YYYY_MM_DD_raw_data.csv" file output by Michael Parkin's
ORCID parser written in R.
"""

import pandas as pd

import local.config as conf


if __name__ == "__main__":
    df = pd.read_csv(conf.parkin_pseudoanonymiser_input)
    df[
        [
            "givenName",
            "familyName",
            "position",
            "email",
            "orcidGivenName",
            "orcidFamilyName",
        ]
    ] = "redacted"
    df.to_csv(conf.parkin_pseudoanonymiser_output, index=False)
