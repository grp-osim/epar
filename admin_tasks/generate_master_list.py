"""
This script compares round 1 and round 2 files in order to assemble
a master list of publications that need to be processed during round 2.

Any publications present in any of the round 1 files will be excluded from
the master list.

Data from WoS, EPMC and Converis exports will be merged to create the master
list.
"""
import numpy as np
import pandas as pd

import local.config as conf
import functools
from habanero import Crossref
from osim_utils.clients.epmc import EpmcClient

from converis.converis_parser import ConverisExportParser
from converis.fields import CONVERIS_FIELDNAMES
from dataclasses import fields
from embl_predictor.predictor_parser import EmblPredictorExportParser
from reports.entities import Publication
from utils.fieldset import StandardNames as sn
from wos.wos_fields import WOS_FIELDNAMES
from wos.wos_parser import WosExportParser


cr = Crossref(mailto=conf.user_email)
fieldnames = [f.name for f in fields(Publication)]


def get_crossref_item(response: dict) -> dict:
    """
    Returns the first item in Crossref response that is not a preprint
    Args:
        response: response body from Crossref API
    """
    items = response["message"]["items"]
    for i in items:
        if i.get("subtype") == "preprint":
            continue
        return i


def get_round_1_merged_dataframe():
    r1_cep_usecols = [
        CONVERIS_FIELDNAMES.doi,
        CONVERIS_FIELDNAMES.pubmed_id,
    ]
    r1_cep = ConverisExportParser(conf.Round1.converis_export, usecols=r1_cep_usecols)
    r1_cep.parse_df_data()
    r1_wos_usecols = [WOS_FIELDNAMES.doi, WOS_FIELDNAMES.pubmed_id]
    r1_wos_parser = WosExportParser(conf.Round1.wos_export, usecols=r1_wos_usecols)
    r1_wos_parser.parse_df_data()
    # check we have DOIs for everything:
    try:
        assert r1_cep.df.loc[r1_cep.df.doi.isna()].empty
    except AssertionError:
        print("Converis dataframe has rows without a DOI:")
        print(r1_cep.df.loc[r1_cep.df.doi.isna()])
    try:
        assert r1_wos_parser.df.loc[r1_wos_parser.df.doi.isna()].empty
    except AssertionError:
        print("WoS dataframe has rows without a DOI:")
        print(r1_cep.df.loc[r1_cep.df.doi.isna()])
    # merge round 1 dataframes on DOI
    return r1_cep.df.merge(r1_wos_parser.df, how="outer", on=sn.doi)


def get_round_2_merged_dataframe():
    r2_cep = ConverisExportParser(conf.Round2.converis_export)
    r2_cep.prune_dataframe()
    r2_cep.parse_df_data()
    try:
        assert r2_cep.df.loc[r2_cep.df.doi.isna()].empty
    except AssertionError:
        print("Converis r2 dataframe has rows without a DOI:")
        print(r2_cep.df.loc[r2_cep.df.doi.isna()])

    r2_wep = WosExportParser(conf.Round2.wos_export)
    r2_wep.prune_dataframe()
    r2_wep.parse_df_data()
    try:
        assert r2_wep.df.loc[r2_wep.df.doi.isna()].empty
    except AssertionError:
        print("WoS r2 dataframe has rows without a DOI:")
        print(r2_wep.df.loc[r2_wep.df.doi.isna()])

    r2_epp = EmblPredictorExportParser(conf.Round2.embl_predictor_xlsx)
    r2_epp.parse_df_data()
    try:
        assert r2_epp.df.loc[r2_epp.df.doi.isna()].empty
    except AssertionError:
        print("Predictor dataframe has rows without a DOI:")
        print(r2_epp.df.loc[r2_epp.df.doi.isna()])

    merged_df = functools.reduce(
        lambda l, r: pd.merge(l, r, how="outer", on=sn.doi),
        [r2_cep.df, r2_wep.df, r2_epp.df],
    )

    return merged_df.loc[~merged_df.doi.duplicated()]


if __name__ == "__main__":

    def resolve_pubmed(row):
        if pd.isna(row.pubmed_id):
            if epmc_result := doi_dict.get(row[sn.doi]):
                return epmc_result.get("pmid", np.nan)
            else:
                return np.nan
        return row.pubmed_id

    r1_df = get_round_1_merged_dataframe()
    r1_df.to_csv("r1_df_debug.csv")

    r2_df = get_round_2_merged_dataframe()
    r2_df.to_csv("r2_df_debug.csv")

    r2_only = r2_df[~r2_df.doi.isin(r1_df.doi)]

    # combine pubmed id columns data in one
    r2_only.loc[r2_only.pubmed_id.isnull(), sn.pubmed_id] = r2_only["pubmed_id_x"]
    r2_only.loc[r2_only.pubmed_id.isnull(), sn.pubmed_id] = r2_only["pubmed_id_y"]
    r2_only.to_csv("r2_only_df_debug_00.csv")

    # get remaining missing pubmed id values from EPMC
    dois = r2_only.loc[r2_only.pubmed_id.isnull()].doi
    epmc_client = EpmcClient()
    doi_dict = epmc_client.query_doi_list(doi_list=list(dois))

    r2_only.pubmed_id = r2_only.apply(resolve_pubmed, axis=1)
    r2_only.to_csv("r2_only_df_debug_01.csv")

    r2_only.to_csv("r2_only_df_debug.csv")
    r2_only.to_csv(
        "r2_masterlist.csv",
        # columns=[sn.pubmed_id, sn.doi]
    )
    r2_only_pubmed = r2_only.loc[~r2_only.pubmed_id.isnull()]
    r2_only_pubmed.to_csv(
        "r2_masterlist_pubmed.csv",
        columns=[sn.pubmed_id],
        index=False,
    )
    r2_only_no_pubmed = r2_only.loc[r2_only.pubmed_id.isnull()]
    r2_only_no_pubmed.to_csv(
        "r2_masterlist_no_pubmed.csv",
        columns=[sn.doi],
        index=False,
    )
