"""
This script:
    - parses a WoS export file to detect collaboration countries in the addresses column
    - loads data from an output file from the Freya ROR predictor
        (https://gitlab.ebi.ac.uk/literature-services/public-projects/ROR-proto-EMBL)
    - compares the two dataframes above to export:
        - a list of publications where there is agreement between the datasets
        - a list of publications that will need to be manually curated because there
            are discrepancies between the two input datasets
"""
import numpy as np
import pandas as pd
from pycountry import countries

import local.config as conf
from freya_ror_predictor.freya_ror_parser import FreyaRorPredictorExportParser
from utils.countries import CountryDetector
from wos.wos_parser import WosExportParser
from reports.constants import member_state_names, member_state_codes


def get_collaborations(row) -> tuple:
    """
    Sorts countries in row["wos_collaboration_countries"] into EMBL member and non-member estates
    """
    countries_str = row["wos_collaboration_countries"]
    member_state_collab = 0
    non_member_state_collab = 0
    member_state_collab_countries = str()
    if (countries_str is not np.nan) and (countries_str is not None):
        country_names = countries_str.split(";")
        member_countries = set()
        non_member_countries = set()
        for c in country_names:
            code = countries.get(
                name=c
            ).alpha_2  # check that name is recognised by pycountry
            if code in member_state_codes:
                member_countries.add(c)
            else:
                non_member_countries.add(c)
        if member_countries:
            member_state_collab = 1
        if non_member_countries:
            non_member_state_collab = 1
        member_state_collab_countries = ";".join(sorted(member_countries))
    return (
        member_state_collab,
        non_member_state_collab,
        member_state_collab_countries,
    )


def get_member_state_countries(countries_str):
    if (countries_str is np.nan) or (countries_str is None):
        return ""
    country_names = countries_str.split(";")
    member_countries = list()
    for c in country_names:
        if c in member_state_names:
            member_countries.append(c)
    return ";".join(member_countries)


def wos_freya_match(row):
    for wos_field, freya_field in [
        ("wos_member_state_collab_countries", "freya_collaboration_countries"),
        ("wos_member_state_collab", "Collaboration with member state"),
        ("wos_non_member_state_collab", "Collaboration with non-member state"),
    ]:
        wos = row[wos_field]
        freya = row[freya_field]
        if type(wos) != type(freya):
            try:
                wos = type(freya)(wos)
            except ValueError:
                return False
        if wos != freya:
            return False
    return True


def wos_freya_mismatches(row):
    mismatches = list()
    for wos_field, freya_field, mismatch_name in [
        (
            "wos_member_state_collab_countries",
            "freya_collaboration_countries",
            "countries",
        ),
        ("wos_member_state_collab", "Collaboration with member state", "member_bool"),
        (
            "wos_non_member_state_collab",
            "Collaboration with non-member state",
            "non-member bool",
        ),
    ]:
        wos = row[wos_field]
        freya = row[freya_field]
        if type(wos) != type(freya):
            try:
                wos = type(freya)(wos)
            except ValueError:
                mismatches.append("missing_data")
        if wos != freya:
            mismatches.append(mismatch_name)
    return ";".join(mismatches)


def make_excel_link(url):
    return f'=HYPERLINK("{url}", "{url}")'


def fill_in_wos_data(row):
    if row["_merge"] == "right_only":
        try:
            row["Date (month)"] = row.publication_date.month
        except AttributeError:
            row["Date (month)"] = np.nan
        try:
            row["Date (year)"] = row.publication_date.year
        except AttributeError:
            row["Date (year)"] = np.nan
        row["EMBL author(s) name"] = WosExportParser.get_embl_authors(row.addresses)
        if (row["wos_non_member_state_collab"] == 0) and (
            row["wos_member_state_collab"] == 0
        ):
            row["EMBL only publication"] = 1
        else:
            row["EMBL only publication"] = 0
        for t in [
            ("DOI", "doi"),
            ("PubMed ID", "pubmed_id"),
            ("Paper title", "title"),
            ("Full journal name", "journal_name"),
            ("Collaboration with member state", "wos_member_state_collab"),
            ("Collaboration with non-member state", "wos_non_member_state_collab"),
        ]:
            row[t[0]] = row[t[1]]

        row["Clickable link to the paper"] = f"https://doi.org/{row['doi']}"

        if row["wos_member_state_collab_countries"].strip():
            try:
                member_state_collab = row["wos_member_state_collab_countries"].split(
                    ";"
                )
            except AttributeError:
                member_state_collab = list()
        else:
            member_state_collab = list()
        row["Total Member State Countries"] = len(member_state_collab)

        try:
            collab_countries = row["wos_collaboration_countries"].split(";")
        except AttributeError:
            collab_countries = list()

        for c in [
            "Australia",
            "Austria",
            "Belgium",
            "Croatia",
            "Denmark",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Hungary",
            "Iceland",
            "Ireland",
            "Israel",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Montenegro",
            "Netherlands",
            "Norway",
            "Poland",
            "Portugal",
            "Slovakia",
            "Spain",
            "Sweden",
            "Switzerland",
            "United Kingdom",
        ]:
            if c in collab_countries:
                row[c] = 1
            else:
                row[c] = 0
        if "Czechia" in collab_countries:
            row["Czech Republic"] = 1
        else:
            row["Czech Republic"] = 0
    return row


def get_non_null_value(row, field):
    fields = [field, f"{field}_x", f"{field}_y", f"{field}_master"]
    for f in fields:
        try:
            if (value := row[f]) is not np.nan:
                return value
        except KeyError:
            continue
    return np.nan


def fill_in_masterlist_data(row):
    if row["_merge_with_master"] == "right_only":
        doi = row["doi_master"]
        row["DOI"] = doi
        row["Clickable link to the paper"] = f"https://doi.org/{doi}"
        row["PubMed ID"] = get_non_null_value(row, "pubmed_id")
        row["Date (year)"] = get_non_null_value(row, "year")
        row["EMBL author(s) name"] = row["embl_authors"]
        row["Paper title"] = get_non_null_value(row, "title")
        row["Full journal name"] = get_non_null_value(row, "journal_name")
        row["License"] = row["licence"]
    return row


if __name__ == "__main__":
    # parse WoS export and output csv for debugging
    wep = WosExportParser(conf.Round2.wos_addresses_xls)
    wep.check_for_duplicates()
    wep.parse_df_data()

    wep.df.publication_date = wep.df.apply(wep.get_publication_date, axis=1)
    cd = CountryDetector()
    wep.df["wos_collaboration_countries"] = wep.df.addresses.apply(
        wep.parse_affiliations, country_detector=cd
    )
    wep.df[
        [
            "wos_member_state_collab",
            "wos_non_member_state_collab",
            "wos_member_state_collab_countries",
        ]
    ] = wep.df.apply(get_collaborations, axis=1, result_type="expand")

    wep.df.to_csv(conf.Round2.wos_addresses_csv_output)

    # load Freya ROR predictor data
    fep = FreyaRorPredictorExportParser(
        conf.Round2.freya_ror_predictor_xlsx,
        # dtype=str
    )
    freya_columns = fep.df.columns
    fep.df["freya_collaboration_countries"] = fep.df.apply(
        fep.get_countries_of_collaboration, axis=1
    )
    # merge dfs on doi
    df = pd.merge(
        fep.df, wep.df, left_on="DOI", right_on="doi", how="outer", indicator=True
    )

    # find and output mismatches between the two datasets
    df["wos_freya_match"] = df.apply(wos_freya_match, axis=1)
    df["wos_freya_mismatches"] = df.apply(wos_freya_mismatches, axis=1)
    # populate freya columns with wos data for anything on wos dataset only
    df = df.apply(fill_in_wos_data, axis=1)
    df["Clickable link to the paper"] = df["Clickable link to the paper"].apply(
        make_excel_link
    )

    # add to report any publication from masterlist that is not represented in the merged dataset
    masterlist_df = pd.read_csv(conf.Round2.masterlist)
    df = pd.merge(
        df,
        masterlist_df,
        left_on="DOI",
        right_on="doi",
        how="outer",
        indicator="_merge_with_master",
        suffixes=("_report", "_master"),
    )
    df = df.apply(fill_in_masterlist_data, axis=1)

    # split publications that require manual curation from those that do not
    df.wos_freya_match.fillna(False, inplace=True)
    curation_df = df[df.wos_freya_match == False]  # includes null
    ok_df = df[df.wos_freya_match == True]

    # output to separate worksheets
    out_columns = [
        "DOI",
        "PubMed ID",
        "Date (month)",
        "Date (year)",
        "EMBL author(s) name",
        "Paper title",
        "Full journal name",
        "License",
        "In EPMC?",
        "Short reference",
        "Keywords",
        "Collaboration with member state",
        "Collaboration with non-member state",
        "EMBL only publication",
        "Clickable link to the paper",
        "Australia",
        "Institute",
        "Austria",
        "Institute.1",
        "Belgium",
        "Institute.2",
        "Croatia",
        "Institute.3",
        "Czech Republic",
        "Institute.4",
        "Denmark",
        "Institute.5",
        "Estonia",
        "Institute.6",
        "Finland",
        "Institute.7",
        "France",
        "Institute.8",
        "Germany",
        "Institute.9",
        "Greece",
        "Institute.10",
        "Hungary",
        "Institute.11",
        "Iceland",
        "Institute.12",
        "Ireland",
        "Institute.13",
        "Israel",
        "Institute.14",
        "Italy",
        "Institute.15",
        "Latvia",
        "Institute.16",
        "Lithuania",
        "Institute.17",
        "Luxembourg",
        "Institute.18",
        "Malta",
        "Institute.19",
        "Montenegro",
        "Institute.20",
        "Netherlands",
        "Institute.21",
        "Norway",
        "Institute.22",
        "Poland",
        "Institute.23",
        "Portugal",
        "Institute.24",
        "Slovakia",
        "Institute.25",
        "Spain",
        "Institute.26",
        "Sweden",
        "Institute.27",
        "Switzerland",
        "Institute.28",
        "United Kingdom",
        "Institute.29",
        "Total Member State Countries",
        "Total Member state institutes",
        "Abstract",
        "freya_collaboration_countries",
        "wos_member_state_collab_countries",
        "wos_collaboration_countries",
        "wos_member_state_collab",
        "wos_non_member_state_collab",
        "wos_freya_mismatches",
        "wos_freya_match",
        "_merge",
    ]
    with pd.ExcelWriter(conf.Round2.wos_freya_comparison_output) as writer:
        curation_df.to_excel(
            writer, sheet_name="Curation", columns=out_columns, index=False
        )
        ok_df.to_excel(writer, sheet_name="Verified", columns=out_columns, index=False)