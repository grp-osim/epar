import test_aux.test_data.dataset_0 as td_0
from test_aux.test_data.annual_report_0 import AnnualReport2022


def test_get_europepmc_data(ar_from_converis_export_0):
    ar = ar_from_converis_export_0
    ar.get_europepmc_data()
    # len of expected_data not 10 because not all rows return a EPMC result
    # these fields are unlikely to change in the API response
    constant_fields = ["id", "doi", "firstIndexDate"]
    for f in constant_fields:
        expected_values = [x[f] for x in td_0.EpmcData0.expected_data]
        actual_values = [x[f] for x in ar.epmc_data]
        assert sorted(actual_values) == sorted(expected_values)


def test_masterlist_coverage_ok(annual_report_0):
    ar = annual_report_0
    masterlist = ar.assemble_masterlist(
        ignore_openalex_cache=False,
        ignore_warehouse_cache=False,
    )
    masterlist.to_csv("test_results.csv")
    masterlist_dois = set(masterlist.doi)
    report22_dois = set(AnnualReport2022.dois)
    assert report22_dois.issubset(masterlist_dois)
