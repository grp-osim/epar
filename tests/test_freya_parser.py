from freya_ror_predictor.freya_fields import FREYA_FIELDNAMES


column_names = list()
for k, v in FREYA_FIELDNAMES.__dict__.items():
    if v:
        column_names.append(k)


def test_parser_init_ok(freya_parser_0):
    fep = freya_parser_0
    assert fep.df.shape == (443, len(column_names))
    assert sorted(fep.df.columns.values.tolist()) == sorted(column_names)
