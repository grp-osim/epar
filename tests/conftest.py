import pytest

from test_aux.testing_utils import WOS_DATASET_0
from reports.annual import AnnualReport
from reports.authors import AuthorsReport
from test_aux.testing_utils import (
    CONVERIS_DATASET_0,
    FREYA_DATASET_0,
    OASWITCHBOARD_DATASET_0,
    PARKIN_DATASET_0_RAW,
    PARKIN_DATASET_0_PUBS,
    WOS_DATASET_0,
    WOS_DATASET_1,
)
from utils.countries import CountryDetector
from utils.fieldset import StandardNames
from wos.wos_parser import WosExportParser


cd = "test_aux.conftests"
pytest_plugins = [
    f"{cd}.converis_export_parser",
    f"{cd}.freya_export_parser",
    f"{cd}.data_container",
    f"{cd}.data_warehouse_parser",
    f"{cd}.facilities_report",
    f"{cd}.oaswitchboard_parser",
    f"{cd}.open_alex_parser",
    f"{cd}.parkin_parser",
]


# region countries
@pytest.fixture(scope="module")
def country_detector_0():
    return CountryDetector()


@pytest.fixture(scope="module")
def country_detector_1():
    return CountryDetector(use_abbreviations=False)


@pytest.fixture(scope="module")
def country_detector_2():
    return CountryDetector(use_extras=False)


# endregion

# region AnnualReport
@pytest.fixture()
def authors_report_0():
    return AuthorsReport(
        [
            {StandardNames.first_name: "Julia", StandardNames.last_name: "Jones"},
            {StandardNames.first_name: "John", StandardNames.last_name: "Smith"},
            {
                StandardNames.first_name: "Sarah",
                StandardNames.last_name: "Johnson",
            },
        ],
        orcid_raw_input_file=PARKIN_DATASET_0_RAW,
        pubs_file_path=PARKIN_DATASET_0_PUBS,
    )


# endregion


# region AnnualReport
@pytest.fixture()
def annual_report_0():
    return AnnualReport(
        report_year=2022,
        converis_input_file=CONVERIS_DATASET_0,
        freya_input_file=FREYA_DATASET_0,
        oaswitchboard_input_file=OASWITCHBOARD_DATASET_0,
        orcid_raw_input_file=PARKIN_DATASET_0_RAW,
        orcid_pubs_file_path=PARKIN_DATASET_0_PUBS,
        wos_input_file=WOS_DATASET_1,
    )


@pytest.fixture(scope="module")
def ar_from_converis_export_0(module_parser_0):
    """
    Annual report containing 10 publications only
    """
    cep = module_parser_0
    ar = AnnualReport(publications=cep.df)
    return ar


@pytest.fixture()
def ar_from_converis_export_1(converis_parser_1):
    """
    Annual report containing all publications in TEST_DATASET_0,
    including preprints, corrections, etc
    """
    cep = converis_parser_1
    ar = AnnualReport(publications=cep.df)
    return ar


@pytest.fixture()
def ar_from_converis_export_2(converis_parser_2):
    """
    Annual report containing publications in TEST_DATASET_0,
    except for preprints, corrections, etc
    """
    cep = converis_parser_2
    ar = AnnualReport(publications=cep.df)
    return ar


# endregion


# region OpenAlex
# @pytest.fixture(scope="module")
# def open_alex_parser_0(ar_from_converis_export_0):
#     """
#     OpenAlex parser for publication data pertaining to the
#     first row of TEST_DATASET_0
#     """
#     ar = ar_from_converis_export_0
#     return OpenAlexParser(publication=ar.df[0], openalex_result=OpenAlexResult0.result)
#
#
# @pytest.fixture(scope="module")
# def open_alex_parser_1(ar_from_converis_export_0):
#     """
#     OpenAlex parser for publication data pertaining to the
#     second row of TEST_DATASET_0
#     """
#     ar = ar_from_converis_export_0
#     return OpenAlexParser(publication=ar.df[1], openalex_result=OpenAlexResult1.result)
#
#
# @pytest.fixture(scope="module")
# def open_alex_parser_2(ar_from_converis_export_0):
#     """
#     OpenAlex parser for publication data pertaining to the
#     third row of TEST_DATASET_0
#     """
#     ar = ar_from_converis_export_0
#     return OpenAlexParser(publication=ar.df[2], openalex_result=OpenAlexResult2.result)
#
#
# @pytest.fixture()
# def open_alex_parser_3(ar_from_converis_export_2):
#     """
#     OpenAlex parser for publication data pertaining to row 298 (DOI 10.1136/gutjnl-2021-324755)
#     of TEST_DATASET_0
#     """
#     ar = ar_from_converis_export_2
#     return OpenAlexParser(
#         publication=ar.get_publication_by_doi("10.1136/gutjnl-2021-324755"),
#         openalex_result=OpenAlexResult3.result,
#     )


# endregion


# region WoS export parser
@pytest.fixture()
def wos_parser_0():
    """
    Default parser for WOS_DATASET_0
    """
    wep = WosExportParser(input_file_path=WOS_DATASET_0)
    wep.df.drop(columns=["provenance"], inplace=True)
    return wep


# endregion
