def test_get_authors_orcids_ok(authors_report_0):
    ar = authors_report_0
    ar.get_authors_orcids()
    assert ar.authors == [
        {"first_name": "Julia", "last_name": "Jones", "orcid": "1111-2345-6789-0000"},
        {"first_name": "John", "last_name": "Smith", "orcid": "2222-3456-7890-1111"},
        {"first_name": "Sarah", "last_name": "Johnson", "orcid": "3333-4567-8901-2222"},
    ]
