import json
from test_aux.test_data.openalex_results_0 import OpenAlexSeries0
from copy import deepcopy


def test_init_from_dataframe(alex_parser_1):
    ap = alex_parser_1
    assert "https://openalex.org/W4313546279" in list(ap.df.open_alex_id)


def test_parse_authors(alex_parser_0):
    ap = alex_parser_0
    df = ap.parse_authors(OpenAlexSeries0.series)
    actual = df.to_dict()
    # deserialise institutions dataframes
    for k, v in actual["institutions"].items():
        actual["institutions"][k] = v.to_dict()

    # assert actual == OpenAlexSeries0.as_dict
    # no idea why the simpler assertion above fails, but it does
    assert json.dumps(actual, sort_keys=True, indent=2) == json.dumps(
        OpenAlexSeries0.as_dict, sort_keys=True, indent=2
    )


# def test_validate_data_strings(open_alex_parser_0):
#     oap = open_alex_parser_0
#     assert oap._validate_data("STRING1", "string1", "error message") is None
#
#
# def test_validate_data_none(open_alex_parser_0):
#     oap = open_alex_parser_0
#     assert oap._validate_data(None, "string1", "error message") is None
#
#
# def test_validate_data_list(open_alex_parser_0):
#     oap = open_alex_parser_0
#     assert (
#         oap._validate_data("string1", ["string1", "string2"], "error message") is None
#     )
#
#
# def test_validate_data_invalid(open_alex_parser_0):
#     oap = open_alex_parser_0
#     with pytest.raises(DataValidationError):
#         oap._validate_data("string2", "string1", "error message")
#
#
# def test_validate_data_case_sensitive(open_alex_parser_0):
#     oap = open_alex_parser_0
#     with pytest.raises(DataValidationError):
#         oap._validate_data(
#             "String1", "string1", "error message", case_insensitive=False
#         )
#
#
# def test_validate_data_case_none_not_ignored(open_alex_parser_0):
#     oap = open_alex_parser_0
#     with pytest.raises(DataValidationError):
#         oap._validate_data(None, "string1", "error message", ignore_none=False)
#
#
# def test_check_and_set_entity_attribute_pub_value_none(open_alex_parser_1):
#     oap = open_alex_parser_1
#     alex_id = "https://openalex.org/W4296983648"
#     oap._check_and_set_entity_attribute(oap.pub, "open_alex_id", alex_id)
#     assert oap.pub.open_alex_id == alex_id
#
#
# def test_check_and_set_entity_attribute_pub_value_exists(open_alex_parser_2):
#     oap = open_alex_parser_2
#     oap._check_and_set_entity_attribute(
#         oap.pub,
#         "licence",
#         LicenceValidator.get_licence_name(oap.alex["host_venue"]["license"]),
#     )
#     assert oap.pub.licence == "CC BY"
#
#
# def test_check_and_set_entity_attribute_validation_fail(open_alex_parser_2):
#     oap = open_alex_parser_2
#     with pytest.raises(DataValidationError):
#         oap._check_and_set_entity_attribute(
#             oap.pub,
#             "licence",
#             oap.alex["host_venue"]["license"],
#         )
#
#
# def test_check_and_set_entity_attribute_custom_validation(open_alex_parser_2):
#     oap = open_alex_parser_2
#     oap._check_and_set_entity_attribute(
#         oap.pub,
#         "title",
#         oap.alex["title"],
#         check_func=oap._validate_similar_strings,
#     )
#
#
# def test_parse_authorships(open_alex_parser_3):
#     oap = open_alex_parser_3
#     oap.parse_authorships()
#     assert oap.pub.doi == "10.1136/gutjnl-2021-324755"
#     assert oap.pub.has_member_state_collaboration is True
#     assert oap.pub.member_state_collaborations == {
#         "DE": {
#             "https://ror.org/045f0ws19": "Universities of Giessen and Marburg Lung Center",
#             "https://ror.org/04cvxnb49": "Goethe University Frankfurt",
#             "https://ror.org/05bx21r34": "Frankfurt Cancer Institute",
#             "https://ror.org/00f7hpc57": "University of Erlangen-Nuremberg",
#             "https://ror.org/00r1edq15": "University of Greifswald",
#         },
#         "ES": {
#             "https://ror.org/00bvhmc43": "Spanish National Cancer Research Centre",
#             "https://ror.org/04hya7017": "Centro de Investigación Biomédica en Red de Cáncer",
#             "https://ror.org/04pmn0e78": "University of Alcalá",
#             "https://ror.org/050eq1942": "Hospital Universitario Ramón y Cajal",
#             "https://ror.org/03cn6tr16": "Centro de Investigación Biomédica en Red de Enfermedades Hepáticas y Digestivas",
#             "https://ror.org/01d5vx451": "Vall d'Hebron Institut de Recerca",
#             "https://ror.org/052g8jq94": "Autonomous University of Barcelona",
#             "https://ror.org/00xvxvn83": "European Foundation for the Study of Chronic Liver Failure",
#             "https://ror.org/04n0g0b29": "Pompeu Fabra University",
#         },
#     }
#
#
# def test_parse_result(open_alex_parser_2):
#     oap = open_alex_parser_2
#     oap.parse_result()
#     assert oap.pub.date == date(2022, 11, 1)
#     assert oap.pub.embl_sites == {"EMBL-EBI"}
#     assert oap.pub.has_member_state_collaboration is True
#     assert oap.pub.member_state_collaborations == {
#         "BE": {"https://ror.org/00afp2z80": "University of Liège"},
#         "CZ": {"https://ror.org/024d6js02": "Charles University"},
#         "GB": {
#             "https://ror.org/018cxtf62": "Earlham Institute",
#             "https://ror.org/01ee9ar58": "University of Nottingham",
#             "https://ror.org/026k5mg93": "University of East Anglia",
#             "https://ror.org/027m9bs27": "University of Manchester",
#             "https://ror.org/03h2bxq36": "University of Dundee",
#             "https://ror.org/05t1h8f27": "University of Huddersfield",
#         },
#         "IT": {"https://ror.org/025602r80": "Sant'Anna School of Advanced Studies"},
#         "PL": {"https://ror.org/039bjqg32": "University of Warsaw"},
#         "SK": {"https://ror.org/01c7rrt10": "Institute of Parasitology"},
#     }
#     assert oap.pub.open_alex_id == "https://openalex.org/W4309664802"
#     assert oap.pub.publisher == "The Company of Biologists"
