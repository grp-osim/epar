import numpy as np

from utils.countries import CountryDetector
from test_aux.test_data.countries_0 import DetectorInput0
from wos.wos_fields import WOS_FIELDNAMES


column_names = list()
for k, v in WOS_FIELDNAMES.__dict__.items():
    if v:
        column_names.append(k)


def test_parser_init_ok(wos_parser_0):
    wep = wos_parser_0
    assert wep.df.shape == (845, len(column_names))
    assert sorted(wep.df.columns.values.tolist()) == sorted(column_names)


def test_parse_affiliations_0(wos_parser_0):
    wep = wos_parser_0
    assert wep.parse_affiliations(wep.df.iloc[703]) == (
        {
            "DE": ["Univ Greifswald"],
            "LU": [
                "Ctr Hosp Luxembourg",
                "Luxembourg Ctr Syst Biomed",
                "Univ Luxembourg",
            ],
            "NL": ["Univ Amsterdam"],
            "US": ["MIT & Harvard"],
        },
        "high",
        np.nan,
    )


def test_parse_affiliations_1(wos_parser_0):
    wep = wos_parser_0
    assert wep.parse_affiliations(wep.df.iloc[504]) == (
        {
            "CH": ["Swiss Inst Bioinformat", "Univ Lausanne"],
            "FR": ["Univ Montpellier"],
            "GA": ["CIRMF"],
            "GB": ["Wellcome Sanger Inst"],
        },
        "high",
        np.nan,
    )
