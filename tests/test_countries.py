import pytest
from country_list import countries_for_language

from test_aux.test_data.countries_0 import DetectorInput0
from utils.countries import CountryDetector


def test_country_detector_min_init_ok():
    cd = CountryDetector(use_extras=False, use_abbreviations=False)
    en_countries = countries_for_language("en")
    assert sorted(list(cd.name2alpha2.keys())) == sorted(
        [x[1].lower() for x in en_countries]
    )
    assert cd.country_pattern == (
        "\\b(Afghanistan|Åland Islands|Albania|Algeria|American "
        "Samoa|Andorra|Angola|Anguilla|Antarctica|Antigua & "
        "Barbuda|Argentina|Armenia|Aruba|Australia|Austria|Azerbaijan|"
        "Bahamas|Bahrain|Bangladesh|Barbados|Belarus|Belgium|Belize|Benin|Bermuda|Bhutan|Bolivia|Bosnia "
        "& Herzegovina|Botswana|Bouvet Island|Brazil|British Indian Ocean "
        "Territory|British Virgin Islands|Brunei|Bulgaria|Burkina "
        "Faso|Burundi|Cambodia|Cameroon|Canada|Cape Verde|Caribbean "
        "Netherlands|Cayman Islands|Central African "
        "Republic|Chad|Chile|China|Christmas Island|Cocos \\(Keeling\\) "
        "Islands|Colombia|Comoros|Congo - Brazzaville|Congo - Kinshasa|Cook "
        "Islands|Costa Rica|Côte "
        "d’Ivoire|Croatia|Cuba|Curaçao|Cyprus|Czechia|Denmark|Djibouti|Dominica|Dominican "
        "Republic|Ecuador|Egypt|El Salvador|Equatorial "
        "Guinea|Eritrea|Estonia|Eswatini|Ethiopia|Falkland Islands|Faroe "
        "Islands|Fiji|Finland|France|French Guiana|French Polynesia|French Southern "
        "Territories|Gabon|Gambia|Georgia|Germany|Ghana|Gibraltar|Greece|"
        "Greenland|Grenada|Guadeloupe|Guam|Guatemala|Guernsey|Guinea|Guinea-Bissau|Guyana|Haiti|Heard "
        "& McDonald Islands|Honduras|Hong Kong SAR "
        "China|Hungary|Iceland|India|Indonesia|Iran|Iraq|Ireland|Isle of "
        "Man|Israel|Italy|Jamaica|Japan|Jersey|Jordan|Kazakhstan|Kenya|Kiribati|Kuwait|"
        "Kyrgyzstan|Laos|Latvia|Lebanon|Lesotho|Liberia|Libya|Liechtenstein|Lithuania|Luxembourg|Macao "
        "SAR China|Madagascar|Malawi|Malaysia|Maldives|Mali|Malta|Marshall "
        "Islands|Martinique|Mauritania|Mauritius|Mayotte|Mexico|Micronesia|Moldova|"
        "Monaco|Mongolia|Montenegro|Montserrat|Morocco|Mozambique|Myanmar "
        "\\(Burma\\)|Namibia|Nauru|Nepal|Netherlands|New Caledonia|New "
        "Zealand|Nicaragua|Niger|Nigeria|Niue|Norfolk Island|North Korea|North "
        "Macedonia|Northern Mariana Islands|Norway|Oman|Pakistan|Palau|Palestinian "
        "Territories|Panama|Papua New Guinea|Paraguay|Peru|Philippines|Pitcairn "
        "Islands|Poland|Portugal|Puerto "
        "Rico|Qatar|Réunion|Romania|Russia|Rwanda|Samoa|San Marino|São Tomé & "
        "Príncipe|Saudi Arabia|Senegal|Serbia|Seychelles|Sierra Leone|Singapore|Sint "
        "Maarten|Slovakia|Slovenia|Solomon Islands|Somalia|South Africa|South Georgia "
        "& South Sandwich Islands|South Korea|South Sudan|Spain|Sri Lanka|St. "
        "Barthélemy|St. Helena|St. Kitts & Nevis|St. Lucia|St. Martin|St. Pierre & "
        "Miquelon|St. Vincent & Grenadines|Sudan|Suriname|Svalbard & Jan "
        "Mayen|Sweden|Switzerland|Syria|Taiwan|Tajikistan|Tanzania|Thailand|Timor-Leste|"
        "Togo|Tokelau|Tonga|Trinidad "
        "& Tobago|Tunisia|Turkey|Turkmenistan|Turks & Caicos Islands|Tuvalu|U.S. "
        "Outlying Islands|U.S. Virgin Islands|Uganda|Ukraine|United Arab "
        "Emirates|United Kingdom|United States|Uruguay|Uzbekistan|Vanuatu|Vatican "
        "City|Venezuela|Vietnam|Wallis & Futuna|Western "
        "Sahara|Yemen|Zambia|Zimbabwe)\\b"
    )


def test_country_detector_init_with_abbrevs_ok():
    cd = CountryDetector(use_extras=False, use_abbreviations=True)
    en_countries = countries_for_language("en")
    expected_keys = [x[1].lower() for x in en_countries] + [
        x.lower() for x in cd.case_sensitive_map.keys()
    ]
    assert sorted(list(cd.name2alpha2.keys())) == sorted(expected_keys)
    assert "|uk|" not in cd.country_pattern.lower()
    assert "|US|" in cd.abbrev_pattern


def test_country_detector_init_with_extras_and_abbrevs_ok():
    cd = CountryDetector()
    en_countries = countries_for_language("en")
    expected_keys = (
        [x[1].lower() for x in en_countries]
        + [x.lower() for x in cd.case_sensitive_map.keys()]
        + [x.lookup_name.lower() for x in cd.special_cases]
        + [x.lower() for x in cd.extra_name2alpha2.keys()]
    )
    assert sorted(list(cd.name2alpha2.keys())) == sorted(expected_keys)
    assert "|uk|" not in cd.country_pattern.lower()
    assert "|US|" in cd.abbrev_pattern


def test_detect_countries(country_detector_0):
    cd = country_detector_0
    expected_countries = ["Belarus", "Russian Federation", "Japan", "Germany"]
    countries = cd.detect_countries(DetectorInput0.text_0)
    assert sorted(list(countries)) == sorted(expected_countries)


def test_detect_usa(country_detector_0):
    cd = country_detector_0
    expected_countries = [
        "Canada",
        "United States",
        "South Africa",
        "United Kingdom",
        "Spain",
    ]
    countries = cd.detect_countries(DetectorInput0.text_1)
    assert sorted(list(countries)) == sorted(expected_countries)


def test_detect_usa_non_abbrev_fails(country_detector_1):
    cd = country_detector_1
    expected_countries = [
        "Canada",
        "South Africa",
        "United Kingdom",
        "Spain",
    ]
    countries = cd.detect_countries(DetectorInput0.text_1)
    assert sorted(list(countries)) == sorted(expected_countries)


def test_detect_uk_non_extras_fails(country_detector_2):
    cd = country_detector_2
    expected_countries = [
        "Canada",
        "United States",
        "South Africa",
        "Spain",
    ]
    countries = cd.detect_countries(DetectorInput0.text_1)
    assert sorted(list(countries)) == sorted(expected_countries)


def test_abbrev_detection_case_sensitive(country_detector_0):
    cd = country_detector_0
    expected_countries = [
        "Canada",
        "South Africa",
        "United Kingdom",
        "Spain",
    ]
    countries = cd.detect_countries(DetectorInput0.text_2)
    assert sorted(list(countries)) == sorted(expected_countries)


def test_expected_results_number_ok(country_detector_0):
    cd = country_detector_0
    expected_countries = ["Belarus", "Russian Federation", "Japan", "Germany"]
    countries = cd.detect_countries(
        DetectorInput0.text_0, expected_results_n=len(expected_countries)
    )
    assert sorted(list(countries)) == sorted(expected_countries)


def test_expected_results_number_fail(country_detector_0):
    cd = country_detector_0
    expected_countries = ["Belarus", "Russian Federation", "Japan", "Germany"]
    with pytest.raises(AssertionError):
        cd.detect_countries(
            DetectorInput0.text_0, expected_results_n=len(expected_countries) + 1
        )


def test_expected_match_number_ok(country_detector_0):
    cd = country_detector_0
    expected_countries = ["United Kingdom"]
    expected_matches_n = 5
    countries = cd.detect_countries(
        DetectorInput0.text_3, expected_match_n=expected_matches_n
    )
    assert sorted(list(countries)) == sorted(expected_countries)


def test_expected_match_number_fail(country_detector_0):
    cd = country_detector_0
    expected_matches_n = 5
    with pytest.raises(AssertionError):
        cd.detect_countries(
            DetectorInput0.text_3, expected_match_n=expected_matches_n - 1
        )
