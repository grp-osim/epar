from utils.validators import LicenceValidator, ReValidator


def test_licence_validator():
    assert LicenceValidator.get_licence_name("CC-BY") == "CC BY"


def test_licence_validator_none_ok():
    assert LicenceValidator.get_licence_name(None) is None


def test_doi_valid_01():
    re_validator = ReValidator()
    assert re_validator.doi_valid("10.1128/msystems.00831-21", None) is True
