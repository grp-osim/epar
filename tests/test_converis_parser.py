import json
import os
import pandas as pd
import pytest

import test_aux.test_data.dataset_1 as td1
from converis.fields import CONVERIS_FIELDNAMES
from utils.exceptions import DuplicateValueError


column_names = list()
for k, v in CONVERIS_FIELDNAMES.__dict__.items():
    if v:
        column_names.append(k)


def test_parser_init_ok(converis_parser_0):
    cep = converis_parser_0
    assert cep.df.shape == (619, len(column_names))
    assert sorted(cep.df.columns.values.tolist()) == sorted(column_names)


def test_filter_out_preprints_corrections_etc(converis_parser_0):
    cep = converis_parser_0
    cep.prune_dataframe()
    assert cep.df.shape == (587, len(column_names))


def test_check_for_duplicates(converis_parser_1):
    cep = converis_parser_1
    with pytest.raises(DuplicateValueError):
        cep.check_for_duplicates()


def test_check_for_duplicates_none_found(converis_parser_2):
    cep = converis_parser_2
    r = cep.check_for_duplicates()
    assert r is None


def test_output_csv(converis_parser_0):
    cep = converis_parser_0
    cep.output_csv()
    df = pd.read_csv(cep.output_filepath)
    expected_number_of_columns = len(column_names) + 1  # + 1 because of index
    assert df.shape == (619, expected_number_of_columns)
    # cleanup
    os.remove(cep.output_filepath)


def test_parse_df_data(converis_module_parser_1):
    cep = converis_module_parser_1
    cep.parse_df_data()
    assert json.loads(cep.df.to_json()) == td1.expected_parsed_df_dict_from_json


def test_label_df(converis_parser_0):
    assert sorted(converis_parser_0.label_df().columns) == sorted(
        [
            "converis_id_converis",
            "abstract_converis",
            "affiliation_converis",
            "authors_converis",
            "doi_converis",
            "embl_authors_converis",
            "embl_pub_converis",
            "external_collaboration_bool_converis",
            "issn_converis",
            "journal_acronym_converis",
            "journal_name_converis",
            "licence_converis",
            "number_of_authors_converis",
            "number_of_embl_authors_converis",
            "oa_route_converis",
            "pmc_id_converis",
            "publication_type_converis",
            "publication_type_in_source_converis",
            "pubmed_id_converis",
            "orcids_converis",
            "title_converis",
            "year_converis",
        ]
    )
