import json
from oaswitchboard.oaswitchboard_fields import oaswitchboard_fieldnames
from test_aux.test_data.oaswitchboard_0 import OaswitchboardData0

column_names = list()
for k, v in oaswitchboard_fieldnames.__dict__.items():
    if v:
        column_names.append(k)


def test_parser_init_ok(oaswitchboard_parser_0):
    oep = oaswitchboard_parser_0
    oep.df.to_csv("oaswitchboard_test.csv")
    assert oep.df.shape == (127, len(column_names) + 1)
    assert sorted(oep.df.columns.values.tolist()) == sorted(
        [*column_names, "provenance"]
    )


def test_parse_authors(oaswitchboard_parser_0):
    oep = oaswitchboard_parser_0
    authors_df = oep.parse_authors(OaswitchboardData0.row_0)
    expected_df_dict = {
        "affiliation": {
            "0": "York Structural Biology Laboratory, Department of "
            "Chemistry, University of York, UK",
            "1": "York Structural Biology Laboratory, Department of "
            "Chemistry, University of York, UK",
            "2": "Structural and Computational Biology Unit, European "
            "Molecular Biology Laboratory, Germany",
            "3": "MRC Mitochondrial Biology Unit, University of "
            "Cambridge, The Keith Peters Building, Cambridge "
            "Biomedical Campus, UK",
            "4": "Department of Structural Biology, School of Medicine, "
            "University of Pittsburgh, USA",
            "5": "Department of Structural Biology, School of Medicine, "
            "University of Pittsburgh, USA",
            "6": "York Structural Biology Laboratory, Department of "
            "Chemistry, University of York, UK",
        },
        "authorship_position": {"0": 1, "1": 2, "2": 3, "3": 4, "4": 5, "5": 6, "6": 7},
        "confidence_embl_author": {
            "0": None,
            "1": None,
            "2": 9.0,
            "3": None,
            "4": None,
            "5": None,
            "6": None,
        },
        "embl_author": {
            "0": False,
            "1": False,
            "2": True,
            "3": False,
            "4": False,
            "5": False,
            "6": False,
        },
        "first_name": {
            "0": "Dorothy E D P",
            "1": "Oliver\xa0W",
            "2": "Herman K H",
            "3": "Daniel N",
            "4": "Alexis",
            "5": "James\xa0F",
            "6": "Alfred A",
        },
        "institutions": {
            "0": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "GB"},
                "is_embl_site": {"0": False},
                "name": {"0": "University of York"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/04m01e293"},
                "source_embl_site": {"0": None},
            },
            "1": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "GB"},
                "is_embl_site": {"0": False},
                "name": {"0": "University of York"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/04m01e293"},
                "source_embl_site": {"0": None},
            },
            "2": {
                "confidence_embl_site": {"0": 9},
                "country_code": {"0": "DE"},
                "is_embl_site": {"0": True},
                "name": {"0": "European Molecular Biology Laboratory"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/03mstc592"},
                "source_embl_site": {"0": "ror"},
            },
            "3": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "GB"},
                "is_embl_site": {"0": False},
                "name": {"0": "MRC Mitochondrial Biology Unit"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/01vdt8f48"},
                "source_embl_site": {"0": None},
            },
            "4": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "US"},
                "is_embl_site": {"0": False},
                "name": {"0": "Southern Illinois University School of " "Medicine"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/0232r4451"},
                "source_embl_site": {"0": None},
            },
            "5": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "US"},
                "is_embl_site": {"0": False},
                "name": {"0": "Southern Illinois University School of " "Medicine"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/0232r4451"},
                "source_embl_site": {"0": None},
            },
            "6": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "GB"},
                "is_embl_site": {"0": False},
                "name": {"0": "University of York"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - ror"}'
                },
                "ror_id": {"0": "https://ror.org/04m01e293"},
                "source_embl_site": {"0": None},
            },
        },
        "is_corresponding": {
            "0": True,
            "1": False,
            "2": False,
            "3": False,
            "4": False,
            "5": False,
            "6": True,
        },
        "last_name": {
            "0": "Hawkins",
            "1": "Bayfield",
            "2": "Fung",
            "3": "Grba",
            "4": "Huet",
            "5": "Conway",
            "6": "Antson",
        },
        "orcid": {
            "0": "0000-0001-9117-680X",
            "1": "0000-0003-1421-7780",
            "2": "0000-0001-9568-1782",
            "3": "",
            "4": "",
            "5": "",
            "6": "0000-0002-4533-3816",
        },
        "source_embl_author": {
            "0": None,
            "1": None,
            "2": "ror",
            "3": None,
            "4": None,
            "5": None,
            "6": None,
        },
    }
    assert json.loads(authors_df.to_json()) == expected_df_dict


def test_parse_authors_no_affiliation_data(oaswitchboard_parser_0):
    oep = oaswitchboard_parser_0
    authors_df = oep.parse_authors(OaswitchboardData0.row_1)
    expected_df_dict = {
        "affiliation": {
            "0": None,
            "1": None,
            "10": "https://ror.org/00fp3ce15",
            "2": None,
            "3": None,
            "4": None,
            "5": None,
            "6": None,
            "7": None,
            "8": None,
            "9": None,
        },
        "authorship_position": {
            "0": 1,
            "1": 2,
            "10": 11,
            "2": 3,
            "3": 4,
            "4": 5,
            "5": 6,
            "6": 7,
            "7": 8,
            "8": 9,
            "9": 10,
        },
        "confidence_embl_author": {
            "0": None,
            "1": None,
            "10": None,
            "2": 3.0,
            "3": None,
            "4": None,
            "5": None,
            "6": None,
            "7": None,
            "8": None,
            "9": None,
        },
        "embl_author": {
            "0": False,
            "1": False,
            "10": False,
            "2": True,
            "3": False,
            "4": False,
            "5": False,
            "6": False,
            "7": False,
            "8": False,
            "9": False,
        },
        "first_name": {
            "0": "Aracely",
            "1": "Christopher",
            "10": "Azim",
            "2": "Michael",
            "3": "Walfred",
            "4": "Toshihiro",
            "5": "Frederick",
            "6": "Sophie",
            "7": "Erin",
            "8": "Thorsten",
            "9": "John",
        },
        "institutions": {
            "0": {"provenance": {}},
            "1": {"provenance": {}},
            "10": {
                "confidence_embl_site": {"0": None},
                "country_code": {"0": "GB"},
                "is_embl_site": {"0": False},
                "name": {"0": "Wellcome/Cancer Research UK Gurdon " "Institute"},
                "provenance": {
                    "0": '{"name": "oaswitchboard - name", '
                    '"ror_id": "oaswitchboard - '
                    'ror"}'
                },
                "ror_id": {"0": "https://ror.org/00fp3ce15"},
                "source_embl_site": {"0": None},
            },
            "2": {"provenance": {}},
            "3": {"provenance": {}},
            "4": {"provenance": {}},
            "5": {"provenance": {}},
            "6": {"provenance": {}},
            "7": {"provenance": {}},
            "8": {"provenance": {}},
            "9": {"provenance": {}},
        },
        "is_corresponding": {
            "0": False,
            "1": False,
            "10": True,
            "2": False,
            "3": False,
            "4": False,
            "5": False,
            "6": False,
            "7": False,
            "8": False,
            "9": False,
        },
        "last_name": {
            "0": "Castillo-Venzor",
            "1": "Penfold",
            "10": "Surani",
            "2": "Morgan",
            "3": "Tang",
            "4": "Kobayashi",
            "5": "Wong",
            "6": "Bergmann",
            "7": "Slatery",
            "8": "Boroviak",
            "9": "Marioni",
        },
        "orcid": {
            "0": "0000-0002-2288-2679",
            "1": "",
            "10": "0000-0002-8640-4318",
            "2": "0000-0003-0757-0711",
            "3": "0000-0002-5803-1681",
            "4": "",
            "5": "",
            "6": "",
            "7": "0000-0002-9097-6618",
            "8": "",
            "9": "",
        },
        "source_embl_author": {
            "0": None,
            "1": None,
            "10": None,
            "2": "orcid",
            "3": None,
            "4": None,
            "5": None,
            "6": None,
            "7": None,
            "8": None,
            "9": None,
        },
    }
    assert json.loads(authors_df.to_json()) == expected_df_dict


def test_parse_df_data(oaswitchboard_parser_1):
    oep = oaswitchboard_parser_1
    oep.parse_df_data()
    assert oep.is_embl_publication(oep.df.loc[0]) == (True, "ror", 9.0)
    assert oep.df.iloc[2].embl_collaborations == {
        "CH": ["Institute for Independent Studies Zürich"],
        "DE": ["Klinikum Saarbrücken", "Saarland University"],
        "ES": ["Centre for Genomic Regulation"],
        "GB": ["William Harvey Research Institute"],
        "US": [
            "Departments of Pediatrics and Biomedical Informatics, The Ohio State "
            "University College of Medicine, Columbus, OH, United States of "
            "America",
            "Lawrence Berkeley National Laboratory",
            "Nationwide Children's Hospital",
            "The Jackson Laboratory for Genomic Medicine, Farmington, CT, United "
            "States of America",
            "University of Colorado Anschutz Medical Campus",
            "University of Connecticut",
        ],
    }
