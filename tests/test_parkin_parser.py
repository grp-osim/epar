def test_parkin_parser_init_ok(parkin_parser_0):
    pp = parkin_parser_0
    assert pp.df.shape == (1252, 15)


def test_get_orcid_first_name_ok(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(first_name="Julia")
    assert orcids == "1111-2345-6789-0000"


def test_get_orcid_first_name_not_found(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(first_name="Monica")
    assert orcids is None


def test_get_orcid_last_name_ok(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(last_name="Jones")
    assert orcids == "1111-2345-6789-0000"


def test_get_orcid_last_name_ambiguous(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(last_name="Smith")
    assert orcids == {"2222-3456-7890-1111", "3333-3456-7890-1111"}


def test_get_orcid_first_and_last_name(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(first_name="Arthur", last_name="Smith")
    assert orcids == "3333-3456-7890-1111"


def test_get_orcid_first_and_last_name_not_found(parkin_parser_1):
    orcids = parkin_parser_1.get_orcid(first_name="Julia", last_name="Smith")
    assert orcids is None


def test_is_orcid_in_dataset(parkin_parser_1):
    assert parkin_parser_1.is_orcid_in_dataset("nonsense") is False
    assert parkin_parser_1.is_orcid_in_dataset("0000-0003-0682-1647") is False
    assert parkin_parser_1.is_orcid_in_dataset("0000-0003-0682-1646") is True
