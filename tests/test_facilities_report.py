import os
import json
from utils.fieldset import StandardNames as sn
from utils.utils import BASE_DIR
from reports.facilities import (
    total_member_state_column_name,
    total_member_institutes_column_name,
    non_member_state_column_name,
    member_state_column_name,
)


def test_query_openalex_publications(facilities_report_1):
    """
    This test may fail because the response from the OpenAlex API
    changes from time to time as that datasource evolves.
    """
    facilities_report_1.query_openalex_publications()
    expected_row_dict = {
        'authors': {'0': {'affiliation': 'acib - Austrian Center of Industrial '
                                         'Biotechnology, Krenngasse 37, 8010 Graz, '
                                         'Austria; Institute of Molecular '
                                         'Biosciences, University of Graz, '
                                         'Humboldtstraße 50, 8010Graz, Austria; '
                                         'BioTechMed-Graz, 8010Graz, Austria',
                          'authorship_position': 'first',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Austrian Centre of '
                                                         'Industrial Biotechnology '
                                                         '(Austria)',
                                                 'open_alex_id': 'https://openalex.org/I4210138243',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/03dm7dd93',
                                                 'source_embl_site': None,
                                                 'type': 'company'},
                                           '1': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'University of Graz',
                                                 'open_alex_id': 'https://openalex.org/I15766117',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/01faaaf77',
                                                 'source_embl_site': None,
                                                 'type': 'education'},
                                           '2': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'BioTechMed-Graz',
                                                 'open_alex_id': 'https://openalex.org/I4210117258',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/02jfbm483',
                                                 'source_embl_site': None,
                                                 'type': 'facility'}},
                          'is_corresponding': False,
                          'name': 'Bastian Daniel',
                          'open_alex_id': 'https://openalex.org/A5066897001',
                          'orcid': '0000-0001-9210-7136',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['acib - Austrian Center of '
                                                      'Industrial Biotechnology, '
                                                      'Krenngasse 37, 8010 Graz, '
                                                      'Austria',
                                                      'Institute of Molecular '
                                                      'Biosciences, University of '
                                                      'Graz, Humboldtstraße 50, '
                                                      '8010Graz, Austria',
                                                      'BioTechMed-Graz, 8010Graz, '
                                                      'Austria'],
                          'source_embl_author': None},
                    '1': {'affiliation': 'acib - Austrian Center of Industrial '
                                         'Biotechnology, Krenngasse 37, 8010 Graz, '
                                         'Austria; Institute of Molecular '
                                         'Biotechnology, Graz University of '
                                         'Technology, Petersgasse 14, 8010 Graz, '
                                         'Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Austrian Centre of '
                                                         'Industrial Biotechnology '
                                                         '(Austria)',
                                                 'open_alex_id': 'https://openalex.org/I4210138243',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/03dm7dd93',
                                                 'source_embl_site': None,
                                                 'type': 'company'},
                                           '1': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Graz University of '
                                                         'Technology',
                                                 'open_alex_id': 'https://openalex.org/I4092182',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/00d7xrm67',
                                                 'source_embl_site': None,
                                                 'type': 'education'},
                                           '2': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Institute of Molecular '
                                                         'Biotechnology',
                                                 'open_alex_id': 'https://openalex.org/I125523173',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/01zqrxf85',
                                                 'source_embl_site': None,
                                                 'type': 'facility'}},
                          'is_corresponding': False,
                          'name': 'Chiam Hashem',
                          'open_alex_id': 'https://openalex.org/A5025512128',
                          'orcid': None,
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['acib - Austrian Center of '
                                                      'Industrial Biotechnology, '
                                                      'Krenngasse 37, 8010 Graz, '
                                                      'Austria',
                                                      'Institute of Molecular '
                                                      'Biotechnology, Graz University '
                                                      'of Technology, Petersgasse 14, '
                                                      '8010 Graz, Austria'],
                          'source_embl_author': None},
                    '10': {'affiliation': 'Department of Biotechnology, TU Delft, Van '
                                          'der Maasweg 9, 2629HZ Delft, The '
                                          'Netherlands; Department of Chemistry, '
                                          'Faculty of Science, Sohag University, '
                                          'Sohag 82524, Egypt',
                           'authorship_position': 'middle',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'EG',
                                                  'is_embl_site': False,
                                                  'name': 'Sohag University',
                                                  'open_alex_id': 'https://openalex.org/I97455420',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/02wgx3e98',
                                                  'source_embl_site': None,
                                                  'type': 'education'},
                                            '1': {'confidence_embl_site': None,
                                                  'country_code': 'NL',
                                                  'is_embl_site': False,
                                                  'name': 'Delft University of '
                                                          'Technology',
                                                  'open_alex_id': 'https://openalex.org/I98358874',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/02e2c7k09',
                                                  'source_embl_site': None,
                                                  'type': 'education'}},
                           'is_corresponding': False,
                           'name': 'Sabry H. H. Younes',
                           'open_alex_id': 'https://openalex.org/A5017229322',
                           'orcid': None,
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['Department of Biotechnology, '
                                                       'TU Delft, Van der Maasweg 9, '
                                                       '2629HZ Delft, The Netherlands',
                                                       'Department of Chemistry, '
                                                       'Faculty of Science, Sohag '
                                                       'University, Sohag 82524, '
                                                       'Egypt'],
                           'source_embl_author': None},
                    '11': {'affiliation': 'Institute for Biochemistry, Graz '
                                          'University of Technology, Petersgasse 12, '
                                          '8010 Graz, Austria',
                           'authorship_position': 'middle',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Graz University of '
                                                          'Technology',
                                                  'open_alex_id': 'https://openalex.org/I4092182',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/00d7xrm67',
                                                  'source_embl_site': None,
                                                  'type': 'education'}},
                           'is_corresponding': False,
                           'name': 'Gustav Oberdorfer',
                           'open_alex_id': 'https://openalex.org/A5021953327',
                           'orcid': '0000-0002-6144-9114',
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['Institute for Biochemistry, '
                                                       'Graz University of '
                                                       'Technology, Petersgasse 12, '
                                                       '8010 Graz, Austria'],
                           'source_embl_author': None},
                    '12': {'affiliation': 'Department of Biological Sciences and '
                                          'Biotechnology, Universiti Kebangsaan '
                                          'Malaysia, 43600 Bangi, Selangor Malaysia',
                           'authorship_position': 'middle',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'MY',
                                                  'is_embl_site': False,
                                                  'name': 'National University of '
                                                          'Malaysia',
                                                  'open_alex_id': 'https://openalex.org/I885383172',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/00bw8d226',
                                                  'source_embl_site': None,
                                                  'type': 'education'}},
                           'is_corresponding': False,
                           'name': 'Farah Diba Abu Bakar',
                           'open_alex_id': 'https://openalex.org/A5042344608',
                           'orcid': '0000-0002-6416-7470',
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['Department of Biological '
                                                       'Sciences and Biotechnology, '
                                                       'Universiti Kebangsaan '
                                                       'Malaysia, 43600 Bangi, '
                                                       'Selangor Malaysia'],
                           'source_embl_author': None},
                    '13': {'affiliation': 'acib - Austrian Center of Industrial '
                                          'Biotechnology, Krenngasse 37, 8010 Graz, '
                                          'Austria; Institute of Molecular '
                                          'Biosciences, University of Graz, '
                                          'Humboldtstraße 50, 8010Graz, Austria; '
                                          'BioTechMed-Graz, 8010Graz, Austria',
                           'authorship_position': 'middle',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Austrian Centre of '
                                                          'Industrial Biotechnology '
                                                          '(Austria)',
                                                  'open_alex_id': 'https://openalex.org/I4210138243',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/03dm7dd93',
                                                  'source_embl_site': None,
                                                  'type': 'company'},
                                            '1': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'University of Graz',
                                                  'open_alex_id': 'https://openalex.org/I15766117',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/01faaaf77',
                                                  'source_embl_site': None,
                                                  'type': 'education'},
                                            '2': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'BioTechMed-Graz',
                                                  'open_alex_id': 'https://openalex.org/I4210117258',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/02jfbm483',
                                                  'source_embl_site': None,
                                                  'type': 'facility'}},
                           'is_corresponding': False,
                           'name': 'Karl Gruber',
                           'open_alex_id': 'https://openalex.org/A5027940353',
                           'orcid': '0000-0002-3485-9740',
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['acib - Austrian Center of '
                                                       'Industrial Biotechnology, '
                                                       'Krenngasse 37, 8010 Graz, '
                                                       'Austria',
                                                       'Institute of Molecular '
                                                       'Biosciences, University of '
                                                       'Graz, Humboldtstraße 50, '
                                                       '8010Graz, Austria',
                                                       'BioTechMed-Graz, 8010Graz, '
                                                       'Austria'],
                           'source_embl_author': None},
                    '14': {'affiliation': 'acib - Austrian Center of Industrial '
                                          'Biotechnology, Krenngasse 37, 8010 Graz, '
                                          'Austria; Institute of Molecular '
                                          'Biosciences, University of Graz, '
                                          'Humboldtstraße 50, 8010Graz, Austria; '
                                          'BioTechMed-Graz, 8010Graz, Austria',
                           'authorship_position': 'middle',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Austrian Centre of '
                                                          'Industrial Biotechnology '
                                                          '(Austria)',
                                                  'open_alex_id': 'https://openalex.org/I4210138243',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/03dm7dd93',
                                                  'source_embl_site': None,
                                                  'type': 'company'},
                                            '1': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'University of Graz',
                                                  'open_alex_id': 'https://openalex.org/I15766117',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/01faaaf77',
                                                  'source_embl_site': None,
                                                  'type': 'education'},
                                            '2': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'BioTechMed-Graz',
                                                  'open_alex_id': 'https://openalex.org/I4210117258',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/02jfbm483',
                                                  'source_embl_site': None,
                                                  'type': 'facility'}},
                           'is_corresponding': True,
                           'name': 'Tea Pavkov-Keller',
                           'open_alex_id': 'https://openalex.org/A5062499817',
                           'orcid': '0000-0001-7871-6680',
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['acib - Austrian Center of '
                                                       'Industrial Biotechnology, '
                                                       'Krenngasse 37, 8010 Graz, '
                                                       'Austria',
                                                       'Institute of Molecular '
                                                       'Biosciences, University of '
                                                       'Graz, Humboldtstraße 50, '
                                                       '8010Graz, Austria',
                                                       'BioTechMed-Graz, 8010Graz, '
                                                       'Austria'],
                           'source_embl_author': None},
                    '15': {'affiliation': 'acib - Austrian Center of Industrial '
                                          'Biotechnology, Krenngasse 37, 8010 Graz, '
                                          'Austria; Institute of Molecular '
                                          'Biotechnology, Graz University of '
                                          'Technology, Petersgasse 14, 8010 Graz, '
                                          'Austria',
                           'authorship_position': 'last',
                           'confidence_embl_author': None,
                           'embl_author': False,
                           'institutions': {'0': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Austrian Centre of '
                                                          'Industrial Biotechnology '
                                                          '(Austria)',
                                                  'open_alex_id': 'https://openalex.org/I4210138243',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/03dm7dd93',
                                                  'source_embl_site': None,
                                                  'type': 'company'},
                                            '1': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Graz University of '
                                                          'Technology',
                                                  'open_alex_id': 'https://openalex.org/I4092182',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/00d7xrm67',
                                                  'source_embl_site': None,
                                                  'type': 'education'},
                                            '2': {'confidence_embl_site': None,
                                                  'country_code': 'AT',
                                                  'is_embl_site': False,
                                                  'name': 'Institute of Molecular '
                                                          'Biotechnology',
                                                  'open_alex_id': 'https://openalex.org/I125523173',
                                                  'provenance': '{"open_alex_id": '
                                                                '"alex - id", "name": '
                                                                '"alex - '
                                                                'display_name", '
                                                                '"ror_id": "alex - '
                                                                'ror", '
                                                                '"country_code": '
                                                                '"alex - '
                                                                'country_code"}',
                                                  'ror_id': 'https://ror.org/01zqrxf85',
                                                  'source_embl_site': None,
                                                  'type': 'facility'}},
                           'is_corresponding': True,
                           'name': 'Margit Winkler',
                           'open_alex_id': 'https://openalex.org/A5046543571',
                           'orcid': '0000-0002-0754-9704',
                           'provenance': '{"open_alex_id": "alex - author.id", '
                                         '"affiliation": "alex - '
                                         'raw_affiliation_string", '
                                         '"authorship_position": "alex - '
                                         'author_position", "institutions": "alex - '
                                         'institutions", "name": "alex - '
                                         'author.display_name", "orcid": "alex - '
                                         'author.orcid"}',
                           'raw_affiliation_strings': ['acib - Austrian Center of '
                                                       'Industrial Biotechnology, '
                                                       'Krenngasse 37, 8010 Graz, '
                                                       'Austria',
                                                       'Institute of Molecular '
                                                       'Biotechnology, Graz '
                                                       'University of Technology, '
                                                       'Petersgasse 14, 8010 Graz, '
                                                       'Austria'],
                           'source_embl_author': None},
                    '2': {'affiliation': 'acib - Austrian Center of Industrial '
                                         'Biotechnology, Krenngasse 37, 8010 Graz, '
                                         'Austria; Institute of Molecular '
                                         'Biosciences, University of Graz, '
                                         'Humboldtstraße 50, 8010Graz, Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Austrian Centre of '
                                                         'Industrial Biotechnology '
                                                         '(Austria)',
                                                 'open_alex_id': 'https://openalex.org/I4210138243',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/03dm7dd93',
                                                 'source_embl_site': None,
                                                 'type': 'company'},
                                           '1': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'University of Graz',
                                                 'open_alex_id': 'https://openalex.org/I15766117',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/01faaaf77',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Marlene Leithold',
                          'open_alex_id': 'https://openalex.org/A5050061566',
                          'orcid': None,
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['acib - Austrian Center of '
                                                      'Industrial Biotechnology, '
                                                      'Krenngasse 37, 8010 Graz, '
                                                      'Austria',
                                                      'Institute of Molecular '
                                                      'Biosciences, University of '
                                                      'Graz, Humboldtstraße 50, '
                                                      '8010Graz, Austria'],
                          'source_embl_author': None},
                    '3': {'affiliation': 'Institute of Molecular Biosciences, '
                                         'University of Graz, Humboldtstraße 50, '
                                         '8010Graz, Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'University of Graz',
                                                 'open_alex_id': 'https://openalex.org/I15766117',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/01faaaf77',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Theo Sagmeister',
                          'open_alex_id': 'https://openalex.org/A5083275750',
                          'orcid': '0000-0002-6703-5510',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Institute of Molecular '
                                                      'Biosciences, University of '
                                                      'Graz, Humboldtstraße 50, '
                                                      '8010Graz, Austria'],
                          'source_embl_author': None},
                    '4': {'affiliation': 'Institute for Biochemistry, Graz University '
                                         'of Technology, Petersgasse 12, 8010 Graz, '
                                         'Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Graz University of '
                                                         'Technology',
                                                 'open_alex_id': 'https://openalex.org/I4092182',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/00d7xrm67',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Adrian Tripp',
                          'open_alex_id': 'https://openalex.org/A5035259320',
                          'orcid': '0000-0001-5217-2400',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Institute for Biochemistry, '
                                                      'Graz University of Technology, '
                                                      'Petersgasse 12, 8010 Graz, '
                                                      'Austria'],
                          'source_embl_author': None},
                    '5': {'affiliation': 'acib - Austrian Center of Industrial '
                                         'Biotechnology, Krenngasse 37, 8010 Graz, '
                                         'Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Austrian Centre of '
                                                         'Industrial Biotechnology '
                                                         '(Austria)',
                                                 'open_alex_id': 'https://openalex.org/I4210138243',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/03dm7dd93',
                                                 'source_embl_site': None,
                                                 'type': 'company'}},
                          'is_corresponding': False,
                          'name': 'Holly Stolterfoht-Stock',
                          'open_alex_id': 'https://openalex.org/A5088229376',
                          'orcid': None,
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['acib - Austrian Center of '
                                                      'Industrial Biotechnology, '
                                                      'Krenngasse 37, 8010 Graz, '
                                                      'Austria'],
                          'source_embl_author': None},
                    '6': {'affiliation': 'Institute for Biochemistry, Graz University '
                                         'of Technology, Petersgasse 12, 8010 Graz, '
                                         'Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'Graz University of '
                                                         'Technology',
                                                 'open_alex_id': 'https://openalex.org/I4092182',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/00d7xrm67',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Julia Messenlehner',
                          'open_alex_id': 'https://openalex.org/A5082496177',
                          'orcid': None,
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Institute for Biochemistry, '
                                                      'Graz University of Technology, '
                                                      'Petersgasse 12, 8010 Graz, '
                                                      'Austria'],
                          'source_embl_author': None},
                    '7': {'affiliation': 'Rutherford Appleton Laboratory, Research '
                                         'Complex at Harwell, UKRI-STFC, Didcot OX11 '
                                         '0FA, United Kingdom',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'GB',
                                                 'is_embl_site': False,
                                                 'name': 'Research Complex at Harwell',
                                                 'open_alex_id': 'https://openalex.org/I4210092957',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/00gqx0331',
                                                 'source_embl_site': None,
                                                 'type': 'facility'},
                                           '1': {'confidence_embl_site': None,
                                                 'country_code': 'GB',
                                                 'is_embl_site': False,
                                                 'name': 'Rutherford Appleton '
                                                         'Laboratory',
                                                 'open_alex_id': 'https://openalex.org/I1286704778',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/03gq8fr08',
                                                 'source_embl_site': None,
                                                 'type': 'facility'}},
                          'is_corresponding': False,
                          'name': 'Ronan Keegan',
                          'open_alex_id': 'https://openalex.org/A5020751117',
                          'orcid': '0000-0002-9495-0431',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Rutherford Appleton '
                                                      'Laboratory, Research Complex '
                                                      'at Harwell, UKRI-STFC, Didcot '
                                                      'OX11 0FA, United Kingdom'],
                          'source_embl_author': None},
                    '8': {'affiliation': 'Institute of Chemistry, University of Graz, '
                                         'Heinrichstraße 28, 8010Graz, Austria',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'AT',
                                                 'is_embl_site': False,
                                                 'name': 'University of Graz',
                                                 'open_alex_id': 'https://openalex.org/I15766117',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/01faaaf77',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Christoph Winkler',
                          'open_alex_id': 'https://openalex.org/A5007043991',
                          'orcid': '0000-0003-3068-9817',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Institute of Chemistry, '
                                                      'University of Graz, '
                                                      'Heinrichstraße 28, 8010Graz, '
                                                      'Austria'],
                          'source_embl_author': None},
                    '9': {'affiliation': 'Department of Biological Sciences and '
                                         'Biotechnology, Universiti Kebangsaan '
                                         'Malaysia, 43600 Bangi, Selangor Malaysia',
                          'authorship_position': 'middle',
                          'confidence_embl_author': None,
                          'embl_author': False,
                          'institutions': {'0': {'confidence_embl_site': None,
                                                 'country_code': 'MY',
                                                 'is_embl_site': False,
                                                 'name': 'National University of '
                                                         'Malaysia',
                                                 'open_alex_id': 'https://openalex.org/I885383172',
                                                 'provenance': '{"open_alex_id": '
                                                               '"alex - id", "name": '
                                                               '"alex - '
                                                               'display_name", '
                                                               '"ror_id": "alex - '
                                                               'ror", "country_code": '
                                                               '"alex - '
                                                               'country_code"}',
                                                 'ror_id': 'https://ror.org/00bw8d226',
                                                 'source_embl_site': None,
                                                 'type': 'education'}},
                          'is_corresponding': False,
                          'name': 'Jonathan Guyang Ling',
                          'open_alex_id': 'https://openalex.org/A5086734079',
                          'orcid': '0000-0001-8188-0836',
                          'provenance': '{"open_alex_id": "alex - author.id", '
                                        '"affiliation": "alex - '
                                        'raw_affiliation_string", '
                                        '"authorship_position": "alex - '
                                        'author_position", "institutions": "alex - '
                                        'institutions", "name": "alex - '
                                        'author.display_name", "orcid": "alex - '
                                        'author.orcid"}',
                          'raw_affiliation_strings': ['Department of Biological '
                                                      'Sciences and Biotechnology, '
                                                      'Universiti Kebangsaan '
                                                      'Malaysia, 43600 Bangi, '
                                                      'Selangor Malaysia'],
                          'source_embl_author': None}},
        'doi': '10.1021/acscatal.2c04426',
        'embl_collaborations': {'AT': ['Austrian Centre of Industrial Biotechnology '
                                       '(Austria)',
                                       'BioTechMed-Graz',
                                       'Graz University of Technology',
                                       'Institute of Molecular Biotechnology',
                                       'University of Graz'],
                                'EG': ['Sohag University'],
                                'GB': ['Research Complex at Harwell',
                                       'Rutherford Appleton Laboratory'],
                                'MY': ['National University of Malaysia'],
                                'NL': ['Delft University of Technology']},
        'embl_collaborations_quality': 'high',
        'embl_collaborations_quality_note': None,
        'embl_pub': False,
        'embl_pub_confidence': None,
        'open_alex_id': 'https://openalex.org/W4312032326',
        'publication_date': '2022-12-06',
        'publication_type': 'article',
        'pubmed_id': 'https://pubmed.ncbi.nlm.nih.gov/37180375',
        'title': 'Structure of the Reductase Domain of a Fungal Carboxylic Acid '
                 'Reductase and Its Substrate Scope in Thioester and Aldehyde '
                 'Reduction',
        'year': 2022}
    assert json.loads(facilities_report_1.oap.df.loc[4].to_json()) == expected_row_dict

    embl_pub_dict = {'authors': {'0': {'affiliation': '1European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany; 2Collaboration '
                                                      'for joint Ph.D. degree between EMBL and '
                                                      'Heidelberg University, Faculty of '
                                                      'Biosciences, Heidelberg, Germany',
                                       'authorship_position': 'first',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9.0,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'},
                                                        '1': {'confidence_embl_site': None,
                                                              'country_code': 'DE',
                                                              'is_embl_site': False,
                                                              'name': 'European Molecular Biology '
                                                                      'Organization',
                                                              'open_alex_id': 'https://openalex.org/I1303691731',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/04wfr2810',
                                                              'source_embl_site': None,
                                                              'type': 'nonprofit'},
                                                        '2': {'confidence_embl_site': None,
                                                              'country_code': 'DE',
                                                              'is_embl_site': False,
                                                              'name': 'Heidelberg University',
                                                              'open_alex_id': 'https://openalex.org/I223822909',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/038t36y30',
                                                              'source_embl_site': None,
                                                              'type': 'education'}},
                                       'is_corresponding': False,
                                       'name': 'Magdalena Büscher',
                                       'open_alex_id': 'https://openalex.org/A5023398541',
                                       'orcid': '0000-0002-2215-3329',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['1European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany',
                                                                   '2Collaboration for joint Ph.D. '
                                                                   'degree between EMBL and '
                                                                   'Heidelberg University, Faculty '
                                                                   'of Biosciences, Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '1': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Rastislav Horos',
                                       'open_alex_id': 'https://openalex.org/A5069138110',
                                       'orcid': '0000-0001-7730-9557',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '2': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Ina Huppertz',
                                       'open_alex_id': 'https://openalex.org/A5080375185',
                                       'orcid': '0000-0002-8520-025X',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '3': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Kevin Haubrich',
                                       'open_alex_id': 'https://openalex.org/A5014356147',
                                       'orcid': '0000-0001-5025-3821',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '4': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Nikolay Dobrev',
                                       'open_alex_id': 'https://openalex.org/A5013334753',
                                       'orcid': '0000-0002-8144-4599',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '5': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Florence Baudin',
                                       'open_alex_id': 'https://openalex.org/A5039829182',
                                       'orcid': '0000-0003-0816-0861',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '6': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'middle',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Janosch Hennig',
                                       'open_alex_id': 'https://openalex.org/A5053211454',
                                       'orcid': '0000-0001-5214-7002',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'},
                                 '7': {'affiliation': 'European Molecular Biology Laboratory, '
                                                      '69117 Heidelberg, Germany',
                                       'authorship_position': 'last',
                                       'confidence_embl_author': 9.0,
                                       'embl_author': True,
                                       'institutions': {'0': {'confidence_embl_site': 9,
                                                              'country_code': 'DE',
                                                              'is_embl_site': True,
                                                              'name': 'European Molecular Biology '
                                                                      'Laboratory',
                                                              'open_alex_id': 'https://openalex.org/I4210138560',
                                                              'provenance': '{"open_alex_id": '
                                                                            '"alex - id", "name": '
                                                                            '"alex - '
                                                                            'display_name", '
                                                                            '"ror_id": "alex - '
                                                                            'ror", "country_code": '
                                                                            '"alex - '
                                                                            'country_code"}',
                                                              'ror_id': 'https://ror.org/03mstc592',
                                                              'source_embl_site': 'ror',
                                                              'type': 'government'}},
                                       'is_corresponding': False,
                                       'name': 'Matthias W. Hentze',
                                       'open_alex_id': 'https://openalex.org/A5000270755',
                                       'orcid': '0000-0002-4023-7876',
                                       'provenance': '{"open_alex_id": "alex - author.id", '
                                                     '"affiliation": "alex - '
                                                     'raw_affiliation_string", '
                                                     '"authorship_position": "alex - '
                                                     'author_position", "institutions": "alex - '
                                                     'institutions", "name": "alex - '
                                                     'author.display_name", "orcid": "alex - '
                                                     'author.orcid"}',
                                       'raw_affiliation_strings': ['European Molecular Biology '
                                                                   'Laboratory, 69117 Heidelberg, '
                                                                   'Germany'],
                                       'source_embl_author': 'ror'}},
                     'doi': '10.1261/rna.079129.122',
                     'embl_collaborations': {},
                     'embl_collaborations_quality': 'high',
                     'embl_collaborations_quality_note': None,
                     'embl_pub': True,
                     'embl_pub_confidence': 72.0,
                     'open_alex_id': 'https://openalex.org/W4213440690',
                     'publication_date': '2022-02-24',
                     'publication_type': 'article',
                     'pubmed_id': 'https://pubmed.ncbi.nlm.nih.gov/35210358',
                     'title': 'Vault RNA1–1 riboregulates the autophagic function of p62 by '
                              'binding to lysine 7 and arginine 21, both of which are critical for '
                              'p62 oligomerization',
                     'year': 2022}
    assert json.loads(facilities_report_1.oap.df.loc[2].to_json()) == embl_pub_dict


def test_get_data_merger(facilities_report_2):
    expected_columns = ['EMBL Acknowledgment (Y/N)_facilities',
                        'EMBL Author (Y/N)_facilities',
                        'Name of facilities used for paper _facilities',
                        'Reason for suitability as a case study (please feel free to provide links to '
                        'additional documents/websites)_facilities',
                        'Suitable as a case study? (Y/N)_facilities',
                        'Type of publication_facilities',
                        '_merge_0',
                        '_merge_1',
                        '_merge_2',
                        'abstract_wos',
                        'addresses_wos',
                        'affiliation_wos',
                        'australia_annual_report',
                        'australia_inst_annual_report',
                        'austria_annual_report',
                        'austria_inst_annual_report',
                        'authors_alex',
                        'authors_wos',
                        'belgium_annual_report',
                        'belgium_inst_annual_report',
                        'correspondence_wos',
                        'croatia_annual_report',
                        'croatia_inst_annual_report',
                        'czech_republic_annual_report',
                        'czech_republic_inst_annual_report',
                        'denmark_annual_report',
                        'denmark_inst_annual_report',
                        'doi',
                        'doi_alex',
                        'doi_annual_report',
                        'doi_facilities',
                        'doi_wos',
                        'eissn_wos',
                        'embl_collaborations',
                        'embl_collaborations_alex',
                        'embl_collaborations_annual_report',
                        'embl_collaborations_quality_alex',
                        'embl_collaborations_quality_note',
                        'embl_collaborations_quality_note_alex',
                        'embl_collaborations_quality_note_wos',
                        'embl_collaborations_quality_wos',
                        'embl_collaborations_wos',
                        'embl_pub_alex',
                        'embl_pub_confidence_alex',
                        'embl_pub_confidence_wos',
                        'embl_pub_wos',
                        'estonia_annual_report',
                        'estonia_inst_annual_report',
                        'finland_annual_report',
                        'finland_inst_annual_report',
                        'france_annual_report',
                        'france_inst_annual_report',
                        'germany_annual_report',
                        'germany_inst_annual_report',
                        'greece_annual_report',
                        'greece_inst_annual_report',
                        'has_member_state_collaboration_annual_report',
                        'has_non_member_state_collaboration_annual_report',
                        'hungary_annual_report',
                        'hungary_inst_annual_report',
                        'iceland_annual_report',
                        'iceland_inst_annual_report',
                        'ireland_annual_report',
                        'ireland_inst_annual_report',
                        'israel_annual_report',
                        'israel_inst_annual_report',
                        'issn_wos',
                        'italy_annual_report',
                        'italy_inst_annual_report',
                        'journal_acronym_wos',
                        'journal_name_wos',
                        'latvia_annual_report',
                        'latvia_inst_annual_report',
                        'lithuania_annual_report',
                        'lithuania_inst_annual_report',
                        'luxembourg_annual_report',
                        'luxembourg_inst_annual_report',
                        'malta_annual_report',
                        'malta_inst_annual_report',
                        'montenegro_annual_report',
                        'montenegro_inst_annual_report',
                        'netherlands_annual_report',
                        'netherlands_inst_annual_report',
                        'norway_annual_report',
                        'norway_inst_annual_report',
                        'open_alex_id_alex',
                        'orcids_wos',
                        'poland_annual_report',
                        'poland_inst_annual_report',
                        'portugal_annual_report',
                        'portugal_inst_annual_report',
                        'provenance_annual_report',
                        'provenance_wos',
                        'publication_date_alex',
                        'publication_date_wos',
                        'publication_type_alex',
                        'publication_type_wos',
                        'publisher_wos',
                        'pubmed_id_alex',
                        'pubmed_id_wos',
                        'slovakia_annual_report',
                        'slovakia_inst_annual_report',
                        'spain_annual_report',
                        'spain_inst_annual_report',
                        'sweden_annual_report',
                        'sweden_inst_annual_report',
                        'switzerland_annual_report',
                        'switzerland_inst_annual_report',
                        'title_alex',
                        'title_wos',
                        'total_member_states_annual_report',
                        'total_member_states_institutes_annual_report',
                        'united_kingdom_annual_report',
                        'united_kingdom_inst_annual_report',
                        'wos_id_wos',
                        'wos_researcher_ids_wos',
                        'year_alex',
                        'year_wos']
    assert sorted(list(facilities_report_2.data_merger.df.columns)) == sorted(
        expected_columns
    )


def test_resolve_collaborations(facilities_report_2):
    assert (
            facilities_report_2.data_merger.df.iloc[2].embl_collaborations_quality_note
            == "Collaboration countries mismatch between OpenAlex and WoS datasets; "
               "WoS data used because it is marked as high quality"
    )
    assert (
            facilities_report_2.data_merger.df.iloc[2][sn.embl_collaborations]
            == facilities_report_2.data_merger.df.iloc[2][
                sn.embl_collaborations + facilities_report_2.wep.data_label
                ]
    )


def test_unpack_collaborations(facilities_report_2):
    fr = facilities_report_2
    fr.data_merger.df = fr.data_merger.df.apply(fr.unpack_collaborations, axis=1)
    assert fr.data_merger.df.iloc[9][
               "title_wos"] == "A Hitchhiker's guide through the bio-image analysis software universe"
    assert fr.data_merger.df.iloc[9][sn.embl_collaborations] == {
        'FI': ['University of Helsinki'],
        'FR': ['Biopolymères Interactions Assemblages',
               'French Clinical Research Infrastructure Network'],
        'DE': ['TU Dresden', 'Center for Systems Biology Dresden'],
        'SE': ['Uppsala University'],
        'GB': ["King's College London"]
    }
    assert fr.data_merger.df.iloc[9][member_state_column_name] == True
    assert fr.data_merger.df.iloc[9][non_member_state_column_name] == False
    assert fr.data_merger.df.iloc[9][total_member_state_column_name] == 5
    assert fr.data_merger.df.iloc[9][total_member_institutes_column_name] == 7


def test_output_report(facilities_report_2):
    fr = facilities_report_2
    fr.resolve_is_embl()
    fr.data_merger.df = fr.data_merger.df.apply(fr.unpack_collaborations, axis=1)
    fr.output_report(os.path.join(BASE_DIR, "debug", "facilities_report_test.xlsx"))
