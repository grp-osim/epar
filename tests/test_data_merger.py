import numpy as np
import pytest

from utils.data_merger import DataMerger
from utils.exceptions import DataValidationError
from utils.fieldset import StandardNames


def test_data_merger_ok(data_container_0, data_container_1):
    dm = DataMerger(
        data_containers=[data_container_0, data_container_1], on=StandardNames.doi
    )
    dm.merge_data()
    assert list(dm.df[StandardNames.doi]) == [
        "10.11646/zootaxa.3772.1.1",
        "10.1111/j.1463-6395.2006.00240.x",
        "10.1093/mollus/eyp029",
    ]


def test_data_merger_strict_error_right(data_container_2, data_container_1):
    dm = DataMerger(
        data_containers=[data_container_2, data_container_1], on=StandardNames.doi
    )
    with pytest.raises(DataValidationError, match=r"doi_dc0"):
        dm.merge_data()


def test_data_merger_strict_error_left(data_container_0, data_container_3):
    dm = DataMerger(
        data_containers=[data_container_0, data_container_3], on=StandardNames.doi
    )
    with pytest.raises(DataValidationError, match=r"doi_dc1"):
        dm.merge_data()


def test_data_merger_strict_false(data_container_2, data_container_3):
    dm = DataMerger(
        data_containers=[data_container_2, data_container_3],
        on=StandardNames.doi,
        strict=False,
    )
    dm.merge_data()
    assert list(dm.df[StandardNames.doi]) == [
        "10.11646/zootaxa.3772.1.1",
        np.nan,
        "10.1111/j.1463-6395.2006.00240.x",
        "10.1093/mollus/eyp029",
        np.nan,
    ]
    assert list(dm.df._merge_0) == [
        "left_only",
        "left_only",
        "both",
        "right_only",
        "right_only",
    ]


def test_data_merger_three_datasets(
    data_container_0, data_container_1, data_container_4
):
    dm = DataMerger(
        data_containers=[data_container_0, data_container_1, data_container_4],
        on=StandardNames.doi,
    )
    dm.merge_data()
    assert list(dm.df[StandardNames.doi]) == [
        "10.1111/j.1502-3931.2009.00166.x",
        "10.11646/zootaxa.3772.1.1",
        "10.1093/mollus/eyp029",
        "10.1111/j.1463-6395.2006.00240.x",
    ]
