from ror.client import RorClient


def test_search_affiliation():
    rc = RorClient()
    r = rc.search_affiliation(
        "School of Chemistry, University of Nottingham, Nottingham NG7 2RD, UK."
    )
    organisations = [x["organization"]["id"] for x in r["items"]]
    assert "https://ror.org/01ee9ar58" in organisations
