from dataclasses import dataclass
from typing import Optional

from utils.fieldset import FieldNames


@dataclass
class FreyaFieldNames(FieldNames):
    date_month: Optional[str] = None
    date_year: Optional[str] = None
    in_epmc: Optional[str] = None
    short_reference: Optional[str] = None
    member_state_collab: Optional[str] = None
    non_member_state_collab: Optional[str] = None
    embl_only: Optional[str] = None


FREYA_FIELDNAMES = FreyaFieldNames(
    doi="DOI",
    pubmed_id="PubMed ID",
    date_month="Date (month)",
    date_year="Date (year)",
    embl_authors="EMBL author(s) name",
    title="Paper title",
    journal_name="Full journal name",
    licence="License",
    in_epmc="In EPMC?",
    short_reference="Short reference",
    keywords="Keywords",
    member_state_collab="Collaboration with member state",
    non_member_state_collab="Collaboration with non-member state",
    embl_only="EMBL only publication",
    epmc_link="Clickable link to the paper",
)
