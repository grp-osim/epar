import local.config as conf
from export_parser.export_parser import ExportParser
from freya_ror_predictor.freya_fields import FREYA_FIELDNAMES


class FreyaRorPredictorExportParser(ExportParser):
    def __init__(self, input_file_path: str = None, **kwargs):
        """
        Initialises the parser by reading an Excel export from
        ROR predictor
        (https://gitlab.ebi.ac.uk/literature-services/public-projects/ROR-proto-EMBL).
        If input_file_path is not passed as an argument,
        an input path must be specified in a
        variable (freya_ror_predictor_xlsx) in config.py

        Args:
            input_file_path: path to the export file. If not passed,
                this is loaded from config.py
        """
        if input_file_path is None:
            input_file_path = conf.freya_ror_predictor_xlsx
        super().__init__(input_file_path, FREYA_FIELDNAMES, "_freya", **kwargs)

    @staticmethod
    def get_countries_of_collaboration(row):
        countries = list()
        for column in [
            "Australia",
            "Austria",
            "Belgium",
            "Croatia",
            "Czech Republic",
            "Denmark",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Hungary",
            "Iceland",
            "Ireland",
            "Israel",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Montenegro",
            "Netherlands",
            "Norway",
            "Poland",
            "Portugal",
            "Slovakia",
            "Spain",
            "Sweden",
            "Switzerland",
            "United Kingdom",
        ]:
            if int(row[column]) > 0:
                countries.append(column)
        return ";".join(sorted(countries)).replace("Czech Republic", "Czechia")
