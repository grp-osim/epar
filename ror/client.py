import requests
import urllib.parse
from http import HTTPStatus
from osim_utils.logger import get_logger

from utils.decorators import check_response, process_response


class RorClient:
    """
    API client for ROR REST API
    (https://ror.readme.io/docs/rest-api)
    """

    def __init__(self):
        self.base_url = "https://api.ror.org/organizations"
        self.session = requests.Session()
        self.logger = get_logger()

    @process_response
    @check_response(HTTPStatus.OK)
    def search_affiliation(self, query):
        params = {"affiliation": urllib.parse.quote(query)}
        return self.session.get(self.base_url, params=params)
