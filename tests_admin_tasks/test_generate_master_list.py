import numpy as np
import pandas as pd
from utils.fieldset import StandardNames as sn


def test_01():
    def resolve_pubmed(row):
        if pd.isna(row.pubmed_id):
            if epmc_result := doi_dict.get(row[sn.doi]):
                return epmc_result.get("pmid", np.nan)
            else:
                return np.nan
        return row.pubmed_id

    doi_dict = {
        "10.1016/j.fishres.2022.106231": {
            "id": "36798657",
            "source": "MED",
            "pmid": "36798657",
            "pmcid": "PMC7614180",
            "fullTextIdList": {"fullTextId": ["PMC7614180"]},
            "doi": "10.1016/j.fishres.2022.106231",
            "title": "Atlantic herring (<i>Clupea harengus</i>) population structure in the Northeast Atlantic Ocean.",
            "authorString": "Kongsstovu SÍ, Mikalsen SO, Homrum EÍ, Jacobsen JA, Als TD, Gislason H, Flicek P, Nielsen EE, Dahl HA.",
            "journalTitle": "Fish Res",
            "journalVolume": "249",
            "pubYear": "2022",
            "journalIssn": "0165-7836",
            "pageInfo": "106231",
            "pubType": "research-article; journal article",
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 0,
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasTMAccessionNumbers": "N",
            "firstIndexDate": "2023-02-17",
            "firstPublicationDate": "2022-05-01",
        },
        "10.1007/s10530-022-02846-y": {
            "id": "IND607935406",
            "source": "AGR",
            "doi": "10.1007/s10530-022-02846-y",
            "title": "Hybrid swarm as a result of hybridization between two alien and two native water frog species (genus Pelophylax) in Central Croatia: Crna Mlaka fishpond acting as a species melting pot?",
            "authorString": "Jelić M, Franjević D, Đikić D, Korlević P, Vucić M, Jelić D, Becking T, Grandjean F, Klobučar G.",
            "journalTitle": "Biol Invasions",
            "issue": "10",
            "journalVolume": "24",
            "pubYear": "2022",
            "journalIssn": "1387-3547; 1573-1464; ",
            "pageInfo": "3291-3304",
            "pubType": "journal article",
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasTMAccessionNumbers": "N",
            "firstIndexDate": "2022-10-06",
            "firstPublicationDate": "2022-10-01",
        },
    }
    df = pd.read_json("r2_only.json")
    df.pubmed_id = df.apply(resolve_pubmed, axis=1)
    # t_df = df.loc[df.doi.isin(doi_dict.keys())]
    # t_df.pubmed_id = t_df.apply(resolve_pubmed, axis=1)
