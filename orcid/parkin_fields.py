from utils.fieldset import FieldNames


PARKIN_FIELDNAMES = FieldNames(
    doi="doi",
    embl_job="position",
    embl_site="site",
    first_name="givenName",
    journal_name="journal",
    last_name="familyName",
    orcid="orcid",
    orcid_first_name="orcidGivenName",
    orcid_last_name="orcidFamilyName",
    pubmed_id="pmid",
    title="title",
    year="pubYear",
)
