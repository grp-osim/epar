import pandas as pd

import local.config as conf
from export_parser.export_parser import ExportParser
from orcid.parkin_fields import PARKIN_FIELDNAMES
from typing import Optional, Union
from utils.fieldset import StandardNames


class OrcidExportParser(ExportParser):
    def __init__(self, raw_file_path: str = None, **kwargs):
        """
        Initialises the parser by reading an Excel export from ORCID created
        by Michael Parkin's R script
        (https://gitlab.ebi.ac.uk/literature-services/development/embl_orcids;
        to request access to this repo, please contact the EuropePMC team).
        If raw_file_path is not passed as an argument,
        an input path must be specified in a
        variable (parkin_csv) in config.py

        Args:
            raw_file_path: path to the "YYYY_MM_DD_raw_data.csv" output file
                of Michael Parkin's ORCID R script.
                If not passed, this is loaded from config.py
            kwargs:
                pubs_file_path (str): path to the "YYYY_MM_DD_raw_data.csv" output file
                    of Michael Parkin's ORCID R script. If not passed, this is loaded from config.py
                year (str): only add items published in this year
        """
        self.year = kwargs.pop("year", None)
        pubs_file_path = kwargs.pop("pubs_file_path", None)
        if raw_file_path is None:
            raw_file_path = conf.parkin_orcid_csv
        kwargs["dtype_dict"] = {
            PARKIN_FIELDNAMES.year: str,
            PARKIN_FIELDNAMES.pubmed_id: str,
        }
        super().__init__(raw_file_path, PARKIN_FIELDNAMES, "_orcid", **kwargs)
        self.df.year = self.df.year.apply(
            lambda x: x.split(".")[0] if pd.notnull(x) else x
        )
        self.df.pubmed_id = self.df.pubmed_id.apply(
            lambda x: x.split(".")[0] if pd.notnull(x) else x
        )
        # throw away anything that doesn't have a DOI in the dataset
        self.df = self.df[pd.notnull(self.df.doi)]
        self.df.doi = self.df.doi.apply(str.lower)
        epmc_year = "pubYearEPMC"
        if pubs_file_path is None:
            pubs_file_path = conf.parkin_pub_csv
        pubs_df = pd.read_csv(pubs_file_path, dtype={epmc_year: str})
        self.df = self.df.merge(
            pubs_df[["doi", epmc_year]], how="outer", on="doi", indicator=True
        )
        if self.year:
            self.df = self.df[
                (self.df.year == self.year) | (self.df[epmc_year] == self.year)
            ]

        agg_funcs = {
            "orcid": lambda x: ",".join(x),
            **{col: "first" for col in self.df.columns if col not in ["orcid", "doi"]},
        }

        self.df = self.df.groupby("doi").agg(agg_funcs).reset_index()

    def get_orcid(self, **kwargs) -> Optional[Union[str, set]]:
        """
        Queries the dataset by StandardNames.first_name and/or StandardNames.last_name
        and returns an ORCID if found (None if not found)

        Args:
            **kwargs:
                StandardNames.first_name: First name of person we wish to find an ORCID for
                StandardNames.last_name: Last name of person we wish to find an ORCID for
        """
        first_name = kwargs.get(StandardNames.first_name)
        last_name = kwargs.get(StandardNames.last_name)
        if first_name and last_name:
            df = self.df[
                (
                    (self.df[StandardNames.last_name] == last_name)
                    | (self.df[StandardNames.orcid_last_name] == last_name)
                )
                & (
                    (self.df[StandardNames.first_name] == first_name)
                    | (self.df[StandardNames.orcid_first_name] == first_name)
                )
            ]

        elif last_name:
            df = self.df[
                (self.df[StandardNames.last_name] == last_name)
                | (self.df[StandardNames.orcid_last_name] == last_name)
            ]
        elif first_name:
            df = self.df[
                (self.df[StandardNames.first_name] == first_name)
                | (self.df[StandardNames.orcid_first_name] == first_name)
            ]
        else:
            return None
        orcids = set(df[StandardNames.orcid])
        if len(orcids) == 1:
            return orcids.pop()
        elif len(orcids) == 0:
            return None
        else:
            return orcids

    def is_orcid_in_dataset(self, orcid: str) -> bool:
        """
        Queries the dataset for the value of orcid and returns
        True if it is found; returns False otherwise

        Args:
            orcid: the ORCID we want to query
        """
        df = self.df[self.df[StandardNames.orcid] == orcid]
        if not df.empty:
            return True
        return False
