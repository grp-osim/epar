from __future__ import annotations
from typing import Union

import pandas as pd
from dataclasses import fields

from data_warehouse.warehouse_fields import WAREHOUSE_FIELDNAMES
from utils.data_container import DataContainer


class DataWarehouseParser(DataContainer):
    """
    Data container for DataWarehouse API results
    """

    def __init__(self, warehouse_results: Union[list[dict], pd.DataFrame]):
        self.fn = WAREHOUSE_FIELDNAMES
        if isinstance(warehouse_results, pd.DataFrame):
            df = warehouse_results
        else:
            df = pd.json_normalize(warehouse_results, max_level=1)
        super().__init__(df, "_warehouse")
        self.df = self.rename_df_columns(fieldnames=self.fn)
        fieldnames = [f.name for f in fields(self.fn)]
        drop_list = [x for x in self.df.columns if x not in fieldnames]
        self.df.drop(columns=drop_list, inplace=True)
