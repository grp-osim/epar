from utils.fieldset import FieldNames


WAREHOUSE_FIELDNAMES = FieldNames(
    authors="srcAuthors",
    book_info="springerBookInfo",
    conference_info="springerEventInfo",
    converis_id="id",
    doi="DOI",
    issue="cfIssue",
    journal_acronym="cfNameAbbrev",
    journal_name="srcJourName",
    pubmed_id="pubmedId",
    publication_date="cfResPublDate",
    publication_type="type",
    start_page="cfStartPage",
    volume="cfVol",
    title="cfTitle",
    year="publYear",
)
# Publication: Investigators' names: contains EMBL researchers in a consortium authorship
