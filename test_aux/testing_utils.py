import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
TEST_DATA_DIR = os.path.join(BASE_DIR, "test_aux")
CONVERIS_DATASET_0 = os.path.join(TEST_DATA_DIR, "Converis_all_info_2022_11_24.xlsx")
CONVERIS_DATASET_1 = os.path.join(TEST_DATA_DIR, "Converis_test_dataset_01.csv")
FACILITIES_DATASET_0 = os.path.join(
    TEST_DATA_DIR, "All services publications_2022_peer reviewed only.xlsx"
)
OASWITCHBOARD_DATASET_0 = os.path.join(
    TEST_DATA_DIR, "report_messages_20230619-124014.json"
)
PARKIN_DATASET_0_RAW = os.path.join(TEST_DATA_DIR, "2023_03_04_raw_data_anon.csv")
PARKIN_DATASET_0_PUBS = os.path.join(TEST_DATA_DIR, "2023_03_04_publications.csv")
FREYA_DATASET_0 = os.path.join(TEST_DATA_DIR, "freya_ror_predictor_output.xlsx")
WOS_DATASET_0 = os.path.join(TEST_DATA_DIR, "WoS_2022_EMBL_publications.xls")
WOS_DATASET_1 = os.path.join(TEST_DATA_DIR, "WoS_export_03.02.xls")
