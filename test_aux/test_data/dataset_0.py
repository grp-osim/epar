class EpmcData0:
    expected_data = [
        {
            "id": "36344848",
            "source": "MED",
            "pmid": "36344848",
            "pmcid": "PMC9663297",
            "fullTextIdList": {"fullTextId": ["PMC9663297"]},
            "doi": "10.1038/s41594-022-00849-w",
            "title": "A structural biology community assessment of AlphaFold2 applications.",
            "authorString": "Akdel M, Pires DEV, Pardo EP, Jänes J, Zalevsky AO, Mészáros B, Bryant P, Good LL, Laskowski RA, Pozzati G, Shenoy A, Zhu W, Kundrotas P, Serra VR, Rodrigues CHM, Dunham AS, Burke D, Borkakoti N, Velankar S, Frost A, Basquin J, Lindorff-Larsen K, Bateman A, Kajava AV, Valencia A, Ovchinnikov S, Durairaj J, Ascher DB, Thornton JM, Davey NE, Stein A, Elofsson A, Croll TI, Beltrao P.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Akdel M",
                        "firstName": "Mehmet",
                        "lastName": "Akdel",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Bioinformatics Group, Department of Plant Sciences, Wageningen University and Research, Wageningen, the Netherlands."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Pires DEV",
                        "firstName": "Douglas E V",
                        "lastName": "Pires",
                        "initials": "DEV",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Computing and Information Systems, University of Melbourne, Melbourne, Victoria, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Pardo EP",
                        "firstName": "Eduard Porta",
                        "lastName": "Pardo",
                        "initials": "EP",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Josep Carreras Leukaemia Research Institute (IJC), Badalona, Spain."
                                },
                                {
                                    "affiliation": "Barcelona Supercomputing Center (BSC), Barcelona, Spain."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Jänes J",
                        "firstName": "Jürgen",
                        "lastName": "Jänes",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2540-1236"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Zalevsky AO",
                        "firstName": "Arthur O",
                        "lastName": "Zalevsky",
                        "initials": "AO",
                        "authorId": {"type": "ORCID", "value": "0000-0001-6987-8119"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Shemyakin-Ovchinnikov Institute of Bioorganic Chemistry, Russian Academy of Sciences, Moscow, Russian Federation."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Mészáros B",
                        "firstName": "Bálint",
                        "lastName": "Mészáros",
                        "initials": "B",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, Heidelberg, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Bryant P",
                        "firstName": "Patrick",
                        "lastName": "Bryant",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0003-3439-1866"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Good LL",
                        "firstName": "Lydia L",
                        "lastName": "Good",
                        "initials": "LL",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5308-8542"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Linderstrøm-Lang Centre for Protein Science, Department of Biology, University of Copenhagen, Copenhagen, Denmark."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Laskowski RA",
                        "firstName": "Roman A",
                        "lastName": "Laskowski",
                        "initials": "RA",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5528-0087"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Pozzati G",
                        "firstName": "Gabriele",
                        "lastName": "Pozzati",
                        "initials": "G",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4303-9939"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Shenoy A",
                        "firstName": "Aditi",
                        "lastName": "Shenoy",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7748-2501"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Zhu W",
                        "firstName": "Wensi",
                        "lastName": "Zhu",
                        "initials": "W",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kundrotas P",
                        "firstName": "Petras",
                        "lastName": "Kundrotas",
                        "initials": "P",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Serra VR",
                        "firstName": "Victoria Ruiz",
                        "lastName": "Serra",
                        "initials": "VR",
                        "authorId": {"type": "ORCID", "value": "0000-0003-3991-0514"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Barcelona Supercomputing Center (BSC), Barcelona, Spain."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Rodrigues CHM",
                        "firstName": "Carlos H M",
                        "lastName": "Rodrigues",
                        "initials": "CHM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4420-6401"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Computing and Information Systems, University of Melbourne, Melbourne, Victoria, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Dunham AS",
                        "firstName": "Alistair S",
                        "lastName": "Dunham",
                        "initials": "AS",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9076-3025"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Burke D",
                        "firstName": "David",
                        "lastName": "Burke",
                        "initials": "D",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8830-3951"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Borkakoti N",
                        "firstName": "Neera",
                        "lastName": "Borkakoti",
                        "initials": "N",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Velankar S",
                        "firstName": "Sameer",
                        "lastName": "Velankar",
                        "initials": "S",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8439-5964"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Frost A",
                        "firstName": "Adam",
                        "lastName": "Frost",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2231-2577"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biochemistry and Biophysics University of California, San Francisco, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Basquin J",
                        "firstName": "Jérôme",
                        "lastName": "Basquin",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Structural Cell Biology, Max Planck Institute of Biochemistry, Martinsried, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Lindorff-Larsen K",
                        "firstName": "Kresten",
                        "lastName": "Lindorff-Larsen",
                        "initials": "K",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4750-6039"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Linderstrøm-Lang Centre for Protein Science, Department of Biology, University of Copenhagen, Copenhagen, Denmark."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Bateman A",
                        "firstName": "Alex",
                        "lastName": "Bateman",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6982-4660"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kajava AV",
                        "firstName": "Andrey V",
                        "lastName": "Kajava",
                        "initials": "AV",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2342-6886"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Université de Montpellier, Centre de Recherche en Biologie Cellulaire de Montpellier (CRBM) CNRS, Montpellier, France."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Valencia A",
                        "firstName": "Alfonso",
                        "lastName": "Valencia",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8937-6789"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Barcelona Supercomputing Center (BSC), Barcelona, Spain. alfonso.valencia@bsc.es."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ovchinnikov S",
                        "firstName": "Sergey",
                        "lastName": "Ovchinnikov",
                        "initials": "S",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2774-2744"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Faculty of Arts and Sciences, Division of Science, Harvard University, Cambridge, MA, USA. so@fas.harvard.edu."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Durairaj J",
                        "firstName": "Janani",
                        "lastName": "Durairaj",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1698-4556"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Biozentrum, University of Basel, Basel, Switzerland. janani.durairaj@gmail.com."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ascher DB",
                        "firstName": "David B",
                        "lastName": "Ascher",
                        "initials": "DB",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2948-2413"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Chemistry and Molecular Biology, University of Queensland, Brisbane, Queensland, Australia. d.ascher@uq.edu.au."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Thornton JM",
                        "firstName": "Janet M",
                        "lastName": "Thornton",
                        "initials": "JM",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0824-4096"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK. thornton@ebi.ac.uk."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Davey NE",
                        "firstName": "Norman E",
                        "lastName": "Davey",
                        "initials": "NE",
                        "authorId": {"type": "ORCID", "value": "0000-0001-6988-4850"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Cancer Research, London, UK. norman.davey@icr.ac.uk."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Stein A",
                        "firstName": "Amelie",
                        "lastName": "Stein",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5862-1681"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Linderstrøm-Lang Centre for Protein Science, Department of Biology, University of Copenhagen, Copenhagen, Denmark. amelie.stein@bio.ku.dk."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Elofsson A",
                        "firstName": "Arne",
                        "lastName": "Elofsson",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-7115-9751"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dep of Biochemistry and Biophysics and Science for Life Laboratory, Solna, Sweden. arne@bioinfo.se."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Croll TI",
                        "firstName": "Tristan I",
                        "lastName": "Croll",
                        "initials": "TI",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3514-8377"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Cambridge Institute for Medical Research, Department of Haematology, The University of Cambridge, Cambridge, UK. tic20@cam.ac.uk."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Beltrao P",
                        "firstName": "Pedro",
                        "lastName": "Beltrao",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2724-7703"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute (EMBL-EBI), Cambridge, UK. pbeltrao@ethz.ch."
                                },
                                {
                                    "affiliation": "Institute of Molecular Systems Biology, ETH Zürich, Zürich, Switzerland. pbeltrao@ethz.ch."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5308-8542"},
                    {"type": "ORCID", "value": "0000-0001-5528-0087"},
                    {"type": "ORCID", "value": "0000-0001-6987-8119"},
                    {"type": "ORCID", "value": "0000-0001-6988-4850"},
                    {"type": "ORCID", "value": "0000-0001-7748-2501"},
                    {"type": "ORCID", "value": "0000-0001-8830-3951"},
                    {"type": "ORCID", "value": "0000-0001-9076-3025"},
                    {"type": "ORCID", "value": "0000-0002-1698-4556"},
                    {"type": "ORCID", "value": "0000-0002-2342-6886"},
                    {"type": "ORCID", "value": "0000-0002-2540-1236"},
                    {"type": "ORCID", "value": "0000-0002-2724-7703"},
                    {"type": "ORCID", "value": "0000-0002-3514-8377"},
                    {"type": "ORCID", "value": "0000-0002-4303-9939"},
                    {"type": "ORCID", "value": "0000-0002-4420-6401"},
                    {"type": "ORCID", "value": "0000-0002-4750-6039"},
                    {"type": "ORCID", "value": "0000-0002-5862-1681"},
                    {"type": "ORCID", "value": "0000-0002-6982-4660"},
                    {"type": "ORCID", "value": "0000-0002-7115-9751"},
                    {"type": "ORCID", "value": "0000-0002-8439-5964"},
                    {"type": "ORCID", "value": "0000-0002-8937-6789"},
                    {"type": "ORCID", "value": "0000-0003-0824-4096"},
                    {"type": "ORCID", "value": "0000-0003-2231-2577"},
                    {"type": "ORCID", "value": "0000-0003-2774-2744"},
                    {"type": "ORCID", "value": "0000-0003-2948-2413"},
                    {"type": "ORCID", "value": "0000-0003-3439-1866"},
                    {"type": "ORCID", "value": "0000-0003-3991-0514"},
                ]
            },
            "dataLinksTagsList": {
                "dataLinkstag": ["altmetrics", "recommended_reviews", "supporting_data"]
            },
            "journalInfo": {
                "issue": "11",
                "volume": "29",
                "journalIssueId": 3457795,
                "dateOfPublication": "2022 Nov",
                "monthOfPublication": 11,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-11-01",
                "journal": {
                    "title": "Nature structural & molecular biology",
                    "medlineAbbreviation": "Nat Struct Mol Biol",
                    "issn": "1545-9993",
                    "essn": "1545-9985",
                    "isoabbreviation": "Nat Struct Mol Biol",
                    "nlmid": "101186374",
                },
            },
            "pubYear": "2022",
            "pageInfo": "1056-1067",
            "abstractText": "Most proteins fold into 3D structures that determine how they function and orchestrate the biological processes of the cell. Recent developments in computational methods for protein structure predictions have reached the accuracy of experimentally determined models. Although this has been independently verified, the implementation of these methods across structural-biology applications remains to be tested. Here, we evaluate the use of AlphaFold2 (AF2) predictions in the study of characteristic structural elements; the impact of missense variants; function and ligand binding site predictions; modeling of interactions; and modeling of experimental structural data. For 11 proteomes, an average of 25% additional residues can be confidently modeled when compared with homology modeling, identifying structural features rarely seen in the Protein Data Bank. AF2-based predictions of protein disorder and complexes surpass dedicated tools, and AF2 models can be used across diverse applications equally well compared with experimentally determined structures, when the confidence metrics are critically considered. In summary, we find that these advances are likely to have a transformative impact in structural biology and broader life-science research.",
            "affiliation": "Bioinformatics Group, Department of Plant Sciences, Wageningen University and Research, Wageningen, the Netherlands.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": [
                    "Research Support, Non-U.S. Gov't",
                    "research-article",
                    "Journal Article",
                ]
            },
            "grantsList": {
                "grant": [{"agency": "Wellcome Trust", "acronym": "WT_", "orderIn": 0}]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "Y", "descriptorName": "Furylfuramide"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Proteins",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Computational Biology",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Binding Sites"},
                    {"majorTopic_YN": "N", "descriptorName": "Protein Conformation"},
                    {"majorTopic_YN": "N", "descriptorName": "Databases, Protein"},
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "Proteins", "registryNumber": "0"},
                    {"name": "Furylfuramide", "registryNumber": "054NR2135Y"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1038/s41594-022-00849-w",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9663297",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9663297?pdf=render",
                    },
                ]
            },
            "commentCorrectionList": {
                "commentCorrection": [
                    {
                        "id": "PPR400415",
                        "source": "PPR",
                        "type": "Preprint in",
                        "note": "Link created based on a title-first author match",
                        "orderIn": 10002,
                    }
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 6,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "Y",
            "tmAccessionTypeList": {
                "accessionType": ["pfam", "emdb", "uniprot", "pdb"]
            },
            "dateOfCompletion": "2022-11-16",
            "dateOfCreation": "2022-11-07",
            "firstIndexDate": "2022-11-09",
            "fullTextReceivedDate": "2022-11-21",
            "dateOfRevision": "2022-12-22",
            "electronicPublicationDate": "2022-11-07",
            "firstPublicationDate": "2022-11-07",
        },
        {
            "id": "36289080",
            "source": "MED",
            "pmid": "36289080",
            "pmcid": "PMC9675693",
            "fullTextIdList": {"fullTextId": ["PMC9675693"]},
            "doi": "10.1007/s00249-022-01620-1",
            "title": "Production and characterisation of modularly deuterated UBE2D1-Ub conjugate by small angle neutron and X-ray scattering.",
            "authorString": "Pietras Z, Duff AP, Morad V, Wood K, Jeffries CM, Sunnerhagen M.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Pietras Z",
                        "firstName": "Zuzanna",
                        "lastName": "Pietras",
                        "initials": "Z",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Physics, Chemistry and Biology, Division of Chemistry, Linköping University, 581 83, Linköping, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Duff AP",
                        "firstName": "Anthony P",
                        "lastName": "Duff",
                        "initials": "AP",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation (ANSTO), New Illawarra Road, Lucas Heights, NSW, 2234, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Morad V",
                        "firstName": "Vivian",
                        "lastName": "Morad",
                        "initials": "V",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Physics, Chemistry and Biology, Division of Chemistry, Linköping University, 581 83, Linköping, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wood K",
                        "firstName": "Kathleen",
                        "lastName": "Wood",
                        "initials": "K",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation (ANSTO), New Illawarra Road, Lucas Heights, NSW, 2234, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jeffries CM",
                        "firstName": "Cy M",
                        "lastName": "Jeffries",
                        "initials": "CM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8718-7343"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory (EMBL) Hamburg Site, 22607, Hamburg, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sunnerhagen M",
                        "firstName": "Maria",
                        "lastName": "Sunnerhagen",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0492-5890"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Physics, Chemistry and Biology, Division of Chemistry, Linköping University, 581 83, Linköping, Sweden. maria.sunnerhagen@liu.se."
                                }
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0002-0492-5890"},
                    {"type": "ORCID", "value": "0000-0002-8718-7343"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["full_text", "supporting_data"]},
            "journalInfo": {
                "issue": "7-8",
                "volume": "51",
                "journalIssueId": 3460267,
                "dateOfPublication": "2022 Dec",
                "monthOfPublication": 12,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-12-01",
                "journal": {
                    "title": "European biophysics journal : EBJ",
                    "medlineAbbreviation": "Eur Biophys J",
                    "issn": "0175-7571",
                    "essn": "1432-1017",
                    "isoabbreviation": "Eur Biophys J",
                    "nlmid": "8409413",
                },
            },
            "pubYear": "2022",
            "pageInfo": "569-577",
            "abstractText": "This structural study exploits the possibility to use modular protein deuteration to facilitate the study of ubiquitin signalling, transfer, and modification. A protein conjugation reaction is used to combine protonated E2 enzyme with deuterated ubiquitin for small angle X-ray and neutron scattering with neutron contrast variation. The combined biomolecules stay as a monodisperse system during data collection in both protonated and deuterated buffers indicating long stability of the E2-Ub conjugate. With multiphase ab initio shape restoration and rigid body modelling, we reconstructed the shape of a E2-Ub-conjugated complex of UBE2D1 linked to ubiquitin via an isopeptide bond. Solution X-ray and neutron scattering data for this E2-Ub conjugate in the absence of E3 jointly indicate an ensemble of open and backbent states, with a preference for the latter in solution. The approach of combining protonated and labelled proteins can be used for solution studies to assess localization and movement of ubiquitin and could be widely applied to modular Ub systems in general.",
            "affiliation": "Department of Physics, Chemistry and Biology, Division of Chemistry, Linköping University, 581 83, Linköping, Sweden.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {"pubType": ["research-article", "Journal Article"]},
            "grantsList": {
                "grant": [
                    {
                        "grantId": "P7227",
                        "agency": "Australian Nuclear Science and Technology Organisation",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "NDF7226",
                        "agency": "Australian Nuclear Science and Technology Organisation",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "2018-04392",
                        "agency": "Vetenskapsrådet",
                        "orderIn": 0,
                    },
                    {"agency": "Linköping University", "orderIn": 0},
                    {
                        "grantId": "GSn15-0008",
                        "agency": "Stiftelsen för\xa0Strategisk Forskning",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Ubiquitin-Conjugating Enzymes",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                },
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                },
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Ubiquitin",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                },
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                },
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Neutrons"},
                    {"majorTopic_YN": "N", "descriptorName": "X-Rays"},
                    {"majorTopic_YN": "N", "descriptorName": "Models, Molecular"},
                    {"majorTopic_YN": "N", "descriptorName": "Scattering, Small Angle"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Ubiquitination",
                    "Small angle X-ray scattering",
                    "Small Angle Neutron Scattering",
                    "Protein Conjugation",
                    "Protein Deuteration",
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "Ubiquitin", "registryNumber": "0"},
                    {
                        "name": "Ubiquitin-Conjugating Enzymes",
                        "registryNumber": "EC 2.3.2.23",
                    },
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1007/s00249-022-01620-1",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9675693",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9675693?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 0,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "Y",
            "tmAccessionTypeList": {"accessionType": ["pdb"]},
            "dateOfCompletion": "2022-11-22",
            "dateOfCreation": "2022-10-26",
            "firstIndexDate": "2022-10-27",
            "fullTextReceivedDate": "2022-11-28",
            "dateOfRevision": "2022-11-22",
            "electronicPublicationDate": "2022-10-26",
            "firstPublicationDate": "2022-10-26",
        },
        {
            "id": "36261518",
            "source": "MED",
            "pmid": "36261518",
            "pmcid": "PMC9668749",
            "fullTextIdList": {"fullTextId": ["PMC9668749"]},
            "doi": "10.1038/s41586-022-05325-5",
            "title": "Semi-automated assembly of high-quality diploid human reference genomes.",
            "authorString": "Jarvis ED, Formenti G, Rhie A, Guarracino A, Yang C, Wood J, Tracey A, Thibaud-Nissen F, Vollger MR, Porubsky D, Cheng H, Asri M, Logsdon GA, Carnevali P, Chaisson MJP, Chin CS, Cody S, Collins J, Ebert P, Escalona M, Fedrigo O, Fulton RS, Fulton LL, Garg S, Gerton JL, Ghurye J, Granat A, Green RE, Harvey W, Hasenfeld P, Hastie A, Haukness M, Jaeger EB, Jain M, Kirsche M, Kolmogorov M, Korbel JO, Koren S, Korlach J, Lee J, Li D, Lindsay T, Lucas J, Luo F, Marschall T, Mitchell MW, McDaniel J, Nie F, Olsen HE, Olson ND, Pesout T, Potapova T, Puiu D, Regier A, Ruan J, Salzberg SL, Sanders AD, Schatz MC, Schmitt A, Schneider VA, Selvaraj S, Shafin K, Shumate A, Stitziel NO, Stober C, Torrance J, Wagner J, Wang J, Wenger A, Xiao C, Zimin AV, Zhang G, Wang T, Li H, Garrison E, Haussler D, Hall I, Zook JM, Eichler EE, Phillippy AM, Paten B, Howe K, Miga KH, Human Pangenome Reference Consortium.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Jarvis ED",
                        "firstName": "Erich D",
                        "lastName": "Jarvis",
                        "initials": "ED",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8931-5049"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Vertebrate Genome Laboratory, The Rockefeller University, New York, NY, USA. ejarvis@rockefeller.edu."
                                },
                                {
                                    "affiliation": "Howard Hughes Medical Institute, Chevy Chase, MD, USA. ejarvis@rockefeller.edu."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Formenti G",
                        "firstName": "Giulio",
                        "lastName": "Formenti",
                        "initials": "G",
                        "authorId": {"type": "ORCID", "value": "0000-0002-7554-5991"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Vertebrate Genome Laboratory, The Rockefeller University, New York, NY, USA. gformenti@rockefeller.edu."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Rhie A",
                        "firstName": "Arang",
                        "lastName": "Rhie",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Genome Informatics Section, Computational and Statistical Genomics Branch, National Human Genome Research Institute, National Institutes of Health, Bethesda, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Guarracino A",
                        "firstName": "Andrea",
                        "lastName": "Guarracino",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9744-131X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Genomics Research Centre, Human Technopole, Viale Rita Levi-Montalcini, Milan, Italy."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Yang C",
                        "firstName": "Chentao",
                        "lastName": "Yang",
                        "initials": "C",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "BGI-Shenzhen, Shenzhen, China."}
                            ]
                        },
                    },
                    {
                        "fullName": "Wood J",
                        "firstName": "Jonathan",
                        "lastName": "Wood",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-7545-2162"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Tree of Life, Wellcome Sanger Institute, Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Tracey A",
                        "firstName": "Alan",
                        "lastName": "Tracey",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4805-9058"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Tree of Life, Wellcome Sanger Institute, Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Thibaud-Nissen F",
                        "firstName": "Francoise",
                        "lastName": "Thibaud-Nissen",
                        "initials": "F",
                        "authorId": {"type": "ORCID", "value": "0000-0003-4957-7807"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "National Center for Biotechnology Information, National Library of Medicine, National Institutes of Health, Bethesda, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Vollger MR",
                        "firstName": "Mitchell R",
                        "lastName": "Vollger",
                        "initials": "MR",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8651-1615"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genome Sciences, University of Washington School of Medicine, Seattle, WA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Porubsky D",
                        "firstName": "David",
                        "lastName": "Porubsky",
                        "initials": "D",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8414-8966"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genome Sciences, University of Washington School of Medicine, Seattle, WA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Cheng H",
                        "firstName": "Haoyu",
                        "lastName": "Cheng",
                        "initials": "H",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Data Sciences, Dana-Farber Cancer Institute, Boston, MA, USA."
                                },
                                {
                                    "affiliation": "Department of Biomedical Informatics, Harvard Medical School, Boston, MA, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Asri M",
                        "firstName": "Mobin",
                        "lastName": "Asri",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Logsdon GA",
                        "firstName": "Glennis A",
                        "lastName": "Logsdon",
                        "initials": "GA",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2396-0656"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genome Sciences, University of Washington School of Medicine, Seattle, WA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Carnevali P",
                        "firstName": "Paolo",
                        "lastName": "Carnevali",
                        "initials": "P",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Chan Zuckerberg Initiative, Redwood City, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Chaisson MJP",
                        "firstName": "Mark J P",
                        "lastName": "Chaisson",
                        "initials": "MJP",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5395-1457"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Quantitative and Computational Biology, University of Southern California, Los Angeles, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Chin CS",
                        "firstName": "Chen-Shan",
                        "lastName": "Chin",
                        "initials": "CS",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Foundation for Biological Data Science, Belmont, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Cody S",
                        "firstName": "Sarah",
                        "lastName": "Cody",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Collins J",
                        "firstName": "Joanna",
                        "lastName": "Collins",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Tree of Life, Wellcome Sanger Institute, Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ebert P",
                        "firstName": "Peter",
                        "lastName": "Ebert",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7441-532X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute for Medical Biometry and Bioinformatics, Medical Faculty, Heinrich Heine University, Düsseldorf, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Escalona M",
                        "firstName": "Merly",
                        "lastName": "Escalona",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0213-4777"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomolecular Engineering, University of California Santa Cruz, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Fedrigo O",
                        "firstName": "Olivier",
                        "lastName": "Fedrigo",
                        "initials": "O",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6450-7551"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Vertebrate Genome Laboratory, The Rockefeller University, New York, NY, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Fulton RS",
                        "firstName": "Robert S",
                        "lastName": "Fulton",
                        "initials": "RS",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Fulton LL",
                        "firstName": "Lucinda L",
                        "lastName": "Fulton",
                        "initials": "LL",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Garg S",
                        "firstName": "Shilpa",
                        "lastName": "Garg",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biology, University of Copenhagen, Copenhagen, Denmark."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Gerton JL",
                        "firstName": "Jennifer L",
                        "lastName": "Gerton",
                        "initials": "JL",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0743-3637"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Stowers Institute for Medical Research, Kansas City, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ghurye J",
                        "firstName": "Jay",
                        "lastName": "Ghurye",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Dovetail Genomics, Scotts Valley, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Granat A",
                        "firstName": "Anastasiya",
                        "lastName": "Granat",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Illumina, Inc., San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Green RE",
                        "firstName": "Richard E",
                        "lastName": "Green",
                        "initials": "RE",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0516-5827"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Harvey W",
                        "firstName": "William",
                        "lastName": "Harvey",
                        "initials": "W",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genome Sciences, University of Washington School of Medicine, Seattle, WA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hasenfeld P",
                        "firstName": "Patrick",
                        "lastName": "Hasenfeld",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2319-2482"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, Genome Biology Unit, Heidelberg, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hastie A",
                        "firstName": "Alex",
                        "lastName": "Hastie",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Bionano Genomics, San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Haukness M",
                        "firstName": "Marina",
                        "lastName": "Haukness",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9991-8089"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jaeger EB",
                        "firstName": "Erich B",
                        "lastName": "Jaeger",
                        "initials": "EB",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Illumina, Inc., San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Jain M",
                        "firstName": "Miten",
                        "lastName": "Jain",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kirsche M",
                        "firstName": "Melanie",
                        "lastName": "Kirsche",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Computer Science, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kolmogorov M",
                        "firstName": "Mikhail",
                        "lastName": "Kolmogorov",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5489-9045"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Computer Science and Engineering, University of California San Diego, La Jolla, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Korbel JO",
                        "firstName": "Jan O",
                        "lastName": "Korbel",
                        "initials": "JO",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2798-3794"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, Genome Biology Unit, Heidelberg, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Koren S",
                        "firstName": "Sergey",
                        "lastName": "Koren",
                        "initials": "S",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1472-8962"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Genome Informatics Section, Computational and Statistical Genomics Branch, National Human Genome Research Institute, National Institutes of Health, Bethesda, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Korlach J",
                        "firstName": "Jonas",
                        "lastName": "Korlach",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0003-3047-4250"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Pacific Biosciences, Menlo Park, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Lee J",
                        "firstName": "Joyce",
                        "lastName": "Lee",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3492-1102"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Bionano Genomics, San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Li D",
                        "firstName": "Daofeng",
                        "lastName": "Li",
                        "initials": "D",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7492-3703"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genetics, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                                {
                                    "affiliation": "The Edison Family Center for Genome Sciences and Systems Biology, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Lindsay T",
                        "firstName": "Tina",
                        "lastName": "Lindsay",
                        "initials": "T",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Lucas J",
                        "firstName": "Julian",
                        "lastName": "Lucas",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Luo F",
                        "firstName": "Feng",
                        "lastName": "Luo",
                        "initials": "F",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4813-2403"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Computing, Clemson University, Clemson, SC, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Marschall T",
                        "firstName": "Tobias",
                        "lastName": "Marschall",
                        "initials": "T",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9376-1030"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute for Medical Biometry and Bioinformatics, Medical Faculty, Heinrich Heine University, Düsseldorf, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Mitchell MW",
                        "firstName": "Matthew W",
                        "lastName": "Mitchell",
                        "initials": "MW",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Coriell Institute for Medical Research, Camden, NJ, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "McDaniel J",
                        "firstName": "Jennifer",
                        "lastName": "McDaniel",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0003-1987-0914"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Material Measurement Laboratory, National Institute of Standards and Technology, Gaithersburg, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Nie F",
                        "firstName": "Fan",
                        "lastName": "Nie",
                        "initials": "F",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Hunan Provincial Key Lab on Bioinformatics, School of Computer Science and Engineering, Central South University, Changsha, China."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Olsen HE",
                        "firstName": "Hugh E",
                        "lastName": "Olsen",
                        "initials": "HE",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Olson ND",
                        "firstName": "Nathan D",
                        "lastName": "Olson",
                        "initials": "ND",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2585-3037"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Material Measurement Laboratory, National Institute of Standards and Technology, Gaithersburg, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Pesout T",
                        "firstName": "Trevor",
                        "lastName": "Pesout",
                        "initials": "T",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Potapova T",
                        "firstName": "Tamara",
                        "lastName": "Potapova",
                        "initials": "T",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Stowers Institute for Medical Research, Kansas City, MO, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Puiu D",
                        "firstName": "Daniela",
                        "lastName": "Puiu",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Engineering, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Regier A",
                        "firstName": "Allison",
                        "lastName": "Regier",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "DNAnexus, Mountain View, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Ruan J",
                        "firstName": "Jue",
                        "lastName": "Ruan",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Agricultural Genomics Institute, Chinese Academy of Agricultural Sciences, Shenzhen, China."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Salzberg SL",
                        "firstName": "Steven L",
                        "lastName": "Salzberg",
                        "initials": "SL",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Engineering, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sanders AD",
                        "firstName": "Ashley D",
                        "lastName": "Sanders",
                        "initials": "AD",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Berlin Institute for Medical Systems Biology, Max Delbrück Center for Molecular Medicine in the Helmholtz Association (MDC), Berlin, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Schatz MC",
                        "firstName": "Michael C",
                        "lastName": "Schatz",
                        "initials": "MC",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Computer Science, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Schmitt A",
                        "firstName": "Anthony",
                        "lastName": "Schmitt",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Arima Genomics, San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Schneider VA",
                        "firstName": "Valerie A",
                        "lastName": "Schneider",
                        "initials": "VA",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "National Center for Biotechnology Information, National Library of Medicine, National Institutes of Health, Bethesda, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Selvaraj S",
                        "firstName": "Siddarth",
                        "lastName": "Selvaraj",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {"affiliation": "Arima Genomics, San Diego, CA, USA."}
                            ]
                        },
                    },
                    {
                        "fullName": "Shafin K",
                        "firstName": "Kishwar",
                        "lastName": "Shafin",
                        "initials": "K",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5252-3434"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Shumate A",
                        "firstName": "Alaina",
                        "lastName": "Shumate",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Engineering, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Stitziel NO",
                        "firstName": "Nathan O",
                        "lastName": "Stitziel",
                        "initials": "NO",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4963-8211"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                                {
                                    "affiliation": "Department of Genetics, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                                {
                                    "affiliation": "Cardiovascular Division, John T. Milliken Department of Internal Medicine, Washington University School of Medicine, St. Louis, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Stober C",
                        "firstName": "Catherine",
                        "lastName": "Stober",
                        "initials": "C",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, Genome Biology Unit, Heidelberg, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Torrance J",
                        "firstName": "James",
                        "lastName": "Torrance",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Tree of Life, Wellcome Sanger Institute, Cambridge, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wagner J",
                        "firstName": "Justin",
                        "lastName": "Wagner",
                        "initials": "J",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Material Measurement Laboratory, National Institute of Standards and Technology, Gaithersburg, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wang J",
                        "firstName": "Jianxin",
                        "lastName": "Wang",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0003-1516-0480"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Hunan Provincial Key Lab on Bioinformatics, School of Computer Science and Engineering, Central South University, Changsha, China."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wenger A",
                        "firstName": "Aaron",
                        "lastName": "Wenger",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Pacific Biosciences, Menlo Park, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Xiao C",
                        "firstName": "Chuanle",
                        "lastName": "Xiao",
                        "initials": "C",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4680-0682"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "State Key Laboratory of Ophthalmology, Zhongshan Ophthalmic Center, Sun Yat-sen University, Guangzhou, China."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Zimin AV",
                        "firstName": "Aleksey V",
                        "lastName": "Zimin",
                        "initials": "AV",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Engineering, Johns Hopkins University, Baltimore, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Zhang G",
                        "firstName": "Guojie",
                        "lastName": "Zhang",
                        "initials": "G",
                        "authorId": {"type": "ORCID", "value": "0000-0001-6860-1521"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Evolutionary & Organismal Biology, Zhejiang University School of Medicine, Hangzhou, China."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wang T",
                        "firstName": "Ting",
                        "lastName": "Wang",
                        "initials": "T",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6800-242X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "McDonnell Genome Institute, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                                {
                                    "affiliation": "Department of Genetics, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                                {
                                    "affiliation": "The Edison Family Center for Genome Sciences and Systems Biology, Washington University School of Medicine, St. Louis, MO, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Li H",
                        "firstName": "Heng",
                        "lastName": "Li",
                        "initials": "H",
                        "authorId": {"type": "ORCID", "value": "0000-0003-4874-2874"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Data Sciences, Dana-Farber Cancer Institute, Boston, MA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Garrison E",
                        "firstName": "Erik",
                        "lastName": "Garrison",
                        "initials": "E",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Genetics, Genomics and Informatics, University of Tennessee Health Science Center, Memphis, TN, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Haussler D",
                        "firstName": "David",
                        "lastName": "Haussler",
                        "initials": "D",
                        "authorId": {"type": "ORCID", "value": "0000-0003-1533-4575"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Howard Hughes Medical Institute, Chevy Chase, MD, USA."
                                },
                                {
                                    "affiliation": "Department of Ecology and Evolutionary Biology, University of California Santa Cruz, Santa Cruz, CA, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Hall I",
                        "firstName": "Ira",
                        "lastName": "Hall",
                        "initials": "I",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Yale School of Medicine, New Haven, CT, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Zook JM",
                        "firstName": "Justin M",
                        "lastName": "Zook",
                        "initials": "JM",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Material Measurement Laboratory, National Institute of Standards and Technology, Gaithersburg, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Eichler EE",
                        "firstName": "Evan E",
                        "lastName": "Eichler",
                        "initials": "EE",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8246-4014"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Howard Hughes Medical Institute, Chevy Chase, MD, USA."
                                },
                                {
                                    "affiliation": "Department of Genome Sciences, University of Washington School of Medicine, Seattle, WA, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Phillippy AM",
                        "firstName": "Adam M",
                        "lastName": "Phillippy",
                        "initials": "AM",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2983-8934"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Genome Informatics Section, Computational and Statistical Genomics Branch, National Human Genome Research Institute, National Institutes of Health, Bethesda, MD, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Paten B",
                        "firstName": "Benedict",
                        "lastName": "Paten",
                        "initials": "B",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Howe K",
                        "firstName": "Kerstin",
                        "lastName": "Howe",
                        "initials": "K",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2237-513X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Tree of Life, Wellcome Sanger Institute, Cambridge, UK. kj2@sanger.ac.uk."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Miga KH",
                        "firstName": "Karen H",
                        "lastName": "Miga",
                        "initials": "KH",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "UC Santa Cruz Genomics Institute, University of California, Santa Cruz, CA, USA. khmiga@ucsc.edu."
                                }
                            ]
                        },
                    },
                    {"collectiveName": "Human Pangenome Reference Consortium"},
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5252-3434"},
                    {"type": "ORCID", "value": "0000-0001-5395-1457"},
                    {"type": "ORCID", "value": "0000-0001-6860-1521"},
                    {"type": "ORCID", "value": "0000-0001-7441-532X"},
                    {"type": "ORCID", "value": "0000-0001-7492-3703"},
                    {"type": "ORCID", "value": "0000-0001-8414-8966"},
                    {"type": "ORCID", "value": "0000-0001-8931-5049"},
                    {"type": "ORCID", "value": "0000-0001-9744-131X"},
                    {"type": "ORCID", "value": "0000-0001-9991-8089"},
                    {"type": "ORCID", "value": "0000-0002-1472-8962"},
                    {"type": "ORCID", "value": "0000-0002-2798-3794"},
                    {"type": "ORCID", "value": "0000-0002-3492-1102"},
                    {"type": "ORCID", "value": "0000-0002-4680-0682"},
                    {"type": "ORCID", "value": "0000-0002-4805-9058"},
                    {"type": "ORCID", "value": "0000-0002-4813-2403"},
                    {"type": "ORCID", "value": "0000-0002-4963-8211"},
                    {"type": "ORCID", "value": "0000-0002-5489-9045"},
                    {"type": "ORCID", "value": "0000-0002-6450-7551"},
                    {"type": "ORCID", "value": "0000-0002-6800-242X"},
                    {"type": "ORCID", "value": "0000-0002-7545-2162"},
                    {"type": "ORCID", "value": "0000-0002-7554-5991"},
                    {"type": "ORCID", "value": "0000-0002-8246-4014"},
                    {"type": "ORCID", "value": "0000-0002-8651-1615"},
                    {"type": "ORCID", "value": "0000-0002-9376-1030"},
                    {"type": "ORCID", "value": "0000-0003-0213-4777"},
                    {"type": "ORCID", "value": "0000-0003-0516-5827"},
                    {"type": "ORCID", "value": "0000-0003-0743-3637"},
                    {"type": "ORCID", "value": "0000-0003-1516-0480"},
                    {"type": "ORCID", "value": "0000-0003-1533-4575"},
                    {"type": "ORCID", "value": "0000-0003-1987-0914"},
                    {"type": "ORCID", "value": "0000-0003-2237-513X"},
                    {"type": "ORCID", "value": "0000-0003-2319-2482"},
                    {"type": "ORCID", "value": "0000-0003-2396-0656"},
                    {"type": "ORCID", "value": "0000-0003-2585-3037"},
                    {"type": "ORCID", "value": "0000-0003-2983-8934"},
                    {"type": "ORCID", "value": "0000-0003-3047-4250"},
                    {"type": "ORCID", "value": "0000-0003-4874-2874"},
                    {"type": "ORCID", "value": "0000-0003-4957-7807"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics", "supporting_data"]},
            "journalInfo": {
                "issue": "7936",
                "volume": "611",
                "journalIssueId": 3457594,
                "dateOfPublication": "2022 Nov",
                "monthOfPublication": 11,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-11-01",
                "journal": {
                    "title": "Nature",
                    "medlineAbbreviation": "Nature",
                    "issn": "0028-0836",
                    "essn": "1476-4687",
                    "isoabbreviation": "Nature",
                    "nlmid": "0410462",
                },
            },
            "pubYear": "2022",
            "pageInfo": "519-531",
            "abstractText": "The current human reference genome, GRCh38, represents over 20 years of effort to generate a high-quality assembly, which has benefitted society<sup>1,2</sup>. However, it still has many gaps and errors, and does not represent a biological genome as it is a blend of multiple individuals<sup>3,4</sup>. Recently, a high-quality telomere-to-telomere reference, CHM13, was generated with the latest long-read technologies, but it was derived from a hydatidiform mole cell line with a nearly homozygous genome<sup>5</sup>. To address these limitations, the Human Pangenome Reference Consortium formed with the goal of creating high-quality, cost-effective, diploid genome assemblies for a pangenome reference that represents human genetic diversity<sup>6</sup>. Here, in our first scientific report, we determined which combination of current genome sequencing and assembly approaches yield the most complete and accurate diploid genome assembly with minimal manual curation. Approaches that used highly accurate long reads and parent-child data with graph-based haplotype phasing during assembly outperformed those that did not. Developing a combination of the top-performing methods, we generated our first high-quality diploid reference assembly, containing only approximately four gaps per chromosome on average, with most chromosomes within ±1% of the length of CHM13. Nearly 48% of protein-coding genes have non-synonymous amino acid changes between haplotypes, and centromeric regions showed the highest diversity. Our findings serve as a foundation for assembling near-complete diploid human genomes at scale for a pangenome reference to capture global genetic variation from single nucleotides to structural rearrangements.",
            "affiliation": "Vertebrate Genome Laboratory, The Rockefeller University, New York, NY, USA. ejarvis@rockefeller.edu.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": [
                    "Research Support, Non-U.S. Gov't",
                    "research-article",
                    "Journal Article",
                    "Research Support, N.I.H., Extramural",
                ]
            },
            "grantsList": {
                "grant": [
                    {
                        "grantId": "R01 HG010040",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "U01 HG010961",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "U01 HG010971",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "NNF21OC0069089",
                        "agency": "Novo Nordisk Fonden",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R01 HG002385",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R01 HG006677",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R01 HG010169",
                        "agency": "NHGRI NIH HHS",
                        "acronym": "HG",
                        "orderIn": 0,
                    },
                    {
                        "agency": "Howard Hughes Medical Institute",
                        "acronym": "HHMI",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Chromosomes, Human",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "GE",
                                    "qualifierName": "genetics",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Humans"},
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Chromosome Mapping",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ST",
                                    "qualifierName": "standards",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Sequence Analysis, DNA",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                },
                                {
                                    "abbreviation": "ST",
                                    "qualifierName": "standards",
                                    "majorTopic_YN": "N",
                                },
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Genomics",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                },
                                {
                                    "abbreviation": "ST",
                                    "qualifierName": "standards",
                                    "majorTopic_YN": "N",
                                },
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Haplotypes",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "GE",
                                    "qualifierName": "genetics",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Diploidy"},
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Genome, Human",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "GE",
                                    "qualifierName": "genetics",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Reference Standards"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Genetic Variation",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "GE",
                                    "qualifierName": "genetics",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "High-Throughput Nucleotide Sequencing",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                },
                                {
                                    "abbreviation": "ST",
                                    "qualifierName": "standards",
                                    "majorTopic_YN": "N",
                                },
                            ]
                        },
                    },
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1038/s41586-022-05325-5",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9668749",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9668749?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 2,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "Y",
            "tmAccessionTypeList": {
                "accessionType": ["bioproject", "gen", "gca", "refseq", "igsr", "rrid"]
            },
            "dateOfCompletion": "2022-11-29",
            "dateOfCreation": "2022-10-19",
            "firstIndexDate": "2022-10-20",
            "fullTextReceivedDate": "2022-11-26",
            "dateOfRevision": "2022-12-31",
            "electronicPublicationDate": "2022-10-19",
            "firstPublicationDate": "2022-10-19",
        },
        {
            "id": "36413069",
            "source": "MED",
            "pmid": "36413069",
            "pmcid": "PMC9805587",
            "fullTextIdList": {"fullTextId": ["PMC9805587"]},
            "doi": "10.1093/bioinformatics/btac749",
            "title": "AlphaPulldown-a python package for protein-protein interaction screens using AlphaFold-Multimer.",
            "authorString": "Yu D, Chojnowski G, Rosenthal M, Kosinski J.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Yu D",
                        "firstName": "Dingquan",
                        "lastName": "Yu",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Centre for Structural Systems Biology (CSSB), Hamburg 22607, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Chojnowski G",
                        "firstName": "Grzegorz",
                        "lastName": "Chojnowski",
                        "initials": "G",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Rosenthal M",
                        "firstName": "Maria",
                        "lastName": "Rosenthal",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2986-936X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Bernhard Nocht Institute for Tropical Medicine, Hamburg 20359, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kosinski J",
                        "firstName": "Jan",
                        "lastName": "Kosinski",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3641-0322"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Centre for Structural Systems Biology (CSSB), Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0002-3641-0322"},
                    {"type": "ORCID", "value": "0000-0003-2986-936X"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "journalInfo": {
                "issue": "1",
                "volume": "39",
                "journalIssueId": 3480930,
                "dateOfPublication": "2023 Jan",
                "monthOfPublication": 1,
                "yearOfPublication": 2023,
                "printPublicationDate": "2023-01-01",
                "journal": {
                    "title": "Bioinformatics (Oxford, England)",
                    "medlineAbbreviation": "Bioinformatics",
                    "issn": "1367-4803",
                    "essn": "1367-4811",
                    "isoabbreviation": "Bioinformatics",
                    "nlmid": "9808944",
                },
            },
            "pubYear": "2023",
            "pageInfo": "btac749",
            "abstractText": "<h4>Summary</h4>The artificial intelligence-based structure prediction program AlphaFold-Multimer enabled structural modelling of protein complexes with unprecedented accuracy. Increasingly, AlphaFold-Multimer is also used to discover new protein-protein interactions (PPIs). Here, we present AlphaPulldown, a Python package that streamlines PPI screens and high-throughput modelling of higher-order oligomers using AlphaFold-Multimer. It provides a convenient command-line interface, a variety of confidence scores and a graphical analysis tool.<h4>Availability and implementation</h4>AlphaPulldown is freely available at https://www.embl-hamburg.de/AlphaPulldown.<h4>Supplementary information</h4>Supplementary note is available at Bioinformatics online.",
            "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print",
            "pubTypeList": {"pubType": ["brief-report", "Journal Article"]},
            "grantsList": {
                "grant": [
                    {"grantId": "KO 5979/2-1", "agency": "DFG", "orderIn": 0},
                    {"agency": "German Research Foundation", "orderIn": 0},
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "Y", "descriptorName": "Artificial Intelligence"},
                    {"majorTopic_YN": "Y", "descriptorName": "Software"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1093/bioinformatics/btac749",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9805587",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9805587?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 1,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2023-01-03",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "fullTextReceivedDate": "2023-01-05",
            "dateOfRevision": "2023-01-04",
            "firstPublicationDate": "2023-01-01",
        },
        {
            "id": "36412269",
            "source": "MED",
            "pmid": "36412269",
            "pmcid": "PMC9836076",
            "fullTextIdList": {"fullTextId": ["PMC9836076"]},
            "doi": "10.1242/bio.059561",
            "title": "Euglena International Network (EIN): Driving euglenoid biotechnology for the benefit of a challenged world.",
            "authorString": "Ebenezer TE, Low RS, O'Neill EC, Huang I, DeSimone A, Farrow SC, Field RA, Ginger ML, Guerrero SA, Hammond M, Hampl V, Horst G, Ishikawa T, Karnkowska A, Linton EW, Myler P, Nakazawa M, Cardol P, Sánchez-Thomas R, Saville BJ, Shah MR, Simpson AGB, Sur A, Suzuki K, Tyler KM, Zimba PV, Hall N, Field MC.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Ebenezer TE",
                        "firstName": "ThankGod Echezona",
                        "lastName": "Ebenezer",
                        "initials": "TE",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9005-1773"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute, Wellcome Genome Campus, Hinxton, Cambridge CB10 1SD, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Low RS",
                        "firstName": "Ross S",
                        "lastName": "Low",
                        "initials": "RS",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0956-2822"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Organisms and Ecosystems, Earlham Institute, Norwich Research Park, Norwich NR4 7UZ, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "O'Neill EC",
                        "firstName": "Ellis Charles",
                        "lastName": "O'Neill",
                        "initials": "EC",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5941-2806"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Chemistry, University of Nottingham, Nottingham NG7 2RD, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Huang I",
                        "firstName": "Ishuo",
                        "lastName": "Huang",
                        "initials": "I",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1711-7147"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Office of Regulatory Science, United States Food and Drug Administration, Center for Food Safety and Applied Nutrition, College Park, MD 20740, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "DeSimone A",
                        "firstName": "Antonio",
                        "lastName": "DeSimone",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2632-3057"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "The BioRobotics Institute, Scuola Superiore Sant'Anna, Pisa 56127, Italy."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Farrow SC",
                        "firstName": "Scott C",
                        "lastName": "Farrow",
                        "initials": "SC",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7106-8924"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Discovery Biology, Noblegen Inc., Peterborough, Ontario K9L 1Z8, Canada."
                                },
                                {
                                    "affiliation": "Environmental and Life Sciences Graduate Program, Trent University, Peterborough, Ontario K9L 0G2, Canada."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Field RA",
                        "firstName": "Robert A",
                        "lastName": "Field",
                        "initials": "RA",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry and Manchester Institute of Biotechnology, University of Manchester, Manchester M1 7DN, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ginger ML",
                        "firstName": "Michael L",
                        "lastName": "Ginger",
                        "initials": "ML",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9643-8482"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Applied Sciences, University of Huddersfield, Huddersfield HD1 3DH, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Guerrero SA",
                        "firstName": "Sergio Adrián",
                        "lastName": "Guerrero",
                        "initials": "SA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5440-9712"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Laboratorio de Enzimología Molecular, Instituto de Agrobiotecnología del Litoral. CCT CONICET Santa Fe, Santa Fe 3000, Argentina."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hammond M",
                        "firstName": "Michael",
                        "lastName": "Hammond",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7406-0717"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Parasitology, Biology Centre, Czech Academy of Sciences, České Budějovice 370 05, Czech Republic."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hampl V",
                        "firstName": "Vladimír",
                        "lastName": "Hampl",
                        "initials": "V",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5430-7564"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Charles University, Faculty of Science, Department of Parasitology, BIOCEV, Vestec 25250, Czech Republic."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Horst G",
                        "firstName": "Geoff",
                        "lastName": "Horst",
                        "initials": "G",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6597-9550"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Kemin Industries, Research and Development, Plymouth, MI 48170, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ishikawa T",
                        "firstName": "Takahiro",
                        "lastName": "Ishikawa",
                        "initials": "T",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5834-2922"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Agricultural and Life Sciences, Academic Assembly, Shimane University, Matsue 690-8504, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Karnkowska A",
                        "firstName": "Anna",
                        "lastName": "Karnkowska",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0003-3709-7873"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Evolutionary Biology, Faculty of Biology, University of Warsaw, Warsaw 02-089, Poland."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Linton EW",
                        "firstName": "Eric W",
                        "lastName": "Linton",
                        "initials": "EW",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0418-5244"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biology, Central Michigan University, Mt. Pleasant, MI 48859, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Myler P",
                        "firstName": "Peter",
                        "lastName": "Myler",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0056-0513"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Global Infectious Disease Research, Seattle Children's Research Institute and Department of Biomedical Informatics & Medical Education, University of Washington, WA 98109, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Nakazawa M",
                        "firstName": "Masami",
                        "lastName": "Nakazawa",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0003-1639-0560"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Applied Biochemistry, Faculty of Agriculture, Osaka Metropolitan University, Sakai, Osaka, 599-8531, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Cardol P",
                        "firstName": "Pierre",
                        "lastName": "Cardol",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9799-0546"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Life Sciences, Institut de Botanique, Université de Liège, Liège 4000, Belgium."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sánchez-Thomas R",
                        "firstName": "Rosina",
                        "lastName": "Sánchez-Thomas",
                        "initials": "R",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3825-591X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Instituto Nacional de Cardiología, Ignacio Chávez, Mexico 14080, Mexico."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Saville BJ",
                        "firstName": "Barry J",
                        "lastName": "Saville",
                        "initials": "BJ",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2949-7156"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Forensic Science, Environmental and Life Sciences Graduate Program, Trent University, Peterborough K9L 0G2, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Shah MR",
                        "firstName": "Mahfuzur R",
                        "lastName": "Shah",
                        "initials": "MR",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2435-1900"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Discovery Biology, Noblegen Inc., Peterborough, Ontario K9L 1Z8, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Simpson AGB",
                        "firstName": "Alastair G B",
                        "lastName": "Simpson",
                        "initials": "AGB",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4133-1709"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biology and Institute for Comparative Genomics, Dalhousie University, Halifax, Nova Scotia B3H 4R2, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sur A",
                        "firstName": "Aakash",
                        "lastName": "Sur",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2946-0038"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Global Infectious Disease Research, Seattle Children's Research Institute and Department of Biomedical Informatics & Medical Education, University of Washington, WA 98109, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Suzuki K",
                        "firstName": "Kengo",
                        "lastName": "Suzuki",
                        "initials": "K",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "R&D Company, Euglena Co., Ltd., 2F Yokohama Bio Industry Center (YBIC), 1-6 Suehiro, Tsurumi, Yokohama, Kanagawa, 230-0045, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Tyler KM",
                        "firstName": "Kevin M",
                        "lastName": "Tyler",
                        "initials": "KM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0647-8158"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Biomedical Research Centre, Norwich Medical School, University of East Anglia, Norwich Research Park, Norwich NR4 7TJ, UK."
                                },
                                {
                                    "affiliation": "Center of Excellence for Bionanoscience Research, King Abdul Aziz University, Jeddah, Saudi Arabia."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Zimba PV",
                        "firstName": "Paul V",
                        "lastName": "Zimba",
                        "initials": "PV",
                        "authorId": {"type": "ORCID", "value": "0000-0001-6541-2055"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "PVZimba, LLC, 12241 Percival St, Chester, VA 23831, USA."
                                },
                                {
                                    "affiliation": "Rice Rivers Center, VA Commonwealth University, Richmond, VA 23284, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Hall N",
                        "firstName": "Neil",
                        "lastName": "Hall",
                        "initials": "N",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2808-0009"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Organisms and Ecosystems, Earlham Institute, Norwich Research Park, Norwich NR4 7UZ, UK."
                                },
                                {
                                    "affiliation": "School of Biological Sciences, University of East Anglia, Norwich, NR4 7TJ, Norfolk, UK."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Field MC",
                        "firstName": "Mark C",
                        "lastName": "Field",
                        "initials": "MC",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8574-0275"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Parasitology, Biology Centre, Czech Academy of Sciences, České Budějovice 370 05, Czech Republic."
                                },
                                {
                                    "affiliation": "School of Life Sciences, University of Dundee, Dundee DD1 5EH, UK."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-6541-2055"},
                    {"type": "ORCID", "value": "0000-0001-7106-8924"},
                    {"type": "ORCID", "value": "0000-0001-7406-0717"},
                    {"type": "ORCID", "value": "0000-0001-8574-0275"},
                    {"type": "ORCID", "value": "0000-0001-9005-1773"},
                    {"type": "ORCID", "value": "0000-0001-9799-0546"},
                    {"type": "ORCID", "value": "0000-0002-0056-0513"},
                    {"type": "ORCID", "value": "0000-0002-0647-8158"},
                    {"type": "ORCID", "value": "0000-0002-0956-2822"},
                    {"type": "ORCID", "value": "0000-0002-1711-7147"},
                    {"type": "ORCID", "value": "0000-0002-2435-1900"},
                    {"type": "ORCID", "value": "0000-0002-2632-3057"},
                    {"type": "ORCID", "value": "0000-0002-3825-591X"},
                    {"type": "ORCID", "value": "0000-0002-4133-1709"},
                    {"type": "ORCID", "value": "0000-0002-5430-7564"},
                    {"type": "ORCID", "value": "0000-0002-5440-9712"},
                    {"type": "ORCID", "value": "0000-0002-5834-2922"},
                    {"type": "ORCID", "value": "0000-0002-5941-2806"},
                    {"type": "ORCID", "value": "0000-0002-6597-9550"},
                    {"type": "ORCID", "value": "0000-0002-9643-8482"},
                    {"type": "ORCID", "value": "0000-0003-0418-5244"},
                    {"type": "ORCID", "value": "0000-0003-1639-0560"},
                    {"type": "ORCID", "value": "0000-0003-2808-0009"},
                    {"type": "ORCID", "value": "0000-0003-2946-0038"},
                    {"type": "ORCID", "value": "0000-0003-2949-7156"},
                    {"type": "ORCID", "value": "0000-0003-3709-7873"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "journalInfo": {
                "issue": "11",
                "volume": "11",
                "journalIssueId": 3449910,
                "dateOfPublication": "2022 Nov",
                "monthOfPublication": 11,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-11-01",
                "journal": {
                    "title": "Biology open",
                    "medlineAbbreviation": "Biol Open",
                    "issn": "2046-6390",
                    "essn": "2046-6390",
                    "isoabbreviation": "Biol Open",
                    "nlmid": "101578018",
                },
            },
            "pubYear": "2022",
            "pageInfo": "bio059561",
            "abstractText": "Euglenoids (Euglenida) are unicellular flagellates possessing exceptionally wide geographical and ecological distribution. Euglenoids combine a biotechnological potential with a unique position in the eukaryotic tree of life. In large part these microbes owe this success to diverse genetics including secondary endosymbiosis and likely additional sources of genes. Multiple euglenoid species have translational applications and show great promise in production of biofuels, nutraceuticals, bioremediation, cancer treatments and more exotically as robotics design simulators. An absence of reference genomes currently limits these applications, including development of efficient tools for identification of critical factors in regulation, growth or optimization of metabolic pathways. The Euglena International Network (EIN) seeks to provide a forum to overcome these challenges. EIN has agreed specific goals, mobilized scientists, established a clear roadmap (Grand Challenges), connected academic and industry stakeholders and is currently formulating policy and partnership principles to propel these efforts in a coordinated and efficient manner.",
            "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute, Wellcome Genome Campus, Hinxton, Cambridge CB10 1SD, UK.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {"pubType": ["review-article", "Journal Article"]},
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Euglena",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "PH",
                                    "qualifierName": "physiology",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Biotechnology"},
                    {"majorTopic_YN": "N", "descriptorName": "Symbiosis"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Biotechnology",
                    "Euglena",
                    "Bioremediation",
                    "Networks",
                    "Biofuels",
                    "Food Supplements",
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1242/bio.059561",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9836076",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9836076?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-23",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "fullTextReceivedDate": "2023-01-16",
            "dateOfRevision": "2023-01-15",
            "electronicPublicationDate": "2022-11-22",
            "firstPublicationDate": "2022-11-22",
        },
        {
            "id": "35020793",
            "source": "MED",
            "pmid": "35020793",
            "pmcid": "PMC8963317",
            "fullTextIdList": {"fullTextId": ["PMC8963317"]},
            "doi": "10.1093/bioinformatics/btac023",
            "title": "Benchmarking the empirical accuracy of short-read sequencing across the M. tuberculosis genome.",
            "authorString": "Marin M, Vargas R, Harris M, Jeffrey B, Epperson LE, Durbin D, Strong M, Salfinger M, Iqbal Z, Akhundova I, Vashakidze S, Crudu V, Rosenthal A, Farhat MR.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Marin M",
                        "firstName": "Maximillian",
                        "lastName": "Marin",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9108-3328"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Informatics, Harvard Medical School, Boston, MA 02115, USA."
                                },
                                {
                                    "affiliation": "Department of Systems Biology, Harvard Medical School, Boston, MA 02115, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Vargas R",
                        "firstName": "Roger",
                        "lastName": "Vargas",
                        "initials": "R",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Informatics, Harvard Medical School, Boston, MA 02115, USA."
                                },
                                {
                                    "affiliation": "Department of Systems Biology, Harvard Medical School, Boston, MA 02115, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Harris M",
                        "firstName": "Michael",
                        "lastName": "Harris",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8789-0912"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Office of Cyber Infrastructure and Computational Biology, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, MD 20894, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jeffrey B",
                        "firstName": "Brendan",
                        "lastName": "Jeffrey",
                        "initials": "B",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Office of Cyber Infrastructure and Computational Biology, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, MD 20894, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Epperson LE",
                        "firstName": "L Elaine",
                        "lastName": "Epperson",
                        "initials": "LE",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Genes, Environment, and Health, National Jewish Health, Denver, CO 80206, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Durbin D",
                        "firstName": "David",
                        "lastName": "Durbin",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Mycobacteriology Reference Laboratory, Advanced Diagnostic Laboratories, National Jewish Health, Denver, CO 80206, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Strong M",
                        "firstName": "Michael",
                        "lastName": "Strong",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Genes, Environment, and Health, National Jewish Health, Denver, CO 80206, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Salfinger M",
                        "firstName": "Max",
                        "lastName": "Salfinger",
                        "initials": "M",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "College of Public Health and Morsani College of Medicine, University of South Florida, Tampa, FL 33612, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Iqbal Z",
                        "firstName": "Zamin",
                        "lastName": "Iqbal",
                        "initials": "Z",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "EMBL-EBI, Wellcome Genome Campus, Hinxton CB10 1SD, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Akhundova I",
                        "firstName": "Irada",
                        "lastName": "Akhundova",
                        "initials": "I",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Scientific Research Institute of Lung Diseases, Ministry of Health, Baku AZ1014, Azerbaijan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Vashakidze S",
                        "firstName": "Sergo",
                        "lastName": "Vashakidze",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Medicine, The University of Georgia, Tbilisi 0171, Georgia."
                                },
                                {
                                    "affiliation": "National Center for Tuberculosis and Lung Diseases, Ministry of Health, Tbilisi 0171, Georgia."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Crudu V",
                        "firstName": "Valeriu",
                        "lastName": "Crudu",
                        "initials": "V",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5059-8002"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Phthisiopneumology Institute, Ministry of Health, Chisinau 2025, Republic of Moldova."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Rosenthal A",
                        "firstName": "Alex",
                        "lastName": "Rosenthal",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Office of Cyber Infrastructure and Computational Biology, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, MD 20894, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Farhat MR",
                        "firstName": "Maha Reda",
                        "lastName": "Farhat",
                        "initials": "MR",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biomedical Informatics, Harvard Medical School, Boston, MA 02115, USA."
                                },
                                {
                                    "affiliation": "Pulmonary and Critical Care Medicine, Massachusetts General Hospital, Boston, MA 02114, USA."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5059-8002"},
                    {"type": "ORCID", "value": "0000-0001-8789-0912"},
                    {"type": "ORCID", "value": "0000-0002-9108-3328"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "journalInfo": {
                "issue": "7",
                "volume": "38",
                "journalIssueId": 3458560,
                "dateOfPublication": "2022 Mar",
                "monthOfPublication": 3,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-03-01",
                "journal": {
                    "title": "Bioinformatics (Oxford, England)",
                    "medlineAbbreviation": "Bioinformatics",
                    "issn": "1367-4803",
                    "essn": "1367-4811",
                    "isoabbreviation": "Bioinformatics",
                    "nlmid": "9808944",
                },
            },
            "pubYear": "2022",
            "pageInfo": "1781-1787",
            "abstractText": "<h4>Motivation</h4>Short-read whole-genome sequencing (WGS) is a vital tool for clinical applications and basic research. Genetic divergence from the reference genome, repetitive sequences and sequencing bias reduces the performance of variant calling using short-read alignment, but the loss in recall and specificity has not been adequately characterized. To benchmark short-read variant calling, we used 36 diverse clinical Mycobacterium tuberculosis (Mtb) isolates dually sequenced with Illumina short-reads and PacBio long-reads. We systematically studied the short-read variant calling accuracy and the influence of sequence uniqueness, reference bias and GC content.<h4>Results</h4>Reference-based Illumina variant calling demonstrated a maximum recall of 89.0% and minimum precision of 98.5% across parameters evaluated. The approach that maximized variant recall while still maintaining high precision (<99%) was tuning the mapping quality filtering threshold, i.e. confidence of the read mapping (recall\u2009=\u200985.8%, precision\u2009=\u200999.1%, MQ\u2009≥\u200940). Additional masking of repetitive sequence content is an alternative conservative approach to variant calling that increases precision at cost to recall (recall\u2009=\u200970.2%, precision\u2009=\u200999.6%, MQ\u2009≥\u200940). Of the genomic positions typically excluded for Mtb, 68% are accurately called using Illumina WGS including 52/168 PE/PPE genes (34.5%). From these results, we present a refined list of low confidence regions across the Mtb genome, which we found to frequently overlap with regions with structural variation, low sequence uniqueness and low sequencing coverage. Our benchmarking results have broad implications for the use of WGS in the study of Mtb biology, inference of transmission in public health surveillance systems and more generally for WGS applications in other organisms.<h4>Availability and implementation</h4>All relevant code is available at https://github.com/farhat-lab/mtb-illumina-wgs-evaluation.<h4>Supplementary information</h4>Supplementary data are available at Bioinformatics online.",
            "affiliation": "Department of Biomedical Informatics, Harvard Medical School, Boston, MA 02115, USA.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print",
            "pubTypeList": {
                "pubType": [
                    "research-article",
                    "Journal Article",
                    "Research Support, N.I.H., Extramural",
                ]
            },
            "grantsList": {
                "grant": [
                    {
                        "agency": "National Institute of Allergy and Infectious Diseases",
                        "orderIn": 0,
                    },
                    {"agency": "NIAID", "orderIn": 0},
                    {
                        "grantId": "AI55765",
                        "agency": "National Institutes of Health",
                        "orderIn": 0,
                    },
                    {
                        "agency": "Office of Science Management and Operations",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "D43 TW007124",
                        "agency": "FIC NIH HHS",
                        "acronym": "TW",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "ES026835",
                        "agency": "National Institutes of Health",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R01 AI155765",
                        "agency": "NIAID NIH HHS",
                        "acronym": "AI",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "N", "descriptorName": "Humans"},
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Mycobacterium tuberculosis",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "GE",
                                    "qualifierName": "genetics",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Tuberculosis"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Sequence Analysis, DNA",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Software"},
                    {"majorTopic_YN": "N", "descriptorName": "Benchmarking"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "High-Throughput Nucleotide Sequencing",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1093/bioinformatics/btac023",
                    },
                    {
                        "availability": "Free",
                        "availabilityCode": "F",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC8963317",
                    },
                    {
                        "availability": "Free",
                        "availabilityCode": "F",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC8963317?pdf=render",
                    },
                    {
                        "availability": "Free",
                        "availabilityCode": "F",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1093/bioinformatics/btac023",
                    },
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 2,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-18",
            "dateOfCreation": "2022-01-12",
            "firstIndexDate": "2022-01-13",
            "fullTextReceivedDate": "2023-01-11",
            "dateOfRevision": "2023-02-03",
            "firstPublicationDate": "2022-03-01",
            "embargoDate": "2023-01-10",
        },
        {
            "id": "36413626",
            "source": "MED",
            "pmid": "36413626",
            "pmcid": "PMC9756341",
            "fullTextIdList": {"fullTextId": ["PMC9756341"]},
            "doi": "10.1021/jacs.2c07378",
            "title": "A KLK6 Activity-Based Probe Reveals a Role for KLK6 Activity in Pancreatic Cancer Cell Invasion.",
            "authorString": "Zhang L, Lovell S, De Vita E, Jagtap PKA, Lucy D, Goya Grocin A, Kjær S, Borg A, Hennig J, Miller AK, Tate EW.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Zhang L",
                        "firstName": "Leran",
                        "lastName": "Zhang",
                        "initials": "L",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1051-8132"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Lovell S",
                        "firstName": "Scott",
                        "lastName": "Lovell",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Life Sciences, University of Bath, Bath BA2 7AX, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "De Vita E",
                        "firstName": "Elena",
                        "lastName": "De Vita",
                        "initials": "E",
                        "authorId": {"type": "ORCID", "value": "0000-0003-4707-8342"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jagtap PKA",
                        "firstName": "Pravin Kumar Ankush",
                        "lastName": "Jagtap",
                        "initials": "PKA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9457-4130"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                                {
                                    "affiliation": "Chair of Biochemistry IV, Biophysical Chemistry, University of Bayreuth, Bayreuth 95447, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Lucy D",
                        "firstName": "Daniel",
                        "lastName": "Lucy",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Goya Grocin A",
                        "firstName": "Andrea",
                        "lastName": "Goya Grocin",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6914-7639"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kjær S",
                        "firstName": "Svend",
                        "lastName": "Kjær",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural Biology Science Technology Platform, The Francis Crick Institute, London NW1 1AT, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Borg A",
                        "firstName": "Annabel",
                        "lastName": "Borg",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural Biology Science Technology Platform, The Francis Crick Institute, London NW1 1AT, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hennig J",
                        "firstName": "Janosch",
                        "lastName": "Hennig",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5214-7002"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                                {
                                    "affiliation": "Chair of Biochemistry IV, Biophysical Chemistry, University of Bayreuth, Bayreuth 95447, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Miller AK",
                        "firstName": "Aubry K",
                        "lastName": "Miller",
                        "initials": "AK",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Cancer Drug Development Group, German Cancer Research Center (DKFZ), Heidelberg 69120, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Tate EW",
                        "firstName": "Edward W",
                        "lastName": "Tate",
                        "initials": "EW",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2213-5814"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    {"type": "ORCID", "value": "0000-0002-1051-8132"},
                    {"type": "ORCID", "value": "0000-0002-6914-7639"},
                    {"type": "ORCID", "value": "0000-0002-9457-4130"},
                    {"type": "ORCID", "value": "0000-0003-2213-5814"},
                    {"type": "ORCID", "value": "0000-0003-4707-8342"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics", "supporting_data"]},
            "journalInfo": {
                "issue": "49",
                "volume": "144",
                "journalIssueId": 3475101,
                "dateOfPublication": "2022 Dec",
                "monthOfPublication": 12,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-12-01",
                "journal": {
                    "title": "Journal of the American Chemical Society",
                    "medlineAbbreviation": "J Am Chem Soc",
                    "issn": "0002-7863",
                    "essn": "1520-5126",
                    "isoabbreviation": "J Am Chem Soc",
                    "nlmid": "7503056",
                },
            },
            "pubYear": "2022",
            "pageInfo": "22493-22504",
            "abstractText": "Pancreatic cancer has the lowest survival rate of all common cancers due to late diagnosis and limited treatment options. Serine hydrolases are known to mediate cancer progression and metastasis through initiation of signaling cascades and cleavage of extracellular matrix proteins, and the kallikrein-related peptidase (KLK) family of secreted serine proteases have emerging roles in pancreatic ductal adenocarcinoma (PDAC). However, the lack of reliable activity-based probes (ABPs) to profile KLK activity has hindered progress in validation of these enzymes as potential targets or biomarkers. Here, we developed potent and selective ABPs for KLK6 by using a positional scanning combinatorial substrate library and characterized their binding mode and interactions by X-ray crystallography. The optimized KLK6 probe IMP-2352 (<i>k</i><sub>obs</sub>/<i>I</i> = 11,000 M<sup>-1</sup> s<sup>-1</sup>) enabled selective detection of KLK6 activity in a variety of PDAC cell lines, and we observed that KLK6 inhibition reduced the invasiveness of PDAC cells that secrete active KLK6. KLK6 inhibitors were combined with N-terminomics to identify potential secreted protein substrates of KLK6 in PDAC cells, providing insights into KLK6-mediated invasion pathways. These novel KLK6 ABPs offer a toolset to validate KLK6 and associated signaling partners as targets or biomarkers across a range of diseases.",
            "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": [
                    "Research Support, Non-U.S. Gov't",
                    "research-article",
                    "Journal Article",
                    "Research Support, N.I.H., Extramural",
                ]
            },
            "grantsList": {
                "grant": [
                    {
                        "grantId": "C24523/A25192",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "C29637/A21451",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "890900",
                        "agency": "H2020 Marie Sklodowska-Curie Actions",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "19-0059",
                        "agency": "Worldwide Cancer Research",
                        "acronym": "AICR_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "C29637/A20183",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "P41 GM103311",
                        "agency": "NIGMS NIH HHS",
                        "acronym": "GM",
                        "orderIn": 0,
                    },
                    {
                        "agency": "Engineering and Physical Sciences Research Council",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "CC1068",
                        "agency": "The Francis Crick Institute",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "N", "descriptorName": "Humans"},
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Carcinoma, Pancreatic Ductal",
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Pancreatic Neoplasms"},
                    {"majorTopic_YN": "N", "descriptorName": "Neoplasm Invasiveness"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Kallikreins",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "KLK6 protein, human", "registryNumber": "EC 3.4.21.-"},
                    {"name": "Kallikreins", "registryNumber": "EC 3.4.21.-"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1021/jacs.2c07378",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9756341",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9756341?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 0,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "Y",
            "tmAccessionTypeList": {"accessionType": ["pdb"]},
            "dateOfCompletion": "2022-12-15",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "fullTextReceivedDate": "2022-12-20",
            "dateOfRevision": "2022-12-22",
            "electronicPublicationDate": "2022-11-22",
            "firstPublicationDate": "2022-11-22",
        },
        {
            "id": "36410948",
            "source": "MED",
            "pmid": "36410948",
            "doi": "10.1016/bs.mie.2022.08.008",
            "title": "Technical considerations for small-angle neutron scattering from biological macromolecules in solution: Cross sections, contrasts, instrument setup and measurement.",
            "authorString": "Pietras Z, Wood K, Whitten AE, Jeffries CM.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Pietras Z",
                        "firstName": "Zuzanna",
                        "lastName": "Pietras",
                        "initials": "Z",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Physics, Chemistry, and Biology, Linköping University, Linköping, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wood K",
                        "firstName": "Kathleen",
                        "lastName": "Wood",
                        "initials": "K",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation, New Illawarra Rd, Lucas Heights, NSW, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Whitten AE",
                        "firstName": "Andrew E",
                        "lastName": "Whitten",
                        "initials": "AE",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation, New Illawarra Rd, Lucas Heights, NSW, Australia. Electronic address: andrew.whitten@ansto.gov.au."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jeffries CM",
                        "firstName": "Cy M",
                        "lastName": "Jeffries",
                        "initials": "CM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8718-7343"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory (EMBL), Hamburg, Germany. Electronic address: cy.jeffries@embl-hamburg.de."
                                }
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [{"type": "ORCID", "value": "0000-0002-8718-7343"}]
            },
            "journalInfo": {
                "volume": "677",
                "journalIssueId": 3461591,
                "dateOfPublication": "2022 ",
                "monthOfPublication": 0,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-01-01",
                "journal": {
                    "title": "Methods in enzymology",
                    "medlineAbbreviation": "Methods Enzymol",
                    "issn": "0076-6879",
                    "essn": "1557-7988",
                    "isoabbreviation": "Methods Enzymol",
                    "nlmid": "0212271",
                },
            },
            "pubYear": "2022",
            "pageInfo": "157-189",
            "abstractText": "Small angle scattering affords an approach to evaluate the structure of dilute populations of macromolecules in solution where the measured scattering intensities relate to the distribution of scattering-pair distances within each macromolecule. When small angle neutron scattering (SANS) with contrast variation is employed, additional structural information can be obtained regarding the internal organization of biomacromolecule complexes and assemblies. The technique allows for the components of assemblies to be selectively 'matched in' and 'matched out' of the scattering profiles due to the different ways the isotopes of hydrogen-protium <sup>1</sup>H, and deuterium <sup>2</sup>H (or D)-scatter neutrons. The isotopic substitution of <sup>1</sup>H for D in the sample enables the controlled variation of the scattering contrasts. A contrast variation experiment requires trade-offs between neutron beam intensity, q-range, wavelength and q-resolution, isotopic labelling levels, sample concentration and path-length, and measurement times. Navigating these competing aspects to find an optimal combination is a daunting task. Here we provide an overview of how to calculate the neutron scattering contrasts of dilute biological macromolecule samples prior to an experiment and how this then informs the approach to configuring SANS instruments and the measurement of a contrast variation series dataset.",
            "affiliation": "Department of Physics, Chemistry, and Biology, Linköping University, Linköping, Sweden.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": ["Research Support, Non-U.S. Gov't", "Journal Article"]
            },
            "grantsList": {
                "grant": [
                    {"agency": "Australian Government", "orderIn": 0},
                    {
                        "grantId": "871037",
                        "agency": "European Commission",
                        "orderIn": 0,
                    },
                    {"agency": "Stiftelsen för\xa0Strategisk Forskning", "orderIn": 0},
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Hydrogen",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Macromolecular Substances",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Neutron Diffraction",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Neutrons"},
                    {"majorTopic_YN": "N", "descriptorName": "Scattering, Small Angle"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Biological macromolecule",
                    "contrast",
                    "Sans",
                    "Contrast Variation",
                    "Dilute Solution Scattering",
                    "Sans Instrument",
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "Macromolecular Substances", "registryNumber": "0"},
                    {"name": "Hydrogen", "registryNumber": "7YNJ3PO35Z"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1016/bs.mie.2022.08.008",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "N",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-23",
            "dateOfCreation": "2022-11-21",
            "firstIndexDate": "2022-11-22",
            "dateOfRevision": "2022-11-30",
            "electronicPublicationDate": "2022-09-22",
            "firstPublicationDate": "2022-09-22",
        },
        {
            "id": "35396167",
            "source": "MED",
            "pmid": "35396167",
            "doi": "10.1016/j.semcdb.2022.03.028",
            "title": "Membrane-actin interactions in morphogenesis: Lessons learned from Drosophila cellularization.",
            "authorString": "Sokac AM, Biel N, De Renzis S.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Sokac AM",
                        "firstName": "Anna Marie",
                        "lastName": "Sokac",
                        "initials": "AM",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Cell and Developmental Biology, University of Illinois at Urbana Champaign, Urbana, IL 61801, USA; Graduate Program in Integrative and Molecular Biomedical Sciences, Baylor College of Medicine, Houston, TX 77030, USA. Electronic address: asokac@illinois.edu."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Biel N",
                        "firstName": "Natalie",
                        "lastName": "Biel",
                        "initials": "N",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Cell and Developmental Biology, University of Illinois at Urbana Champaign, Urbana, IL 61801, USA; Graduate Program in Integrative and Molecular Biomedical Sciences, Baylor College of Medicine, Houston, TX 77030, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "De Renzis S",
                        "firstName": "Stefano",
                        "lastName": "De Renzis",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Heidelberg, 69117 Heidelberg, Germany."
                                }
                            ]
                        },
                    },
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics", "related_data"]},
            "journalInfo": {
                "volume": "133",
                "journalIssueId": 3461063,
                "dateOfPublication": "2023 Jan",
                "monthOfPublication": 1,
                "yearOfPublication": 2023,
                "printPublicationDate": "2023-01-01",
                "journal": {
                    "title": "Seminars in cell & developmental biology",
                    "medlineAbbreviation": "Semin Cell Dev Biol",
                    "issn": "1084-9521",
                    "essn": "1096-3634",
                    "isoabbreviation": "Semin Cell Dev Biol",
                    "nlmid": "9607332",
                },
            },
            "pubYear": "2023",
            "pageInfo": "107-122",
            "abstractText": "During morphogenesis, changes in the shapes of individual cells are harnessed to mold an entire tissue. These changes in cell shapes require the coupled remodeling of the plasma membrane and underlying actin cytoskeleton. In this review, we highlight cellularization of the Drosophila embryo as a model system to uncover principles of how membrane and actin dynamics are co-regulated in space and time to drive morphogenesis.",
            "affiliation": "Department of Cell and Developmental Biology, University of Illinois at Urbana Champaign, Urbana, IL 61801, USA; Graduate Program in Integrative and Molecular Biomedical Sciences, Baylor College of Medicine, Houston, TX 77030, USA. Electronic address: asokac@illinois.edu.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": [
                    "Research Support, Non-U.S. Gov't",
                    "Review",
                    "Journal Article",
                    "Research Support, N.I.H., Extramural",
                ]
            },
            "grantsList": {
                "grant": [
                    {
                        "grantId": "R01 GM115111",
                        "agency": "NIGMS NIH HHS",
                        "acronym": "GM",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R35 GM136384",
                        "agency": "NIGMS NIH HHS",
                        "acronym": "GM",
                        "orderIn": 0,
                    },
                    {"agency": "European Molecular Biology Laboratory", "orderIn": 0},
                    {
                        "grantId": "R01 GM115111",
                        "agency": "National Institutes of Health",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "R35 GM136384",
                        "agency": "National Institutes of Health",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Cell Membrane",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Embryo, Nonmammalian",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Animals"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Drosophila",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Drosophila melanogaster",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Actins",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Drosophila Proteins",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Morphogenesis"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Actin",
                    "Exocytosis",
                    "Endocytosis",
                    "Phosphoinositides",
                    "membrane trafficking",
                    "cytokinesis",
                    "Membrane Reservoir",
                    "Cortical Compartments",
                    "Myosin-2",
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "Actins", "registryNumber": "0"},
                    {"name": "Drosophila Proteins", "registryNumber": "0"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1016/j.semcdb.2022.03.028",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 2,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by-nc-nd",
            "authMan": "Y",
            "epmcAuthMan": "N",
            "nihAuthMan": "Y",
            "manuscriptId": "NIHMS1796477",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-22",
            "dateOfCreation": "2022-04-09",
            "firstIndexDate": "2022-04-11",
            "dateOfRevision": "2023-02-09",
            "electronicPublicationDate": "2022-04-05",
            "firstPublicationDate": "2022-04-05",
            "embargoDate": "2024-01-15",
        },
        {
            "id": "PPR570124",
            "source": "PPR",
            "doi": "10.1101/2022.11.11.516098",
            "title": "Structural basis of RNA-induced autoregulation of the DExH-type RNA helicase maleless",
            "authorString": "Jagtap PKA, Müller M, Kiss AE, Thomae AW, Lapouge K, Beck M, Becker PB, Hennig J.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Jagtap PKA",
                        "firstName": "Pravin Kumar Ankush",
                        "lastName": "Jagtap",
                        "initials": "PKA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9457-4130"},
                    },
                    {
                        "fullName": "Müller M",
                        "firstName": "Marisa",
                        "lastName": "Müller",
                        "initials": "M",
                    },
                    {
                        "fullName": "Kiss AE",
                        "firstName": "Anna",
                        "lastName": "Kiss",
                        "initials": "AE",
                    },
                    {
                        "fullName": "Thomae AW",
                        "firstName": "Andreas",
                        "lastName": "Thomae",
                        "initials": "AW",
                    },
                    {
                        "fullName": "Lapouge K",
                        "firstName": "Karine",
                        "lastName": "Lapouge",
                        "initials": "K",
                    },
                    {
                        "fullName": "Beck M",
                        "firstName": "Martin",
                        "lastName": "Beck",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0002-7397-1321"},
                    },
                    {
                        "fullName": "Becker PB",
                        "firstName": "Peter",
                        "lastName": "Becker",
                        "initials": "PB",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7186-0372"},
                    },
                    {
                        "fullName": "Hennig J",
                        "firstName": "Janosch",
                        "lastName": "Hennig",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    {"type": "ORCID", "value": "0000-0001-7186-0372"},
                    {"type": "ORCID", "value": "0000-0002-7397-1321"},
                    {"type": "ORCID", "value": "0000-0002-9457-4130"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "pubYear": "2022",
            "abstractText": "<h4>Summary</h4> Unwinding RNA secondary structures by RNA helicases is essential for RNA metabolism. How the basic unwinding reaction of DExH-type helicases is regulated by their accessory domains is unresolved. Here, we combine structural and functional analyses to address this challenge for the prototypic DExH RNA helicase maleless (MLE) from Drosophila . We captured the helicase cycle of MLE with multiple structural snapshots. We discovered that initially, dsRBD2 flexibly samples substrate dsRNA and aligns it with the open helicase tunnel. Subsequently, dsRBD2 releases RNA and associates with the helicase core, leading to closure of the tunnel around ssRNA. Structure-based MLE mutations confirm the functional relevance of the structural model in cells. We propose a molecular model in which the dsRBD2 domain of MLE orchestrates large structural transitions that depend on substrate RNA but are independent of ATP. Our findings reveal the fundamental mechanics of dsRNA unwinding by DExH helicases with high general relevance for dosage compensation and specific implications for MLE’s human orthologue DHX9/RHA mechanisms in disease.",
            "pubTypeList": {"pubType": ["Preprint"]},
            "bookOrReportDetails": {"publisher": "bioRxiv", "yearOfPublication": 2022},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1101/2022.11.11.516098",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCreation": "2022-11-13",
            "firstIndexDate": "2022-11-13",
            "firstPublicationDate": "2022-11-11",
        },
    ]


class EpmcData1:
    epmc_data = [
        {
            "id": "PPR570124",
            "source": "PPR",
            "doi": "10.1101/2022.11.11.516098",
            "title": "Structural basis of RNA-induced autoregulation of the DExH-type RNA helicase maleless",
            "authorString": "Jagtap PKA, Müller M, Kiss AE, Thomae AW, Lapouge K, Beck M, Becker PB, Hennig J.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Jagtap PKA",
                        "firstName": "Pravin Kumar Ankush",
                        "lastName": "Jagtap",
                        "initials": "PKA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9457-4130"},
                    },
                    {
                        "fullName": "Müller M",
                        "firstName": "Marisa",
                        "lastName": "Müller",
                        "initials": "M",
                    },
                    {
                        "fullName": "Kiss AE",
                        "firstName": "Anna",
                        "lastName": "Kiss",
                        "initials": "AE",
                    },
                    {
                        "fullName": "Thomae AW",
                        "firstName": "Andreas",
                        "lastName": "Thomae",
                        "initials": "AW",
                    },
                    {
                        "fullName": "Lapouge K",
                        "firstName": "Karine",
                        "lastName": "Lapouge",
                        "initials": "K",
                    },
                    {
                        "fullName": "Beck M",
                        "firstName": "Martin",
                        "lastName": "Beck",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0002-7397-1321"},
                    },
                    {
                        "fullName": "Becker PB",
                        "firstName": "Peter",
                        "lastName": "Becker",
                        "initials": "PB",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7186-0372"},
                    },
                    {
                        "fullName": "Hennig J",
                        "firstName": "Janosch",
                        "lastName": "Hennig",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    {"type": "ORCID", "value": "0000-0001-7186-0372"},
                    {"type": "ORCID", "value": "0000-0002-7397-1321"},
                    {"type": "ORCID", "value": "0000-0002-9457-4130"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "pubYear": "2022",
            "abstractText": "<h4>Summary</h4> Unwinding RNA secondary structures by RNA helicases is essential for RNA metabolism. How the basic unwinding reaction of DExH-type helicases is regulated by their accessory domains is unresolved. Here, we combine structural and functional analyses to address this challenge for the prototypic DExH RNA helicase maleless (MLE) from Drosophila . We captured the helicase cycle of MLE with multiple structural snapshots. We discovered that initially, dsRBD2 flexibly samples substrate dsRNA and aligns it with the open helicase tunnel. Subsequently, dsRBD2 releases RNA and associates with the helicase core, leading to closure of the tunnel around ssRNA. Structure-based MLE mutations confirm the functional relevance of the structural model in cells. We propose a molecular model in which the dsRBD2 domain of MLE orchestrates large structural transitions that depend on substrate RNA but are independent of ATP. Our findings reveal the fundamental mechanics of dsRNA unwinding by DExH helicases with high general relevance for dosage compensation and specific implications for MLE’s human orthologue DHX9/RHA mechanisms in disease.",
            "pubTypeList": {"pubType": ["Preprint"]},
            "bookOrReportDetails": {"publisher": "bioRxiv", "yearOfPublication": 2022},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1101/2022.11.11.516098",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCreation": "2022-11-13",
            "firstIndexDate": "2022-11-13",
            "firstPublicationDate": "2022-11-11",
        },
        {
            "id": "36413069",
            "source": "MED",
            "pmid": "36413069",
            "doi": "10.1093/bioinformatics/btac749",
            "title": "AlphaPulldown-a python package for protein-protein interaction screens using AlphaFold-Multimer.",
            "authorString": "Yu D, Chojnowski G, Rosenthal M, Kosinski J.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Yu D",
                        "firstName": "Dingquan",
                        "lastName": "Yu",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Centre for Structural Systems Biology (CSSB), Hamburg 22607, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Chojnowski G",
                        "firstName": "Grzegorz",
                        "lastName": "Chojnowski",
                        "initials": "G",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Rosenthal M",
                        "firstName": "Maria",
                        "lastName": "Rosenthal",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2986-936X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Bernhard Nocht Institute for Tropical Medicine, Hamburg 20359, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kosinski J",
                        "firstName": "Jan",
                        "lastName": "Kosinski",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3641-0322"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Centre for Structural Systems Biology (CSSB), Hamburg 22607, Germany."
                                },
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0002-3641-0322"},
                    {"type": "ORCID", "value": "0000-0003-2986-936X"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "journalInfo": {
                "issue": "1",
                "volume": "39",
                "journalIssueId": 3480930,
                "dateOfPublication": "2023 Jan",
                "monthOfPublication": 1,
                "yearOfPublication": 2023,
                "printPublicationDate": "2023-01-01",
                "journal": {
                    "title": "Bioinformatics (Oxford, England)",
                    "medlineAbbreviation": "Bioinformatics",
                    "isoabbreviation": "Bioinformatics",
                    "nlmid": "9808944",
                    "issn": "1367-4803",
                    "essn": "1367-4811",
                },
            },
            "pubYear": "2023",
            "pageInfo": "btac749",
            "abstractText": "<h4>Summary</h4>The artificial intelligence-based structure prediction program AlphaFold-Multimer enabled structural modelling of protein complexes with unprecedented accuracy. Increasingly, AlphaFold-Multimer is also used to discover new protein-protein interactions (PPIs). Here, we present AlphaPulldown, a Python package that streamlines PPI screens and high-throughput modelling of higher-order oligomers using AlphaFold-Multimer. It provides a convenient command-line interface, a variety of confidence scores and a graphical analysis tool.<h4>Availability and implementation</h4>AlphaPulldown is freely available at https://www.embl-hamburg.de/AlphaPulldown.<h4>Supplementary information</h4>Supplementary note is available at Bioinformatics online.",
            "affiliation": "European Molecular Biology Laboratory Hamburg, Hamburg 22607, Germany.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print",
            "pubTypeList": {"pubType": ["Journal Article"]},
            "grantsList": {
                "grant": [
                    {"grantId": "KO 5979/2-1", "agency": "DFG", "orderIn": 0},
                    {"agency": "German Research Foundation", "orderIn": 0},
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "Y", "descriptorName": "Artificial Intelligence"},
                    {"majorTopic_YN": "Y", "descriptorName": "Software"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1093/bioinformatics/btac749",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 1,
            "hasData": "N",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2023-01-03",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "dateOfRevision": "2023-01-04",
            "firstPublicationDate": "2023-01-01",
        },
        {
            "id": "36412269",
            "source": "MED",
            "pmid": "36412269",
            "doi": "10.1242/bio.059561",
            "title": "Euglena International Network (EIN): Driving euglenoid biotechnology for the benefit of a challenged world.",
            "authorString": "Ebenezer TE, Low RS, O'Neill EC, Huang I, DeSimone A, Farrow SC, Field RA, Ginger ML, Guerrero SA, Hammond M, Hampl V, Horst G, Ishikawa T, Karnkowska A, Linton EW, Myler P, Nakazawa M, Cardol P, Sánchez-Thomas R, Saville BJ, Shah MR, Simpson AGB, Sur A, Suzuki K, Tyler KM, Zimba PV, Hall N, Field MC.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Ebenezer TE",
                        "firstName": "ThankGod Echezona",
                        "lastName": "Ebenezer",
                        "initials": "TE",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9005-1773"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute, Wellcome Genome Campus, Hinxton, Cambridge CB10 1SD, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Low RS",
                        "firstName": "Ross S",
                        "lastName": "Low",
                        "initials": "RS",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0956-2822"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Organisms and Ecosystems, Earlham Institute, Norwich Research Park, Norwich NR4 7UZ, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "O'Neill EC",
                        "firstName": "Ellis Charles",
                        "lastName": "O'Neill",
                        "initials": "EC",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5941-2806"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Chemistry, University of Nottingham, Nottingham NG7 2RD, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Huang I",
                        "firstName": "Ishuo",
                        "lastName": "Huang",
                        "initials": "I",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1711-7147"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Office of Regulatory Science, United States Food and Drug Administration, Center for Food Safety and Applied Nutrition, College Park, MD 20740, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "DeSimone A",
                        "firstName": "Antonio",
                        "lastName": "DeSimone",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2632-3057"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "The BioRobotics Institute, Scuola Superiore Sant'Anna, Pisa 56127, Italy."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Farrow SC",
                        "firstName": "Scott C",
                        "lastName": "Farrow",
                        "initials": "SC",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7106-8924"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Discovery Biology, Noblegen Inc., Peterborough, Ontario K9L 1Z8, Canada."
                                },
                                {
                                    "affiliation": "Environmental and Life Sciences Graduate Program, Trent University, Peterborough, Ontario K9L 0G2, Canada."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Field RA",
                        "firstName": "Robert A",
                        "lastName": "Field",
                        "initials": "RA",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry and Manchester Institute of Biotechnology, University of Manchester, Manchester M1 7DN, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ginger ML",
                        "firstName": "Michael L",
                        "lastName": "Ginger",
                        "initials": "ML",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9643-8482"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "School of Applied Sciences, University of Huddersfield, Huddersfield HD1 3DH, UK."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Guerrero SA",
                        "firstName": "Sergio Adrián",
                        "lastName": "Guerrero",
                        "initials": "SA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5440-9712"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Laboratorio de Enzimología Molecular, Instituto de Agrobiotecnología del Litoral. CCT CONICET Santa Fe, Santa Fe 3000, Argentina."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hammond M",
                        "firstName": "Michael",
                        "lastName": "Hammond",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0001-7406-0717"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Parasitology, Biology Centre, Czech Academy of Sciences, České Budějovice 370 05, Czech Republic."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hampl V",
                        "firstName": "Vladimír",
                        "lastName": "Hampl",
                        "initials": "V",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5430-7564"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Charles University, Faculty of Science, Department of Parasitology, BIOCEV, Vestec 25250, Czech Republic."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Horst G",
                        "firstName": "Geoff",
                        "lastName": "Horst",
                        "initials": "G",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6597-9550"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Kemin Industries, Research and Development, Plymouth, MI 48170, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Ishikawa T",
                        "firstName": "Takahiro",
                        "lastName": "Ishikawa",
                        "initials": "T",
                        "authorId": {"type": "ORCID", "value": "0000-0002-5834-2922"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Agricultural and Life Sciences, Academic Assembly, Shimane University, Matsue 690-8504, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Karnkowska A",
                        "firstName": "Anna",
                        "lastName": "Karnkowska",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0003-3709-7873"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Evolutionary Biology, Faculty of Biology, University of Warsaw, Warsaw 02-089, Poland."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Linton EW",
                        "firstName": "Eric W",
                        "lastName": "Linton",
                        "initials": "EW",
                        "authorId": {"type": "ORCID", "value": "0000-0003-0418-5244"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biology, Central Michigan University, Mt. Pleasant, MI 48859, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Myler P",
                        "firstName": "Peter",
                        "lastName": "Myler",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0056-0513"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Global Infectious Disease Research, Seattle Children's Research Institute and Department of Biomedical Informatics & Medical Education, University of Washington, WA 98109, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Nakazawa M",
                        "firstName": "Masami",
                        "lastName": "Nakazawa",
                        "initials": "M",
                        "authorId": {"type": "ORCID", "value": "0000-0003-1639-0560"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Applied Biochemistry, Faculty of Agriculture, Osaka Metropolitan University, Sakai, Osaka, 599-8531, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Cardol P",
                        "firstName": "Pierre",
                        "lastName": "Cardol",
                        "initials": "P",
                        "authorId": {"type": "ORCID", "value": "0000-0001-9799-0546"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Life Sciences, Institut de Botanique, Université de Liège, Liège 4000, Belgium."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sánchez-Thomas R",
                        "firstName": "Rosina",
                        "lastName": "Sánchez-Thomas",
                        "initials": "R",
                        "authorId": {"type": "ORCID", "value": "0000-0002-3825-591X"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Instituto Nacional de Cardiología, Ignacio Chávez, Mexico 14080, Mexico."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Saville BJ",
                        "firstName": "Barry J",
                        "lastName": "Saville",
                        "initials": "BJ",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2949-7156"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Forensic Science, Environmental and Life Sciences Graduate Program, Trent University, Peterborough K9L 0G2, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Shah MR",
                        "firstName": "Mahfuzur R",
                        "lastName": "Shah",
                        "initials": "MR",
                        "authorId": {"type": "ORCID", "value": "0000-0002-2435-1900"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Discovery Biology, Noblegen Inc., Peterborough, Ontario K9L 1Z8, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Simpson AGB",
                        "firstName": "Alastair G B",
                        "lastName": "Simpson",
                        "initials": "AGB",
                        "authorId": {"type": "ORCID", "value": "0000-0002-4133-1709"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Biology and Institute for Comparative Genomics, Dalhousie University, Halifax, Nova Scotia B3H 4R2, Canada."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Sur A",
                        "firstName": "Aakash",
                        "lastName": "Sur",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2946-0038"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Center for Global Infectious Disease Research, Seattle Children's Research Institute and Department of Biomedical Informatics & Medical Education, University of Washington, WA 98109, USA."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Suzuki K",
                        "firstName": "Kengo",
                        "lastName": "Suzuki",
                        "initials": "K",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "R&D Company, Euglena Co., Ltd., 2F Yokohama Bio Industry Center (YBIC), 1-6 Suehiro, Tsurumi, Yokohama, Kanagawa, 230-0045, Japan."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Tyler KM",
                        "firstName": "Kevin M",
                        "lastName": "Tyler",
                        "initials": "KM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-0647-8158"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Biomedical Research Centre, Norwich Medical School, University of East Anglia, Norwich Research Park, Norwich NR4 7TJ, UK."
                                },
                                {
                                    "affiliation": "Center of Excellence for Bionanoscience Research, King Abdul Aziz University, Jeddah, Saudi Arabia."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Zimba PV",
                        "firstName": "Paul V",
                        "lastName": "Zimba",
                        "initials": "PV",
                        "authorId": {"type": "ORCID", "value": "0000-0001-6541-2055"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "PVZimba, LLC, 12241 Percival St, Chester, VA 23831, USA."
                                },
                                {
                                    "affiliation": "Rice Rivers Center, VA Commonwealth University, Richmond, VA 23284, USA."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Hall N",
                        "firstName": "Neil",
                        "lastName": "Hall",
                        "initials": "N",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2808-0009"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Organisms and Ecosystems, Earlham Institute, Norwich Research Park, Norwich NR4 7UZ, UK."
                                },
                                {
                                    "affiliation": "School of Biological Sciences, University of East Anglia, Norwich, NR4 7TJ, Norfolk, UK."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Field MC",
                        "firstName": "Mark C",
                        "lastName": "Field",
                        "initials": "MC",
                        "authorId": {"type": "ORCID", "value": "0000-0001-8574-0275"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Institute of Parasitology, Biology Centre, Czech Academy of Sciences, České Budějovice 370 05, Czech Republic."
                                },
                                {
                                    "affiliation": "School of Life Sciences, University of Dundee, Dundee DD1 5EH, UK."
                                },
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-6541-2055"},
                    {"type": "ORCID", "value": "0000-0001-7106-8924"},
                    {"type": "ORCID", "value": "0000-0001-7406-0717"},
                    {"type": "ORCID", "value": "0000-0001-8574-0275"},
                    {"type": "ORCID", "value": "0000-0001-9005-1773"},
                    {"type": "ORCID", "value": "0000-0001-9799-0546"},
                    {"type": "ORCID", "value": "0000-0002-0056-0513"},
                    {"type": "ORCID", "value": "0000-0002-0647-8158"},
                    {"type": "ORCID", "value": "0000-0002-0956-2822"},
                    {"type": "ORCID", "value": "0000-0002-1711-7147"},
                    {"type": "ORCID", "value": "0000-0002-2435-1900"},
                    {"type": "ORCID", "value": "0000-0002-2632-3057"},
                    {"type": "ORCID", "value": "0000-0002-3825-591X"},
                    {"type": "ORCID", "value": "0000-0002-4133-1709"},
                    {"type": "ORCID", "value": "0000-0002-5430-7564"},
                    {"type": "ORCID", "value": "0000-0002-5440-9712"},
                    {"type": "ORCID", "value": "0000-0002-5834-2922"},
                    {"type": "ORCID", "value": "0000-0002-5941-2806"},
                    {"type": "ORCID", "value": "0000-0002-6597-9550"},
                    {"type": "ORCID", "value": "0000-0002-9643-8482"},
                    {"type": "ORCID", "value": "0000-0003-0418-5244"},
                    {"type": "ORCID", "value": "0000-0003-1639-0560"},
                    {"type": "ORCID", "value": "0000-0003-2808-0009"},
                    {"type": "ORCID", "value": "0000-0003-2946-0038"},
                    {"type": "ORCID", "value": "0000-0003-2949-7156"},
                    {"type": "ORCID", "value": "0000-0003-3709-7873"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics"]},
            "journalInfo": {
                "issue": "11",
                "volume": "11",
                "journalIssueId": 3449910,
                "dateOfPublication": "2022 Nov",
                "monthOfPublication": 11,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-11-01",
                "journal": {
                    "title": "Biology open",
                    "medlineAbbreviation": "Biol Open",
                    "isoabbreviation": "Biol Open",
                    "nlmid": "101578018",
                    "issn": "2046-6390",
                    "essn": "2046-6390",
                },
            },
            "pubYear": "2022",
            "pageInfo": "bio059561",
            "abstractText": "Euglenoids (Euglenida) are unicellular flagellates possessing exceptionally wide geographical and ecological distribution. Euglenoids combine a biotechnological potential with a unique position in the eukaryotic tree of life. In large part these microbes owe this success to diverse genetics including secondary endosymbiosis and likely additional sources of genes. Multiple euglenoid species have translational applications and show great promise in production of biofuels, nutraceuticals, bioremediation, cancer treatments and more exotically as robotics design simulators. An absence of reference genomes currently limits these applications, including development of efficient tools for identification of critical factors in regulation, growth or optimization of metabolic pathways. The Euglena International Network (EIN) seeks to provide a forum to overcome these challenges. EIN has agreed specific goals, mobilized scientists, established a clear roadmap (Grand Challenges), connected academic and industry stakeholders and is currently formulating policy and partnership principles to propel these efforts in a coordinated and efficient manner.",
            "affiliation": "European Molecular Biology Laboratory, European Bioinformatics Institute, Wellcome Genome Campus, Hinxton, Cambridge CB10 1SD, UK.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {"pubType": ["Journal Article"]},
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Euglena",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "PH",
                                    "qualifierName": "physiology",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "N", "descriptorName": "Biotechnology"},
                    {"majorTopic_YN": "N", "descriptorName": "Symbiosis"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Biotechnology",
                    "Euglena",
                    "Bioremediation",
                    "Networks",
                    "Biofuels",
                    "Food Supplements",
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1242/bio.059561",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-23",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "dateOfRevision": "2022-12-14",
            "electronicPublicationDate": "2022-11-22",
            "firstPublicationDate": "2022-11-22",
        },
        {
            "id": "36410948",
            "source": "MED",
            "pmid": "36410948",
            "doi": "10.1016/bs.mie.2022.08.008",
            "title": "Technical considerations for small-angle neutron scattering from biological macromolecules in solution: Cross sections, contrasts, instrument setup and measurement.",
            "authorString": "Pietras Z, Wood K, Whitten AE, Jeffries CM.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Pietras Z",
                        "firstName": "Zuzanna",
                        "lastName": "Pietras",
                        "initials": "Z",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Physics, Chemistry, and Biology, Linköping University, Linköping, Sweden."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Wood K",
                        "firstName": "Kathleen",
                        "lastName": "Wood",
                        "initials": "K",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation, New Illawarra Rd, Lucas Heights, NSW, Australia."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Whitten AE",
                        "firstName": "Andrew E",
                        "lastName": "Whitten",
                        "initials": "AE",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Australian Nuclear Science and Technology Organisation, New Illawarra Rd, Lucas Heights, NSW, Australia. Electronic address: andrew.whitten@ansto.gov.au."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jeffries CM",
                        "firstName": "Cy M",
                        "lastName": "Jeffries",
                        "initials": "CM",
                        "authorId": {"type": "ORCID", "value": "0000-0002-8718-7343"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "European Molecular Biology Laboratory (EMBL), Hamburg, Germany. Electronic address: cy.jeffries@embl-hamburg.de."
                                }
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [{"type": "ORCID", "value": "0000-0002-8718-7343"}]
            },
            "journalInfo": {
                "volume": "677",
                "journalIssueId": 3461591,
                "dateOfPublication": "2022 ",
                "monthOfPublication": 0,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-01-01",
                "journal": {
                    "title": "Methods in enzymology",
                    "medlineAbbreviation": "Methods Enzymol",
                    "isoabbreviation": "Methods Enzymol",
                    "nlmid": "0212271",
                    "issn": "0076-6879",
                    "essn": "1557-7988",
                },
            },
            "pubYear": "2022",
            "pageInfo": "157-189",
            "abstractText": "Small angle scattering affords an approach to evaluate the structure of dilute populations of macromolecules in solution where the measured scattering intensities relate to the distribution of scattering-pair distances within each macromolecule. When small angle neutron scattering (SANS) with contrast variation is employed, additional structural information can be obtained regarding the internal organization of biomacromolecule complexes and assemblies. The technique allows for the components of assemblies to be selectively 'matched in' and 'matched out' of the scattering profiles due to the different ways the isotopes of hydrogen-protium <sup>1</sup>H, and deuterium <sup>2</sup>H (or D)-scatter neutrons. The isotopic substitution of <sup>1</sup>H for D in the sample enables the controlled variation of the scattering contrasts. A contrast variation experiment requires trade-offs between neutron beam intensity, q-range, wavelength and q-resolution, isotopic labelling levels, sample concentration and path-length, and measurement times. Navigating these competing aspects to find an optimal combination is a daunting task. Here we provide an overview of how to calculate the neutron scattering contrasts of dilute biological macromolecule samples prior to an experiment and how this then informs the approach to configuring SANS instruments and the measurement of a contrast variation series dataset.",
            "affiliation": "Department of Physics, Chemistry, and Biology, Linköping University, Linköping, Sweden.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": ["Research Support, Non-U.S. Gov't", "Journal Article"]
            },
            "grantsList": {
                "grant": [
                    {"agency": "Australian Government", "orderIn": 0},
                    {
                        "grantId": "871037",
                        "agency": "European Commission",
                        "orderIn": 0,
                    },
                    {"agency": "Stiftelsen för\xa0Strategisk Forskning", "orderIn": 0},
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Hydrogen",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Macromolecular Substances",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "CH",
                                    "qualifierName": "chemistry",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Neutron Diffraction",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "MT",
                                    "qualifierName": "methods",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Neutrons"},
                    {"majorTopic_YN": "N", "descriptorName": "Scattering, Small Angle"},
                ]
            },
            "keywordList": {
                "keyword": [
                    "Biological macromolecule",
                    "contrast",
                    "Sans",
                    "Contrast Variation",
                    "Dilute Solution Scattering",
                    "Sans Instrument",
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "Macromolecular Substances", "registryNumber": "0"},
                    {"name": "Hydrogen", "registryNumber": "7YNJ3PO35Z"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1016/bs.mie.2022.08.008",
                    }
                ]
            },
            "isOpenAccess": "N",
            "inEPMC": "N",
            "inPMC": "N",
            "hasPDF": "N",
            "hasBook": "N",
            "hasSuppl": "N",
            "citedByCount": 0,
            "hasData": "N",
            "hasReferences": "N",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "N",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "N",
            "dateOfCompletion": "2022-11-23",
            "dateOfCreation": "2022-11-21",
            "firstIndexDate": "2022-11-22",
            "dateOfRevision": "2022-11-30",
            "electronicPublicationDate": "2022-09-22",
            "firstPublicationDate": "2022-09-22",
        },
        {
            "id": "36413626",
            "source": "MED",
            "pmid": "36413626",
            "pmcid": "PMC9756341",
            "fullTextIdList": {"fullTextId": ["PMC9756341"]},
            "doi": "10.1021/jacs.2c07378",
            "title": "A KLK6 Activity-Based Probe Reveals a Role for KLK6 Activity in Pancreatic Cancer Cell Invasion.",
            "authorString": "Zhang L, Lovell S, De Vita E, Jagtap PKA, Lucy D, Goya Grocin A, Kjær S, Borg A, Hennig J, Miller AK, Tate EW.",
            "authorList": {
                "author": [
                    {
                        "fullName": "Zhang L",
                        "firstName": "Leran",
                        "lastName": "Zhang",
                        "initials": "L",
                        "authorId": {"type": "ORCID", "value": "0000-0002-1051-8132"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Lovell S",
                        "firstName": "Scott",
                        "lastName": "Lovell",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Life Sciences, University of Bath, Bath BA2 7AX, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "De Vita E",
                        "firstName": "Elena",
                        "lastName": "De Vita",
                        "initials": "E",
                        "authorId": {"type": "ORCID", "value": "0000-0003-4707-8342"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Jagtap PKA",
                        "firstName": "Pravin Kumar Ankush",
                        "lastName": "Jagtap",
                        "initials": "PKA",
                        "authorId": {"type": "ORCID", "value": "0000-0002-9457-4130"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                                {
                                    "affiliation": "Chair of Biochemistry IV, Biophysical Chemistry, University of Bayreuth, Bayreuth 95447, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Lucy D",
                        "firstName": "Daniel",
                        "lastName": "Lucy",
                        "initials": "D",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Goya Grocin A",
                        "firstName": "Andrea",
                        "lastName": "Goya Grocin",
                        "initials": "A",
                        "authorId": {"type": "ORCID", "value": "0000-0002-6914-7639"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Kjær S",
                        "firstName": "Svend",
                        "lastName": "Kjær",
                        "initials": "S",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural Biology Science Technology Platform, The Francis Crick Institute, London NW1 1AT, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Borg A",
                        "firstName": "Annabel",
                        "lastName": "Borg",
                        "initials": "A",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural Biology Science Technology Platform, The Francis Crick Institute, London NW1 1AT, U.K."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Hennig J",
                        "firstName": "Janosch",
                        "lastName": "Hennig",
                        "initials": "J",
                        "authorId": {"type": "ORCID", "value": "0000-0001-5214-7002"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg 69117, Germany."
                                },
                                {
                                    "affiliation": "Chair of Biochemistry IV, Biophysical Chemistry, University of Bayreuth, Bayreuth 95447, Germany."
                                },
                            ]
                        },
                    },
                    {
                        "fullName": "Miller AK",
                        "firstName": "Aubry K",
                        "lastName": "Miller",
                        "initials": "AK",
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Cancer Drug Development Group, German Cancer Research Center (DKFZ), Heidelberg 69120, Germany."
                                }
                            ]
                        },
                    },
                    {
                        "fullName": "Tate EW",
                        "firstName": "Edward W",
                        "lastName": "Tate",
                        "initials": "EW",
                        "authorId": {"type": "ORCID", "value": "0000-0003-2213-5814"},
                        "authorAffiliationDetailsList": {
                            "authorAffiliation": [
                                {
                                    "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K."
                                }
                            ]
                        },
                    },
                ]
            },
            "authorIdList": {
                "authorId": [
                    {"type": "ORCID", "value": "0000-0001-5214-7002"},
                    {"type": "ORCID", "value": "0000-0002-1051-8132"},
                    {"type": "ORCID", "value": "0000-0002-6914-7639"},
                    {"type": "ORCID", "value": "0000-0002-9457-4130"},
                    {"type": "ORCID", "value": "0000-0003-2213-5814"},
                    {"type": "ORCID", "value": "0000-0003-4707-8342"},
                ]
            },
            "dataLinksTagsList": {"dataLinkstag": ["altmetrics", "supporting_data"]},
            "journalInfo": {
                "issue": "49",
                "volume": "144",
                "journalIssueId": 3475101,
                "dateOfPublication": "2022 Dec",
                "monthOfPublication": 12,
                "yearOfPublication": 2022,
                "printPublicationDate": "2022-12-01",
                "journal": {
                    "title": "Journal of the American Chemical Society",
                    "medlineAbbreviation": "J Am Chem Soc",
                    "isoabbreviation": "J Am Chem Soc",
                    "nlmid": "7503056",
                    "issn": "0002-7863",
                    "essn": "1520-5126",
                },
            },
            "pubYear": "2022",
            "pageInfo": "22493-22504",
            "abstractText": "Pancreatic cancer has the lowest survival rate of all common cancers due to late diagnosis and limited treatment options. Serine hydrolases are known to mediate cancer progression and metastasis through initiation of signaling cascades and cleavage of extracellular matrix proteins, and the kallikrein-related peptidase (KLK) family of secreted serine proteases have emerging roles in pancreatic ductal adenocarcinoma (PDAC). However, the lack of reliable activity-based probes (ABPs) to profile KLK activity has hindered progress in validation of these enzymes as potential targets or biomarkers. Here, we developed potent and selective ABPs for KLK6 by using a positional scanning combinatorial substrate library and characterized their binding mode and interactions by X-ray crystallography. The optimized KLK6 probe IMP-2352 (<i>k</i><sub>obs</sub>/<i>I</i> = 11,000 M<sup>-1</sup> s<sup>-1</sup>) enabled selective detection of KLK6 activity in a variety of PDAC cell lines, and we observed that KLK6 inhibition reduced the invasiveness of PDAC cells that secrete active KLK6. KLK6 inhibitors were combined with N-terminomics to identify potential secreted protein substrates of KLK6 in PDAC cells, providing insights into KLK6-mediated invasion pathways. These novel KLK6 ABPs offer a toolset to validate KLK6 and associated signaling partners as targets or biomarkers across a range of diseases.",
            "affiliation": "Department of Chemistry, Molecular Sciences Research Hub, Imperial College London, London W12 0BZ, U.K.",
            "publicationStatus": "ppublish",
            "language": "eng",
            "pubModel": "Print-Electronic",
            "pubTypeList": {
                "pubType": [
                    "Research Support, Non-U.S. Gov't",
                    "research-article",
                    "Journal Article",
                    "Research Support, N.I.H., Extramural",
                ]
            },
            "grantsList": {
                "grant": [
                    {
                        "grantId": "C24523/A25192",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "C29637/A21451",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "890900",
                        "agency": "H2020 Marie Sklodowska-Curie Actions",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "19-0059",
                        "agency": "Worldwide Cancer Research",
                        "acronym": "AICR_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "C29637/A20183",
                        "agency": "Cancer Research UK",
                        "acronym": "CRUK_",
                        "orderIn": 0,
                    },
                    {
                        "grantId": "P41 GM103311",
                        "agency": "NIGMS NIH HHS",
                        "acronym": "GM",
                        "orderIn": 0,
                    },
                    {
                        "agency": "Engineering and Physical Sciences Research Council",
                        "orderIn": 0,
                    },
                ]
            },
            "meshHeadingList": {
                "meshHeading": [
                    {"majorTopic_YN": "N", "descriptorName": "Humans"},
                    {
                        "majorTopic_YN": "Y",
                        "descriptorName": "Carcinoma, Pancreatic Ductal",
                    },
                    {"majorTopic_YN": "Y", "descriptorName": "Pancreatic Neoplasms"},
                    {"majorTopic_YN": "N", "descriptorName": "Neoplasm Invasiveness"},
                    {
                        "majorTopic_YN": "N",
                        "descriptorName": "Kallikreins",
                        "meshQualifierList": {
                            "meshQualifier": [
                                {
                                    "abbreviation": "ME",
                                    "qualifierName": "metabolism",
                                    "majorTopic_YN": "N",
                                }
                            ]
                        },
                    },
                ]
            },
            "chemicalList": {
                "chemical": [
                    {"name": "KLK6 protein, human", "registryNumber": "EC 3.4.21.-"},
                    {"name": "Kallikreins", "registryNumber": "EC 3.4.21.-"},
                ]
            },
            "subsetList": {"subset": [{"code": "IM", "name": "Index Medicus"}]},
            "fullTextUrlList": {
                "fullTextUrl": [
                    {
                        "availability": "Subscription required",
                        "availabilityCode": "S",
                        "documentStyle": "doi",
                        "site": "DOI",
                        "url": "https://doi.org/10.1021/jacs.2c07378",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "html",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9756341",
                    },
                    {
                        "availability": "Open access",
                        "availabilityCode": "OA",
                        "documentStyle": "pdf",
                        "site": "Europe_PMC",
                        "url": "https://europepmc.org/articles/PMC9756341?pdf=render",
                    },
                ]
            },
            "isOpenAccess": "Y",
            "inEPMC": "Y",
            "inPMC": "N",
            "hasPDF": "Y",
            "hasBook": "N",
            "hasSuppl": "Y",
            "citedByCount": 0,
            "hasData": "Y",
            "hasReferences": "Y",
            "hasTextMinedTerms": "Y",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "license": "cc by",
            "authMan": "N",
            "epmcAuthMan": "N",
            "nihAuthMan": "N",
            "hasTMAccessionNumbers": "Y",
            "tmAccessionTypeList": {"accessionType": ["pdb"]},
            "dateOfCompletion": "2022-12-15",
            "dateOfCreation": "2022-11-22",
            "firstIndexDate": "2022-11-23",
            "fullTextReceivedDate": "2022-12-20",
            "dateOfRevision": "2022-12-22",
            "electronicPublicationDate": "2022-11-22",
            "firstPublicationDate": "2022-11-22",
        },
    ]
