import numpy as np
import pandas as pd
from utils.fieldset import StandardNames


class MalacoDfs:
    df_0 = pd.DataFrame(
        {
            StandardNames.doi: [
                np.nan,
                "10.1093/mollus/eyp029",
                "10.1111/j.1463-6395.2006.00240.x",
            ],
            StandardNames.title: [
                "Radiation and decline of endodontid land snails in Makatea, French Polynesia",
                np.nan,
                np.nan,
            ],
        }
    )
    df_1 = pd.DataFrame(
        {
            StandardNames.doi: [
                "10.11646/zootaxa.3772.1.1",
                np.nan,
                "10.1111/j.1463-6395.2006.00240.x",
            ],
            StandardNames.title: [
                np.nan,
                "Morphology and postlarval development of the ligament of Thracia phaseolina (Bivalvia: Thraciidae), "
                "with a discussion of model choice in allometric studies",
                "Arenophilic mantle glands in the Laternulidae (Bivalvia: Anomalodesmata) "
                "and their evolutionary significance",
            ],
        }
    )
    df_2 = pd.DataFrame(
        {
            StandardNames.doi: [
                "10.1111/j.1502-3931.2009.00166.x",
                "10.11646/zootaxa.3772.1.1",
                "10.1093/mollus/eyp029",
                np.nan,
            ],
            StandardNames.title: [
                "Sticky bivalves from the Mesozoic: clues to the origin of the anomalodesmatan arenophilic system",
                "Radiation and decline of endodontid land snails in Makatea, French Polynesia",
                "Morphology and postlarval development of the ligament of Thracia phaseolina (Bivalvia: Thraciidae), "
                "with a discussion of model choice in allometric studies",
                "Anthropogenic extinction of Pacific land snails: A case study of Rurutu, French Polynesia, "
                "with description of eight new species of endodontids (Pulmonata)",
            ],
        }
    )
