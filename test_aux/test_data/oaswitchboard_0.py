import pandas as pd
from numpy import nan


class OaswitchboardData0:
    row_0 = pd.Series(
        {
            "acceptance_date": "2023-05-19",
            "authors": [
                {
                    "lastName": "Hawkins",
                    "firstName": "Dorothy E D P",
                    "initials": "DEDPH",
                    "ORCID": "https://orcid.org/0000-0001-9117-680X",
                    "collaboration": "",
                    "listingorder": 1,
                    "creditroles": [],
                    "isCorrespondingAuthor": True,
                    "institutions": [
                        {
                            "name": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                            "ror": "https://ror.org/04m01e293",
                        }
                    ],
                    "affiliation": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                    "currentaddress": [],
                },
                {
                    "lastName": "Bayfield",
                    "firstName": "Oliver\xa0W",
                    "initials": "OB",
                    "ORCID": "https://orcid.org/0000-0003-1421-7780",
                    "collaboration": "",
                    "listingorder": 2,
                    "creditroles": [],
                    "isCorrespondingAuthor": False,
                    "institutions": [
                        {
                            "name": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                            "ror": "https://ror.org/04m01e293",
                        }
                    ],
                    "affiliation": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                    "currentaddress": [],
                },
                {
                    "lastName": "Fung",
                    "firstName": "Herman K H",
                    "initials": "HKHF",
                    "ORCID": "https://orcid.org/0000-0001-9568-1782",
                    "collaboration": "",
                    "listingorder": 3,
                    "creditroles": [],
                    "isCorrespondingAuthor": False,
                    "institutions": [
                        {
                            "name": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Germany",
                            "ror": "https://ror.org/03mstc592",
                        }
                    ],
                    "affiliation": "Structural and Computational Biology Unit, European Molecular Biology Laboratory, Germany",
                    "currentaddress": [],
                },
                {
                    "lastName": "Grba",
                    "firstName": "Daniel N",
                    "initials": "DNG",
                    "ORCID": "",
                    "collaboration": "",
                    "listingorder": 4,
                    "creditroles": [],
                    "isCorrespondingAuthor": False,
                    "institutions": [
                        {
                            "name": "MRC Mitochondrial Biology Unit, University of Cambridge, The Keith Peters Building, Cambridge Biomedical Campus, UK",
                            "ror": "https://ror.org/01vdt8f48",
                        }
                    ],
                    "affiliation": "MRC Mitochondrial Biology Unit, University of Cambridge, The Keith Peters Building, Cambridge Biomedical Campus, UK",
                    "currentaddress": [],
                },
                {
                    "lastName": "Huet",
                    "firstName": "Alexis",
                    "initials": "AH",
                    "ORCID": "",
                    "collaboration": "",
                    "listingorder": 5,
                    "creditroles": [],
                    "isCorrespondingAuthor": False,
                    "institutions": [
                        {
                            "name": "Department of Structural Biology, School of Medicine, University of Pittsburgh, USA",
                            "ror": "https://ror.org/0232r4451",
                        }
                    ],
                    "affiliation": "Department of Structural Biology, School of Medicine, University of Pittsburgh, USA",
                    "currentaddress": [],
                },
                {
                    "lastName": "Conway",
                    "firstName": "James\xa0F",
                    "initials": "JC",
                    "ORCID": "",
                    "collaboration": "",
                    "listingorder": 6,
                    "creditroles": [],
                    "isCorrespondingAuthor": False,
                    "institutions": [
                        {
                            "name": "Department of Structural Biology, School of Medicine, University of Pittsburgh, USA",
                            "ror": "https://ror.org/0232r4451",
                        }
                    ],
                    "affiliation": "Department of Structural Biology, School of Medicine, University of Pittsburgh, USA",
                    "currentaddress": [],
                },
                {
                    "lastName": "Antson",
                    "firstName": "Alfred A",
                    "initials": "AAA",
                    "ORCID": "https://orcid.org/0000-0002-4533-3816",
                    "collaboration": "",
                    "listingorder": 7,
                    "creditroles": [],
                    "isCorrespondingAuthor": True,
                    "institutions": [
                        {
                            "name": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                            "ror": "https://ror.org/04m01e293",
                        }
                    ],
                    "affiliation": "York Structural Biology Laboratory, Department of Chemistry, University of York, UK",
                    "currentaddress": [],
                },
            ],
            "doi": "https://doi.org/10.1093/nar/gkad480",
            "issn": "1362-4962",
            "journal_name": "Nucleic Acids Research",
            "journal_oa_status": "pure OA journal",
            "licence": "CC BY",
            "preprint_url": nan,
            "publication_date": "2023-06-09",
            "publication_type": "research-article",
            "submission_date": "2023-03-16",
            "title": "Insights into a viral motor: the structure of the HK97 packaging termination assembly",
            "provenance": '{"acceptance_date": "oaswitchboard - data.article.manuscript.dates.acceptance", "authors": "oaswitchboard - data.authors", "doi": "oaswitchboard - data.article.doi", "issn": "oaswitchboard - data.journal.id", "journal_name": "oaswitchboard - data.journal.name", "journal_oa_status": "oaswitchboard - data.article.vor.publication", "licence": "oaswitchboard - data.article.vor.license", "preprint_url": "oaswitchboard - data.article.preprint.url", "publication_date": "oaswitchboard - data.article.manuscript.dates.publication", "publication_type": "oaswitchboard - data.article.type", "submission_date": "oaswitchboard - data.article.manuscript.dates.submission", "title": "oaswitchboard - data.article.title"}',
        }
    )
    row_1 = pd.Series(
        {
            "acceptance_date": "2023-05-11",
            "authors": [
                {
                    "listingorder": 1,
                    "lastName": "Castillo-Venzor",
                    "firstName": "Aracely",
                    "ORCID": "0000-0002-2288-2679",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 2,
                    "lastName": "Penfold",
                    "firstName": "Christopher",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 3,
                    "lastName": "Morgan",
                    "firstName": "Michael",
                    "ORCID": "0000-0003-0757-0711",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 4,
                    "lastName": "Tang",
                    "firstName": "Walfred",
                    "ORCID": "0000-0002-5803-1681",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 5,
                    "lastName": "Kobayashi",
                    "firstName": "Toshihiro",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 6,
                    "lastName": "Wong",
                    "firstName": "Frederick",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 7,
                    "lastName": "Bergmann",
                    "firstName": "Sophie",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 8,
                    "lastName": "Slatery",
                    "firstName": "Erin",
                    "ORCID": "0000-0002-9097-6618",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 9,
                    "lastName": "Boroviak",
                    "firstName": "Thorsten",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 10,
                    "lastName": "Marioni",
                    "firstName": "John",
                    "ORCID": "",
                    "isCorrespondingAuthor": False,
                },
                {
                    "listingorder": 11,
                    "lastName": "Surani",
                    "firstName": "Azim",
                    "ORCID": "0000-0002-8640-4318",
                    "isCorrespondingAuthor": True,
                    "institutions": [
                        {
                            "name": "Wellcome/Cancer Research UK Gurdon Institute",
                            "ror": "https://ror.org/00fp3ce15",
                        }
                    ],
                    "affiliation": "https://ror.org/00fp3ce15",
                },
            ],
            "doi": "https://doi.org/10.26508/lsa.202201706",
            "issn": "2575-1077",
            "journal_name": "Life Science Alliance",
            "journal_oa_status": "pure OA journal",
            "licence": "CC BY",
            "preprint_url": nan,
            "publication_date": "2023-05-22",
            "publication_type": "research-article",
            "submission_date": "2022-09-03",
            "title": "Origin and segregation of the human germlineOrigin and segregation of the human germline",
            "provenance": '{"acceptance_date": "oaswitchboard - data.article.manuscript.dates.acceptance", "authors": "oaswitchboard - data.authors", "doi": "oaswitchboard - data.article.doi", "issn": "oaswitchboard - data.journal.id", "journal_name": "oaswitchboard - data.journal.name", "journal_oa_status": "oaswitchboard - data.article.vor.publication", "licence": "oaswitchboard - data.article.vor.license", "preprint_url": "oaswitchboard - data.article.preprint.url", "publication_date": "oaswitchboard - data.article.manuscript.dates.publication", "publication_type": "oaswitchboard - data.article.type", "submission_date": "oaswitchboard - data.article.manuscript.dates.submission", "title": "oaswitchboard - data.article.title"}',
        }
    )
