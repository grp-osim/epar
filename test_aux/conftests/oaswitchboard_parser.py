import pytest
from oaswitchboard.oaswitchboard_parser import OaswitchboardExportParser
from test_aux.testing_utils import OASWITCHBOARD_DATASET_0


@pytest.fixture
def oaswitchboard_parser_0(parkin_parser_1):
    return OaswitchboardExportParser(
        input_file_path=OASWITCHBOARD_DATASET_0, orcid_export_parser=parkin_parser_1
    )


@pytest.fixture
def oaswitchboard_parser_1(oaswitchboard_parser_0):
    """
    Switchboard parser with a dataframe containing only the first 10 rows
    of OASWITCHBOARD_DATASET_0
    """
    oep = oaswitchboard_parser_0
    oep.df = oep.df.loc[:10]
    return oep
