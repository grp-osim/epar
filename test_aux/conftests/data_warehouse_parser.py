import pytest
from data_warehouse.data_warehouse_parser import DataWarehouseParser
from test_aux.test_data.data_warehouse_results_0 import WarehouseResult0


@pytest.fixture
def warehouse_parser_0():
    return DataWarehouseParser(warehouse_results=WarehouseResult0.result)
