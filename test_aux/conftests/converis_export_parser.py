import pytest
from test_aux.testing_utils import CONVERIS_DATASET_0, CONVERIS_DATASET_1, WOS_DATASET_0
from converis.converis_parser import ConverisExportParser


# region ConverisExportParser
@pytest.fixture
def converis_parser_0():
    """
    Default parser for TEST_DATASET_0
    """
    cep = ConverisExportParser(input_file_path=CONVERIS_DATASET_0)
    cep.df.drop(columns=["provenance"], inplace=True)
    return cep


@pytest.fixture
def converis_parser_1(converis_parser_0):
    """
    Parser for test dataset TEST_DATASET_0, with pandas dataframe sanitised
    """
    cep = converis_parser_0
    cep.sanitise_dataframe()
    cep.parse_df_data()
    return cep


@pytest.fixture
def converis_parser_2(converis_parser_1):
    """
    Parser for test dataset TEST_DATASET_0, with pandas dataframe sanitised,
    preprints, corrections, etc excluded
    """
    cep = converis_parser_1
    cep.prune_dataframe()
    return cep


@pytest.fixture(scope="module")
def module_parser_0():
    """
    Parser for test dataset TEST_DATASET_0, with pandas dataframe sanitised,
    truncated for first 10 rows
    """
    cep = ConverisExportParser(input_file_path=CONVERIS_DATASET_0)
    cep.df = cep.df.truncate(before=0, after=9)
    cep.sanitise_dataframe()
    cep.parse_df_data()
    return cep


@pytest.fixture(scope="module")
def converis_module_parser_1():
    """
    Parser for test dataset CONVERIS_DATASET_1
    """
    cep = ConverisExportParser(input_file_path=CONVERIS_DATASET_1)
    cep.df.drop(columns=["provenance"], inplace=True)
    return cep


# endregion
