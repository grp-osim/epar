import pytest
from freya_ror_predictor.freya_ror_parser import FreyaRorPredictorExportParser
from test_aux.testing_utils import FREYA_DATASET_0


@pytest.fixture
def freya_parser_0():
    fep = FreyaRorPredictorExportParser(input_file_path=FREYA_DATASET_0)
    fep.df.drop(columns=["provenance"], inplace=True)
    return fep
