import pandas as pd
import pytest
from openalex.openalex_parser import OpenAlexParser
from test_aux.test_data.openalex_results_0 import OpenAlexResult4
from test_aux.test_data.openalex_df_0 import OpenAlexDf0


@pytest.fixture
def alex_parser_0(parkin_parser_1):
    return OpenAlexParser(
        openalex_results=OpenAlexResult4.result, orcid_export_parser=parkin_parser_1
    )


@pytest.fixture
def alex_parser_1():
    return OpenAlexParser(openalex_results=pd.DataFrame.from_dict(OpenAlexDf0.as_dict))
