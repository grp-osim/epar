import pandas as pd
import pytest

from utils.data_container import DataContainer
from test_aux.test_data.tinydfs_0 import MalacoDfs
from utils.fieldset import StandardNames


@pytest.fixture
def data_container_0():
    df = MalacoDfs.df_0
    return DataContainer(df[pd.notnull(df[StandardNames.doi])], "_dc0")


@pytest.fixture
def data_container_1():
    df = MalacoDfs.df_1
    return DataContainer(df[pd.notnull(df[StandardNames.doi])], "_dc1")


@pytest.fixture
def data_container_2():
    return DataContainer(MalacoDfs.df_0, "_dc0")


@pytest.fixture
def data_container_3():
    return DataContainer(MalacoDfs.df_1, "_dc1")


@pytest.fixture
def data_container_4():
    df = MalacoDfs.df_2
    return DataContainer(df[pd.notnull(df[StandardNames.doi])], "_dc2")
