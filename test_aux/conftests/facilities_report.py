import pytest
from reports.facilities import FacilitiesReport
from test_aux.testing_utils import FACILITIES_DATASET_0


@pytest.fixture(scope="module")
def facilities_report_0(parkin_parser_1):
    return FacilitiesReport(
        input_file_path=FACILITIES_DATASET_0, orcid_export_parser=parkin_parser_1
    )


@pytest.fixture(scope="module")
def facilities_report_1(facilities_report_0):
    """
    Sample of facilities_report_0 containing only 10 rows
    """
    fc = facilities_report_0
    fc.df = fc.df.sample(n=10, random_state=1)
    return fc


@pytest.fixture(scope="module")
def facilities_report_2(facilities_report_1):
    fc = facilities_report_1
    fc.get_data_merger()
    fc.resolve_collaborations()
    return fc
