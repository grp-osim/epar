import pytest
from orcid.parkin_parser import OrcidExportParser
from test_aux.testing_utils import PARKIN_DATASET_0_RAW, PARKIN_DATASET_0_PUBS


@pytest.fixture
def parkin_parser_0():
    return OrcidExportParser(
        raw_file_path=PARKIN_DATASET_0_RAW,
        pubs_file_path=PARKIN_DATASET_0_PUBS,
        year="2022",
    )


@pytest.fixture(scope="module")
def parkin_parser_1():
    return OrcidExportParser(
        raw_file_path=PARKIN_DATASET_0_RAW,
        pubs_file_path=PARKIN_DATASET_0_PUBS,
    )
