import json
import numpy as np
import pandas as pd
from abc import ABC
from osim_utils.clients.ror import RorClient
from osim_utils.logger import get_logger
from typing import Optional, Union

from ror.embl_ids import ror_id2site
from utils.countries import CountryDetector
from utils.fieldset import FieldNames, StandardNames as sn
from utils.validators import ReValidator


class DataContainer(ABC):
    """
    Abstract class representing a set of publication data from one of various
    data sources (Converis, EPMC, Open Alex, etc).
    """

    def __init__(self, df: pd.DataFrame, data_label: str, **kwargs):
        """
        Args:
            df: pandas dataframe containing the data
            data_label: a tag specifying the provenance of the data, which
                can be used to label columns of the class' dataframe before
                merging with another instance (e.g. _converis)
        """
        self.df = df
        self.data_label = data_label
        self.validator = ReValidator()
        self.ror_client = RorClient()
        self.country_detector = CountryDetector()
        self.orcid_parser = kwargs.pop("orcid_export_parser", None)
        self.logger = get_logger()

    def check_doi(self, doi: str) -> Union[str, np.float64]:
        """
        Checks that doi value is valid and converts it to lowercase

        Args:
            doi: a row of the dataframe (i.e. a publication)

        Returns: Doi of input row converted to lowercase
        """
        try:
            doi = doi.strip(" .")
        except AttributeError:
            return np.nan
        if self.validator.doi_valid(doi, doi):
            return doi.lower()
        return np.nan

    @staticmethod
    def get_orcid_from_url(orcid_url: str) -> Union[str, np.float64]:
        if not isinstance(orcid_url, str):
            return np.nan
        return orcid_url.split("/")[-1].strip()

    def convert_doi_url(self, doi_url: str) -> Union[str, np.float64]:
        """
        Converts doi_url to a DOI value
        """
        return self.check_doi(doi_url.replace("https://doi.org/", ""))

    def rename_df_columns(
        self, fieldnames: FieldNames, df: Optional[pd.DataFrame] = None
    ) -> pd.DataFrame:
        """
        Renames df column names to standard field names using
        the mapping in fieldnames

        Args:
            fieldnames: dataclass instance mapping df column names to standard names
            df: pandas dataframe
        """
        names_map = {}
        provenance_dict = {}
        for k, v in fieldnames.__dict__.items():
            if v:
                names_map[v] = k
                provenance_dict[k] = f"{self.data_label[1:]} - {v}"
        if df is None:
            df = self.df
        df[sn.provenance] = json.dumps(provenance_dict)
        return df.rename(columns=names_map)

    def label_df(self) -> pd.DataFrame:
        """
        Returns a copy of self.df with self.data_label appended to column names.
        Useful for keeping provenance of data when merging with other datasources
        """
        return self.df.rename(columns=lambda x: f"{x}{self.data_label}")

    @staticmethod
    def append_names_map_with_calculated_author_attributes(names_map: dict) -> dict:
        names_map[sn.embl_author] = sn.embl_author
        names_map[sn.source_embl_author] = sn.source_embl_author
        names_map[sn.confidence_embl_author] = sn.confidence_embl_author
        return names_map

    @staticmethod
    def set_default_values_of_calculated_fields_in_author_dataframe(
        authors_df: pd.DataFrame,
    ) -> pd.DataFrame:
        authors_df[sn.embl_author] = False
        authors_df[sn.source_embl_author] = np.nan
        authors_df[sn.confidence_embl_author] = np.nan
        return authors_df

    def get_organisation_name_and_country(self, row: pd.Series) -> tuple:
        try:
            ror_id = row[sn.ror_id]
        except (AttributeError, KeyError):
            ror_id = np.nan

        if ror_id and pd.notna(ror_id):
            org = self.ror_client.get_organisation_by_id(ror_id)
            return org["name"], org["country"]["country_code"]
        else:
            name = row[sn.name]
            country_set = self.country_detector.detect_countries(name)
            if country_set:
                country_code = self.country_detector.name2alpha2.get(
                    country_set.pop().lower(), np.nan
                )
                return name, country_code
            else:
                return name, np.nan

    def is_embl_institution(self, row: pd.Series) -> tuple[bool, str, int]:
        try:
            if row[sn.ror_id] in ror_id2site.keys():
                return True, "ror", 9
        except (AttributeError, KeyError):
            return np.nan, np.nan, np.nan
        else:
            return False, np.nan, np.nan

    def detect_embl_authors_and_institution_countries(
        self, authors_df: pd.DataFrame
    ) -> pd.DataFrame:
        for index, institution_df in authors_df.institutions.items():
            if not institution_df.empty:
                institution_df[[sn.name, sn.country_code]] = institution_df.apply(
                    self.get_organisation_name_and_country,
                    axis=1,
                    result_type="expand",
                )

                institution_df[
                    [sn.is_embl_site, sn.source_embl_site, sn.confidence_embl_site]
                ] = institution_df.apply(
                    self.is_embl_institution,
                    axis=1,
                    result_type="expand",
                )

                embl_sites_df = institution_df[institution_df.is_embl_site == True]
                if not embl_sites_df.empty:
                    # flag author as embl author
                    authors_df.loc[index, sn.embl_author] = True
                    # using source and confidence from the first detected EMBL site for simplicity
                    authors_df.loc[index, sn.source_embl_author] = embl_sites_df.iloc[
                        0
                    ][sn.source_embl_site]
                    authors_df.loc[
                        index, sn.confidence_embl_author
                    ] = embl_sites_df.iloc[0][sn.confidence_embl_site]

        # parse orcids
        try:
            authors_df.orcid = authors_df.orcid.apply(self.get_orcid_from_url)
        except AttributeError:
            authors_df[sn.orcid] = np.nan
        authors_df[
            [sn.embl_author, sn.source_embl_author, sn.confidence_embl_author]
        ] = authors_df.apply(self.is_embl_orcid, axis=1, result_type="expand")

        return authors_df

    def is_embl_orcid(self, row: pd.Series) -> tuple:
        """
        Attempts to identify EMBL authors by comparing ORCID values to data in SAP.
        If an author is already identified as an EMBL author (based on affiliation data),
        does not do anything
        """
        from orcid.parkin_parser import OrcidExportParser

        if self.orcid_parser is None:
            self.orcid_parser = OrcidExportParser()
        if row[sn.embl_author] is False:
            embl_orcid = self.orcid_parser.is_orcid_in_dataset(row[sn.orcid])
            if embl_orcid is True:
                row[sn.embl_author] = True
                row[sn.source_embl_author] = "orcid"
                row[sn.confidence_embl_author] = 3
        return (
            row[sn.embl_author],
            row[sn.source_embl_author],
            row[sn.confidence_embl_author],
        )

    def get_collaborations(self, row: pd.Series) -> tuple:
        collaborations_quality = "high"
        collaborations_quality_note = np.nan
        collaborations = dict()
        collaborators_df = row.authors[row.authors.embl_author == False]
        for _, institution_df in collaborators_df.institutions.items():
            if institution_df.empty:
                collaborations_quality = "low"
                collaborations_quality_note = (
                    "Affiliation data missing in source data for one or more authors"
                )
            else:
                for inst in institution_df.itertuples(index=False):
                    try:
                        collaborations[inst.country_code].add(inst.name)
                    except KeyError:
                        collaborations[inst.country_code] = {inst.name}
        # sort collaboration sets to facilitate testing
        collaborations = {k: sorted(v) for k, v in collaborations.items()}
        return (
            collaborations,
            collaborations_quality,
            collaborations_quality_note,
        )

    def is_embl_publication(self, row: pd.Series) -> tuple:
        embl_authors_df = row.authors[row.authors.embl_author == True]
        if embl_authors_df.empty:
            return False, np.nan
        else:
            return True, sum(embl_authors_df[sn.confidence_embl_author])
