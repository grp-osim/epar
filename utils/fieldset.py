from dataclasses import dataclass
from typing import Optional


class StandardNames:
    """
    Standard attribute names for use in dataframe columns,
    classes, etc

    Use this class to avoid hardcoding
    """

    # publication attributes
    abstract = "abstract"
    acceptance_date = "acceptance_date"
    addresses = "addresses"
    affiliation = "affiliation"
    authors = "authors"
    converis_id = "converis_id"
    correspondence = "correspondence"
    current_address = "current_address"
    publication_date = "publication_date"
    doi = "doi"
    embl_pub = "embl_pub"
    embl_pub_confidence = "embl_pub_confidence"
    embl_pub_note = "embl_pub_note"
    embl_authors = "embl_authors"
    embl_collaborations = "embl_collaborations"
    embl_collaborations_quality = "embl_collaborations_quality"
    embl_collaborations_quality_note = "embl_collaborations_quality_note"
    external_collaboration_bool = "external_collaboration_bool"
    first_name = "first_name"
    eissn = "eissn"
    is_corresponding = "is_corresponding"
    issn = "issn"
    journal_acronym = "journal_acronym"
    journal_name = "journal_name"
    last_name = "last_name"
    licence = "licence"
    number_of_authors = "number_of_authors"
    number_of_embl_authors = "number_of_embl_authors"
    oa_route = "oa_route"
    open_alex_id = "open_alex_id"
    orcid = "orcid"
    orcids = "orcids"
    pmc_id = "pmc_id"
    preprint_url = "preprint_url"
    provenance = "provenance"
    pubmed_id = "pubmed_id"
    publication_type = "publication_type"
    publication_type_in_source = "publication_type_in_source"
    publisher = "publisher"
    submission_date = "submission_date"
    title = "title"
    wos_id = "wos_id"
    wos_researcher_ids = "wos_researcher_ids"
    year = "year"
    # author attributes
    authorship_position = "authorship_position"
    embl_author = "embl_author"
    first_name = "first_name"
    source_embl_author = "source_embl_author"
    confidence_embl_author = "confidence_embl_author"
    institutions = "institutions"
    last_name = "last_name"
    name = "name"
    orcid = "orcid"
    orcid_first_name = "orcid_first_name"
    orcid_last_name = "orcid_last_name"
    # institution attributes
    is_embl_site = "is_embl_site"
    source_embl_site = "source_embl_site"
    confidence_embl_site = "confidence_embl_site"
    ror_id = "ror_id"
    country_code = "country_code"


@dataclass
class FieldNames:
    """
    Translates different names in data sources to
    standard attribute names

    Ensure every attribute in this class is also a
    value of an attribute of StandardNames above
    """

    # publication attributes
    abstract: Optional[str] = None
    acceptance_date: Optional[str] = None
    addresses: Optional[str] = None  # WoS field
    authors: Optional[str] = None
    book_info: Optional[str] = None
    conference_info: Optional[str] = None
    converis_id: Optional[str] = None
    correspondence: Optional[str] = None
    doi: Optional[str] = None
    embl_pub: Optional[str] = None
    embl_authors: Optional[str] = None
    external_collaboration_bool: Optional[str] = None
    eissn: Optional[str] = None
    has_member_state_collaboration: Optional[str] = None
    has_non_member_state_collaboration: Optional[str] = None
    issn: Optional[str] = None
    issue: Optional[str] = None
    epmc_link: Optional[str] = None
    journal_acronym: Optional[str] = None
    journal_name: Optional[str] = None
    journal_oa_status: Optional[str] = None
    keywords: Optional[str] = None
    licence: Optional[str] = None
    number_of_authors: Optional[str] = None
    number_of_embl_authors: Optional[str] = None
    oa_route: Optional[str] = None
    open_alex_id: Optional[str] = None
    orcids: Optional[str] = None
    pmc_id: Optional[str] = None
    preprint_url: Optional[str] = None
    pubmed_id: Optional[str] = None
    publication_date: Optional[str] = None
    publication_type: Optional[str] = None
    publication_type_in_source: Optional[str] = None
    publisher: Optional[str] = None
    start_page: Optional[str] = None
    submission_date: Optional[str] = None
    title: Optional[str] = None
    volume: Optional[str] = None
    wos_id: Optional[str] = None
    wos_researcher_ids: Optional[str] = None
    year: Optional[str] = None
    # author attributes
    affiliation: Optional[str] = None
    authorship_position: Optional[str] = None
    current_address: Optional[str] = None
    embl_author: Optional[str] = None
    first_name: Optional[str] = None
    institutions: Optional[str] = None
    last_name: Optional[str] = None
    is_corresponding: Optional[str] = None
    name: Optional[str] = None
    orcid: Optional[str] = None
    orcid_first_name: Optional[str] = None
    orcid_last_name: Optional[str] = None
    embl_site: Optional[str] = None
    embl_job: Optional[str] = None
    # institution attributes
    ror_id: Optional[str] = None
    country_code: Optional[str] = None

    @property
    def names_map(self):
        return {v: k for k, v in self.__dict__.items() if v is not None}
