import numpy as np
import re
from difflib import SequenceMatcher
from typing import Optional, Pattern

from osim_utils.logger import get_logger


class ReValidator:
    """
    Regular expressions validator
    """

    def __init__(self):
        """
        DOI pattern for doi_re adapted from:
        https://www.crossref.org/blog/dois-and-matching-regular-expressions/
        If in practice we find out that this doesn't work well enough
        for our requirements, possible options to explore are to
        use a more catchy re from that blog post, or validate
        by making calls to https://doi.org/api/handles/{doi}, which
        is the approach used by this package for example:
        https://github.com/papis/python-doi/blob/master/src/doi/__init__.py
        """
        self.pmc_id_re = re.compile(r"PMC\d{5,10}")
        self.pubmed_id_re = re.compile(r"\d{5,10}")
        self.doi_re = re.compile(r"^10.\d{4,9}/[-._;()/:A-Z0-9]+$", re.IGNORECASE)
        self.logger = get_logger()

    def _base_re_validator(
        self, id_value: str, source: dict, id_name: str, re_exp: Pattern[str]
    ) -> bool:
        """
        Abstract method generalising all concrete implementations
        Args:
            id_value: value to be validated
            source: source of value to be validated; used for debugging only
            id_name: name of id_value type (e.g. if id_value is a doi value, then id_name is "DOI");
                used for debugging only
            re_exp: compiled regular expression used for matching
        """
        valid_id = re_exp.match(str(id_value))
        if valid_id:
            return True
        else:
            if (id_value is not np.nan) and (id_value != "NAN"):
                self.logger.info(
                    f"Invalid {id_name} {id_value} detected for publication",
                    extra={"publication": source},
                )
            return False

    def pmc_id_valid(self, pmc_id: str, source: dict) -> bool:
        """
        Returns True if pmc_id is a valid PubMed Central ID; False if not

        Args:
            pmc_id: PubMed Central ID to be validated
            source: source of pmc_id; used for debugging only
        """
        return self._base_re_validator(
            str(pmc_id).upper(), source, "PMC id", self.pmc_id_re
        )

    def pubmed_id_valid(self, pm_id, source):
        """
        Returns True if pm_id is a valid PubMed ID; False if not
        Args:
            pm_id: PubMed ID to be validated
            source: source of pm_id; used for debugging only
        """
        return self._base_re_validator(pm_id, source, "PubMed id", self.pubmed_id_re)

    def doi_valid(self, doi, source):
        """
        Returns True if doi is a valid DOI; False if not
        Args:
            doi: DOI to be validated
            source: source of doi; used for debugging only
        """
        return self._base_re_validator(doi, source, "DOI", self.doi_re)


def are_strings_similar(str1, str2, threshold=0.75):
    """
    Returns True if the ratio of similarity between strings str1
    and str2 is greater than or equal to threshold; False
    if the similarity ratio is lower than threshold

    Args:
        str1: string to be compared to str2
        str2: string to be compared to str1
        threshold: minimum similarity ratio between str1 and str2
            for validation to pass
    """
    similarity = SequenceMatcher(None, str1, str2).ratio()
    if similarity >= threshold:
        return True
    else:
        return False


class LicenceValidator:
    """
    Standardises licence names
    """

    cc_by = "CC BY"
    cc_by_nc_nd = "CC BY-NC-ND"

    @classmethod
    def get_licence_name(cls, lic_str: Optional[str]) -> Optional[str]:
        """
        Translates licence strings to standardised licence
        names

        Args:
            lic_str: licence string (or None)

        Returns: Standard licence string or None
        """
        try:
            match lic_str.lower():
                case "cc-by-nc-nd":
                    return cls.cc_by_nc_nd
                case "cc-by":
                    return cls.cc_by
        except AttributeError:
            return None
