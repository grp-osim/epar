import pandas as pd
import re
from collections import namedtuple
from osim_utils.logger import get_logger

from utils.data_container import DataContainer
from utils.exceptions import DataValidationError


ParsedContainer = namedtuple("ParsedContainer", ["df", "merge_column", "null_df"])


class DataMerger:
    """
    Matches and merges publication data from different sources
    """

    def __init__(
        self, data_containers: list[DataContainer], on: str, strict: bool = True
    ):
        """
        Args:
            data_containers: list of classes containing publication data
                (e.g. ConversisExportParser, WosExportParser, etc)
            on: column to merge data_containers on (use standard name [e.g. doi instead of doi_wos])
            strict: if True, raise an error if column to merge on contains null values
        """
        self.data_containers = data_containers
        self.on = on
        self.strict = strict
        self.df = None
        self.logger = get_logger()

    def _parse_container(self, container: DataContainer):
        df = container.label_df()
        merge_col = f"{self.on}{container.data_label}"
        null_df = df[pd.isnull(df[merge_col])]
        err = "Missing value(s) present in column {}"

        if not null_df.empty:
            if self.strict is True:
                raise DataValidationError(err.format(merge_col))
            else:
                self.logger.warning(err.format(merge_col))

        return ParsedContainer(
            df=df,
            merge_column=merge_col,
            null_df=null_df,
        )

    def merge_data(self, **kwargs) -> None:
        """
        Args:
            **kwargs: Arguments to be passed to pd.merge
        """
        how = kwargs.pop("how", "outer")
        indicator = kwargs.pop("indicator", "_merge_0")

        self.df, l_on, l_on_missing = self._parse_container(self.data_containers.pop())
        self.df[self.on] = self.df[l_on]
        merge_counter = 0
        while self.data_containers:
            indicator = re.sub(r"_\d+", f"_{merge_counter}", indicator)
            r_df, r_on, r_on_missing = self._parse_container(self.data_containers.pop())
            self.df = self.df.merge(
                r_df[pd.notnull(r_df[r_on])],
                left_on=self.on,
                right_on=r_on,
                how=how,
                indicator=indicator,
                **kwargs,
            )
            merge_counter += 1

            r_on_missing[indicator] = "right_only"
            self.df = pd.concat(
                [
                    self.df,
                    r_on_missing,
                ]
            )

            self.df[self.on] = self.df[self.on].combine_first(self.df[r_on])
