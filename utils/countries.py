import pycountry
import re
from collections import namedtuple
from country_list import countries_for_language, available_languages
from typing import Optional, Union

SpecialCase = namedtuple(
    "SpecialCase", ["detection_re", "lookup_name", "country_alpha2"]
)


class CountryDetector:
    invalid_country_codes = [
        "XA",  # 'Pseudo-Accents' see https://github.com/umpirsky/country-list/issues/112
    ]
    extra_name2alpha2 = {
        "Czech Republic": "CZ",
        "England": "GB",
        "Northern Ireland": "GB",
        "Scotland": "GB",
    }
    special_cases = [SpecialCase(r"(?<!New South )\bWales\b", "Wales", "GB")]
    case_sensitive_map = {
        "UK": "GB",
        "US": "US",
        "USA": "US",
    }

    def __init__(self, **kwargs):
        """
        Args:
            **kwargs:
                languages (Union[list, str]): list of country_list languages to include in
                    detection dict/re (print country_list.available_languages for a comprehensive list).
                    Defaults to 'en' if not passed. The string "all" can be passed as a shorthand for
                    using all available languages.
                use_extras (bool): if True (default), extra_name2alpha2 will be added to detector
                    attributes (dict and re)
                use_abbreviations (bool): if True (default), case_sensitive_map will be compiled
                    to a separate case-sensitive country-detection regular expression

        Notes:
            self.name2alpha2 (dict): lowercase names to alpha2 country codes. This dict is used for
                translating matches to country codes, so it should contain all strings used during detection,
                including abbreviations. Example: {"united kingdom": "GB", "usa": "US"}
            self.alpha2names (dict): alpha2 country codes to list of country names. At present, used only for
                debugging.
                    Example: {"GB": ["Great Britain", "United Kingdom"]}
        """
        self.name2alpha2 = dict()
        self.alpha2names = dict()
        self.country_pattern = str()
        self.pattern_sep = r"|"
        pattern_end = r")\b"
        pattern_start = r"\b("

        langs = kwargs.get("languages", ["en"])
        if langs == "all":
            langs = available_languages()
        self._ingest_data_from_country_list_package(languages=langs)

        extras = kwargs.get("use_extras", True)
        if extras:
            for name, code in self.extra_name2alpha2.items():
                self._parse_country_data(country_name=name, alpha2code=code)
            for sc in self.special_cases:
                self._parse_country_data(
                    country_name=sc.lookup_name,
                    alpha2code=sc.country_alpha2,
                    detection_pattern=sc.detection_re,
                )

        self.use_abbreviations = kwargs.get("use_abbreviations", True)
        if self.use_abbreviations:
            for name, code in self.case_sensitive_map.items():
                self._parse_country_data(
                    country_name=name, alpha2code=code, skip_pattern=True
                )
            self.abbrev_pattern = (
                pattern_start
                + self.pattern_sep.join(self.case_sensitive_map.keys())
                + pattern_end
            )
            self.abbrev_re = re.compile(self.abbrev_pattern)

        self.country_pattern = (
            pattern_start + self.country_pattern.rstrip(self.pattern_sep) + pattern_end
        )
        self.country_re = re.compile(self.country_pattern, re.IGNORECASE)

    def _ingest_data_from_country_list_package(self, languages: list[str]):
        for language in languages:
            for country in countries_for_language(language):
                self._parse_country_data(country_name=country[1], alpha2code=country[0])

    def _parse_country_data(
        self,
        country_name: str,
        alpha2code: str,
        detection_pattern: Optional[str] = None,
        skip_pattern: bool = False,
    ) -> None:
        self.name2alpha2[country_name.lower()] = alpha2code
        try:
            self.alpha2names[alpha2code].append(country_name)
        except KeyError:
            self.alpha2names[alpha2code] = [country_name]
        if not skip_pattern:
            if detection_pattern:
                self.country_pattern += f"{detection_pattern}{self.pattern_sep}"
            else:
                self.country_pattern += (
                    country_name.replace("(", "\(").replace(")", "\)")
                    + self.pattern_sep
                )

    def detect_countries(self, text: str, **kwargs) -> set[str]:
        """
        Args:
            text: input text containing country names to be detected
            **kwargs:
                expected_match_n (int): number of country name matches expected in text.
                    If passed, the actual number of matches found will be checked against
                    this expected number and an exception raised if the numbers aren't equal
                expected_results_n (int): number of unique country name matches expected in the
                    set returned by this method
                return_code (bool): if True, return detected country code; if False, return name. Default:
                    False

        Returns: Set of unique pycountry country names detected in input text
        """
        return_code = kwargs.get("return_code", False)
        country_codes = [
            self.name2alpha2[x.lower()] for x in self.country_re.findall(text)
        ]
        if self.use_abbreviations:
            country_codes += [
                self.name2alpha2[x.lower()] for x in self.abbrev_re.findall(text)
            ]
        match_n = kwargs.get("expected_match_n")
        if match_n:
            assert (cc_n := len(country_codes)) == match_n, (
                f"Expected ({match_n}) and actual ({cc_n}) number of country names do not match. \n"
                f"Actual matches: {country_codes}\n"
                f"Text: {text}"
            )
        if return_code is True:
            unique_matches = set(country_codes)
        else:
            unique_matches = {
                pycountry.countries.get(alpha_2=x).name for x in country_codes
            }
        u_match_n = kwargs.get("expected_results_n")
        if u_match_n:
            assert (cc_n := len(unique_matches)) == u_match_n, (
                f"Expected ({u_match_n}) and actual ({cc_n}) number of unique country names do not match. \n"
                f"Actual unique matches: {unique_matches}\n"
                f"Text: {text}"
            )
        return unique_matches
