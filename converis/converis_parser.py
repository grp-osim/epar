import numpy as np
import pandas as pd
from typing import Optional, Union

import local.config as conf
from converis.fields import CONVERIS_FIELDNAMES
from osim_utils.clients.epmc import EpmcClient
from export_parser.export_parser import ExportParser
from utils.exceptions import DuplicateValueError, ImprovementNeededError
from utils.fieldset import StandardNames as sn
from utils.validators import LicenceValidator


class ConverisExportParser(ExportParser):
    def __init__(self, input_file_path: str = None, **kwargs):
        """
        Initialises the parser by reading an Excel export from Converis.
        If input_file_path is not passed as an argument,
        an input path must be specified in a
        variable (converis_xlsx) in config.py

        Args:
            input_file_path: path to the Converis export file. If not passed,
                this is loaded from config.py
            kwargs: keyword arguments for ExportParcer.load_datagframe method
        """
        if input_file_path is None:
            input_file_path = conf.converis_xlsx
        dtype_dict = {
            CONVERIS_FIELDNAMES.converis_id: str,
            CONVERIS_FIELDNAMES.pubmed_id: str,
            CONVERIS_FIELDNAMES.year: str,
        }
        super().__init__(
            input_file_path,
            CONVERIS_FIELDNAMES,
            "_converis",
            dtype_dict=dtype_dict,
            **kwargs,
        )
        self.epmc_client = EpmcClient()

    def check_for_duplicates(self) -> None:
        """
        Checks dataframe for duplicate doi, pmc_id and pubmed_id values.
        If any duplicates are found, these are logged and an exception
        is raised
        """
        doi_duplicates = self.df.loc[self.df[sn.doi].duplicated(keep=False)]
        pmc_id_dups = self.df.loc[self.df[sn.pmc_id].duplicated(keep=False)]
        pubmed_id_dups = self.df.loc[self.df[sn.pubmed_id].duplicated(keep=False)]
        # exclude null-value (nan) duplicates
        doi_duplicates = doi_duplicates[doi_duplicates[sn.doi].notnull()]
        pmc_id_dups = pmc_id_dups[pmc_id_dups[sn.pmc_id].notnull()]
        pubmed_id_dups = pubmed_id_dups[pubmed_id_dups[sn.pubmed_id].notnull()]
        error = False
        for var_name, df in [
            ("DOI", doi_duplicates),
            ("PMC ID", pmc_id_dups),
            ("PubMed ID", pubmed_id_dups),
        ]:
            if not df.empty:
                self.logger.error(f"Duplicate {var_name} found in dataset:\n{df}")
                error = True
        if error:
            raise DuplicateValueError(
                "Dataset contains duplicated values for one or more identifiers. "
                "See logged errors above for details"
            )

        if not doi_duplicates.empty:
            raise DuplicateValueError(
                f"Duplicate DOIs found in dataset:\n{doi_duplicates}"
            )

    def prune_dataframe(self) -> None:
        """
        Removes preprints, corrections and editorial materials from the dataframe
        """
        self.df.drop(
            self.df.loc[
                (
                    self.df[sn.journal_acronym]
                    .str.lower()
                    .isin(
                        [
                            "biorxiv",
                            "biorxivorg",
                            "osf preprints",
                        ]
                    )
                    | self.df[sn.publication_type]
                    .str.lower()
                    .isin(
                        [
                            "pre-print article",
                            "correction",
                            "editorial material",
                        ]
                    )
                    | self.df[sn.publication_type_in_source]
                    .str.lower()
                    .isin(
                        [
                            "editorial",
                        ]
                    )
                )
            ].index,
            inplace=True,
        )

    @staticmethod
    def yes_no_str_to_bool(value: Union[str, np.float64]) -> Optional[bool]:
        """
        In Converis exports, boolean values are represented YES and NO strings.
        This method converts those strings into bool.

        Args:
            value: a dataframe cell value (string or np.nan)

        Returns: True if value is "yes", False if value is "no" and None
            if value is np.nan (missing data)
        """
        try:
            if value.strip().lower() == "yes":
                return True
            elif value.strip().lower() == "no":
                return False
        except AttributeError:  # value is np.nan
            return None

    def parse_authors(self, row: pd.Series) -> pd.DataFrame:
        """
        Translates authorship data from each dataframe row
        into a dataframe of authors

        Args:
            row: a row of the dataframe (i.e. a publication)
        """
        parsed_authors = list()
        try:
            authors = [a.strip() for a in row[sn.authors].split(",")]
        except AttributeError:  # row[self.fn.authors] is nan
            authors = list()

        authors_n = row.get(sn.number_of_authors, np.nan)
        if not np.isnan(authors_n):
            assert len(authors) == int(authors_n)

        try:
            embl_authors = [a.strip().lower() for a in row[sn.embl_authors].split(",")]
        except AttributeError:  # row[self.fn.embl_authors] is nan
            embl_authors = list()

        embl_authors_n = row.get(sn.number_of_embl_authors, np.nan)
        if not np.isnan(embl_authors_n):
            try:
                assert len(embl_authors) == int(embl_authors_n)
            except AssertionError:
                self.logger.warning(
                    f"Number of listed EMBL author names ({len(embl_authors)}) "
                    f"does not match number_of_embl_authors field ({int(embl_authors_n)})"
                )

        if embl_authors and not authors:
            raise ImprovementNeededError(
                f"Converis export row has info in embl_authors field but not in authors",
                extra=row.to_dict(),
            )
        for a in authors:
            if a.lower() in embl_authors:
                parsed_authors.append({sn.name: a, sn.embl_author: True})
            else:
                parsed_authors.append({sn.name: a, sn.embl_author: np.nan})
        # if (aff := row[self.fn.affiliation]) is not np.nan:
        #     # add paper affiliation under a nameless author
        #     parsed_authors.append(Author(affiliations=[Affiliation(raw=aff)]))
        return pd.DataFrame(parsed_authors)

    def parse_df_data(self):
        if sn.authors in self.df.columns:
            self.df.authors = self.df.apply(self.parse_authors, axis=1)
        if sn.doi in self.df.columns:
            self.df.doi = self.df.doi.apply(self.check_doi)
        if sn.external_collaboration_bool in self.df.columns:
            self.df.external_collaboration_bool = (
                self.df.external_collaboration_bool.apply(self.yes_no_str_to_bool)
            )
        if sn.issn in self.df.columns:
            self.df.issn = self.df.issn.apply(lambda x: {x})
        if sn.licence in self.df.columns:
            self.df.licence = self.df.licence.apply(LicenceValidator.get_licence_name)
        if sn.pmc_id in self.df.columns:
            self.df.pmc_id = self.df.pmc_id.apply(self.check_pmc_id)
        if sn.pubmed_id in self.df.columns:
            self.df.pubmed_id = self.df.pubmed_id.apply(self.check_pubmed_id)
