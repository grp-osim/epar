from utils.fieldset import FieldNames


CONVERIS_FIELDNAMES = FieldNames(
    abstract="Publication: Abstract",
    affiliation="Publication: Affiliation in source",
    authors="Publication: Authors’ names",
    converis_id="Publication: Id ",
    doi="Publication: DOI",
    embl_pub="Publication: EMBL Publication",
    embl_authors="Publication: EMBL authors",
    external_collaboration_bool="Publication: External collaboration",
    issn="Publication: ISSN",
    journal_acronym="Publication: Journal acronym",
    journal_name="Publication: Journal name in source",
    licence="Publication: License",
    number_of_authors="Publication: Number of authors",
    number_of_embl_authors="Publication: Number of EMBL authors",
    oa_route="Publication: Open Access Route",
    orcids="Publication: Related ORCIDs",
    pmc_id="Publication: PMC ID",
    pubmed_id="Publication: PubMed ID",
    publication_type="Publication: Publication type",
    publication_type_in_source="Publication: Publication type in source",
    title="Publication: Title",
    year="Publication: Year in source",
)
# Publication: Investigators' names: contains EMBL researchers in a consortium authorship
