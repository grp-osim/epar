import numpy as np
import pandas as pd
import re
from dataclasses import dataclass
from dateutil import parser
from osim_utils.logger import get_logger
from typing import Optional, Union

import local.config as conf
from export_parser.export_parser import ExportParser
from utils.exceptions import DuplicateValueError, ImprovementNeededError
from utils.fieldset import StandardNames as sn
from utils.validators import LicenceValidator
from reports.entities import Author, Publication
from utils.countries import CountryDetector
from wos.wos_fields import WOS_FIELDNAMES


@dataclass
class Author:
    """
    Author of a Publication in WoS dataset
    """

    name: str
    is_embl_author: bool


class WosAddressParser:
    def __init__(self, address: str):
        """

        Args:
            address: WoS address string in the format:
                [author1; author2...] institute1; [author1; author3...] institute2
        """
        self.address = address
        self.authorships = self.address.split("; [")
        self.logger = get_logger()

        embl_pattern = (
            r"\b("
            + r"|".join(
                [
                    "European Mol Biol Lab",
                    "EMBL",
                    "European Bioinformat Inst",
                    "HGNC",
                    "Ctr Struct Syst Biol",
                    "European Microbiol",
                ]
            )
            + r")\b"
        )
        self.embl_re = re.compile(embl_pattern, re.IGNORECASE)

        self.institute2authors = dict()
        self.author_dict = self.get_authors()

    def is_embl(self, wos_authorship: str) -> bool:
        m = self.embl_re.search(wos_authorship)
        if m:
            return True
        return False

    def get_authors(self) -> dict[str, Author]:
        """
        Parses authorship info in WoS address string

        Returns:
            Dictionary of Author instances keyed on author name
        """
        author_dict = dict()
        for author_inst in self.authorships:
            try:
                authors_str, inst_str = author_inst.split("]")
            except ValueError:
                self.logger.warning(
                    f"Could not split WoS address string '{author_inst}' into authors "
                    f"and institute substrings; skipping it"
                )
                continue
            inst_is_embl = self.is_embl(inst_str)
            authors = [x.lstrip("[").strip() for x in authors_str.split(";")]
            for name in authors:
                try:
                    self.institute2authors[inst_str].append(name)
                except KeyError:
                    self.institute2authors[inst_str] = [name]
                try:
                    author = author_dict[name]
                except KeyError:
                    author_dict[name] = Author(name=name, is_embl_author=inst_is_embl)
                else:
                    if inst_is_embl is True:
                        author.is_embl_author = True
        return author_dict

    def get_collaborations(self, country_detector: CountryDetector) -> tuple:
        collaborations_quality = "high"
        collaborations_quality_note = np.nan
        collaborations = dict()
        if not self.institute2authors:
            collaborations_quality = "low"
            collaborations_quality_note = (
                "Affiliation data missing in source data from Web of Science"
            )
        for inst_str, authors in self.institute2authors.items():
            non_embl_authors = [
                x for x in authors if not self.author_dict[x].is_embl_author
            ]
            inst_is_embl = self.is_embl(inst_str)
            if inst_is_embl or not non_embl_authors:
                continue  # ignore EMBL authorships and co-affiliations

            affiliations = inst_str.split(";")
            for a in affiliations:
                name = a.split(",")[0].strip()
                country_substr = a.split(",")[-1]
                country_code = country_detector.detect_countries(
                    country_substr, expected_results_n=1, return_code=True
                ).pop()

                try:
                    collaborations[country_code].add(name)
                except KeyError:
                    collaborations[country_code] = {name}
        # sort collaboration sets to facilitate testing
        collaborations = {k: sorted(v) for k, v in collaborations.items()}
        return (
            collaborations,
            collaborations_quality,
            collaborations_quality_note,
        )

    def get_embl_authors(self):
        authors = self.get_authors()
        return "; ".join(
            [v.name for k, v in authors.items() if v.is_embl_author is True]
        )


class WosExportParser(ExportParser):
    def __init__(self, input_file_path: str = None, **kwargs):
        """
        Initialises the parser by reading an Excel export from WoS.
        If input_file_path is not passed as an argument,
        an input path must be specified in a
        variable (wos_xlsx) in config.py

        Args:
            input_file_path: path to the Converis export file. If not passed,
                this is loaded from config.py
        """
        if input_file_path is None:
            input_file_path = conf.wos_xlsx
        super().__init__(input_file_path, WOS_FIELDNAMES, "_wos", **kwargs)
        self.embl_re = None  # set by is_embl method
        self.country_detector = kwargs.get("country_detector")
        if self.country_detector is None:
            self.country_detector = CountryDetector()

    def check_for_duplicates(self) -> None:
        """
        Checks dataframe for duplicate doi, pmc_id and pubmed_id values.
        If any duplicates are found, these are logged and an exception
        is raised
        """
        doi_duplicates = self.df.loc[self.df[sn.doi].duplicated(keep=False)]
        pubmed_id_dups = self.df.loc[self.df[sn.pubmed_id].duplicated(keep=False)]
        # exclude null-value (nan) duplicates
        doi_duplicates = doi_duplicates[doi_duplicates[sn.doi].notnull()]
        pubmed_id_dups = pubmed_id_dups[pubmed_id_dups[sn.pubmed_id].notnull()]
        error = False
        for var_name, df in [
            ("DOI", doi_duplicates),
            ("PubMed ID", pubmed_id_dups),
        ]:
            if not df.empty:
                self.logger.error(
                    f"Duplicate {var_name} found in dataset: {df[[sn.doi, sn.pubmed_id]].to_dict()}"
                )
                error = True
        if error:
            raise DuplicateValueError(
                "Dataset contains duplicated values for one or more identifiers. "
                "See logged errors above for details"
            )

        if not doi_duplicates.empty:
            raise DuplicateValueError(
                f"Duplicate DOIs found in dataset:\n{doi_duplicates}"
            )

    def check_doi(self, doi: pd.Series) -> Optional[str]:
        """
        WoS export sometimes lists several DOIs in the DOI column
        (example: 10.7554/eLife.68048; 10.7554/eLife.68048.sa0;
        10.7554/eLife.68048.sa1; 10.7554/eLife.68048.sa2)

        In cases like this, this method uses the first DOI value
        and ignores the rest.
        """
        try:
            doi = doi.split(";")[0]
        except AttributeError:  # doi is np.nan
            return np.nan
        if self.validator.doi_valid(doi, doi):
            return doi.lower()

    def prune_dataframe(self) -> None:
        """
        Removes conference abstracts, preprints, corrections and editorial materials
        from the dataframe
        """
        self.df.drop(
            self.df.loc[
                (
                    self.df[sn.publication_type]
                    .str.lower()
                    .isin(
                        [
                            "pre-print article",
                            "correction",
                            "editorial material",
                            "meeting abstract",
                            "biographical-item",
                            "editorial material; early access",
                        ]
                    )
                )
            ].index,
            inplace=True,
        )

    def df_to_publications_list(self) -> list[Publication]:
        """
        Converts the parser's dataframe into a list of
        Publication instances.
        """
        return [
            Publication(
                abstract=row.get(self.fn.abstract),
                # authors=self.parse_authors(row),
                doi=self.check_doi(row),
                issn={row[self.fn.issn], row[self.fn.eissn]},
                journal=row.get(self.fn.journal_name),
                pubmed_id=self.check_pubmed_id(row),
                title=row[self.fn.title],
                year=row.get(self.fn.year),
            )
            for _, row in self.df.iterrows()
        ]

    def get_publication_date(self, row):
        if (row.publication_date is not np.nan) and (row.year is not np.nan):
            return parser.parse(f"{row.publication_date} {int(row.year)}").date()

    @staticmethod
    def get_embl_authors(address):
        """
        This method could be merged to parse_affiliations
        """
        if address is np.nan:
            return np.nan
        else:
            address_parser = WosAddressParser(address)
            return address_parser.get_embl_authors()

    def parse_affiliations(self, row: pd.Series) -> tuple:
        address = row[sn.addresses]
        if address is np.nan:
            return np.nan, "low", "Addresses missing in source data from Web of Science"
        else:
            address_parser = WosAddressParser(address)
            return address_parser.get_collaborations(self.country_detector)

    @staticmethod
    def get_embl_pub_and_confidence(row: pd.Series) -> tuple:
        address = row[sn.addresses]
        if address is np.nan:
            return np.nan, np.nan
        else:
            address_parser = WosAddressParser(address)
            embl_authors = [
                v.name
                for k, v in address_parser.author_dict.items()
                if v.is_embl_author is True
            ]
            if embl_authors:
                return True, len(embl_authors)
            else:
                return False, 5

    def parse_df_data(self):
        if sn.doi in self.df.columns:
            self.df.doi = self.df.doi.apply(self.check_doi)
        if sn.pubmed_id in self.df.columns:
            self.df.pubmed_id = self.df.pubmed_id.apply(self.check_pubmed_id)
        self.df[
            [
                sn.embl_collaborations,
                sn.embl_collaborations_quality,
                sn.embl_collaborations_quality_note,
            ]
        ] = self.df.apply(self.parse_affiliations, axis=1, result_type="expand")
        self.df[[sn.embl_pub, sn.embl_pub_confidence]] = self.df.apply(
            self.get_embl_pub_and_confidence, axis=1, result_type="expand"
        )

        # self.df[sn.embl_collaborations] = self.df.addresses.apply(
        #     self.parse_affiliations
        # )
        # if sn.publication_date in self.df.columns:
        #     self.df.publication_date = self.df.apply(self.get_publication_date, axis=1)
