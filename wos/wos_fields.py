from utils.fieldset import FieldNames


WOS_FIELDNAMES = FieldNames(
    abstract="Abstract",
    addresses="Addresses",
    affiliation="Affiliations",
    authors="Author Full Names",
    correspondence="Reprint Addresses",
    doi="DOI",
    eissn="eISSN",
    issn="ISSN",
    journal_acronym="Journal ISO Abbreviation",
    journal_name="Source Title",
    orcids="ORCIDs",
    pubmed_id="Pubmed Id",
    publication_date="Publication Date",
    publication_type="Document Type",
    publisher="Publisher",
    title="Article Title",
    wos_id="UT (Unique WOS ID)",
    wos_researcher_ids="Researcher Ids",
    year="Publication Year",
)
