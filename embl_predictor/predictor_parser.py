import pandas as pd
from osim_utils.clients.epmc import EpmcClient

import local.config as conf
from export_parser.export_parser import ExportParser
from utils.fieldset import StandardNames as sn
from embl_predictor.predictor_fields import PREDICTOR_FIELDNAMES


class EmblPredictorExportParser(ExportParser):
    def __init__(self, input_file_path: str = None, **kwargs):
        """
        Initialises the parser by reading an Excel export from
        ML-EMBL-publication-project
        (https://gitlab.ebi.ac.uk/literature-services/public-projects/ML-EMBL-publication-project).
        If input_file_path is not passed as an argument,
        an input path must be specified in a
        variable (embl_predictor_xlsx) in config.py

        Args:
            input_file_path: path to the export file. If not passed,
                this is loaded from config.py
        """
        if input_file_path is None:
            input_file_path = conf.embl_predictor_xlsx
        super().__init__(input_file_path, PREDICTOR_FIELDNAMES, **kwargs)
        self.epmc_client = EpmcClient()

    def load_dataframe(self, **kwargs) -> pd.DataFrame:
        """
        Reads an export file. Creates and returns a Pandas dataframe.

        Args:
            kwargs:
                other kwargs: keyword arguments for pandas.read_csv method
                usecols: columns to read from input file. Defaults to CONVERIS_FIELDNAMES


        Returns: Pandas dataframe
        """
        usecols = kwargs.pop("usecols", list())
        default_usecols = True
        if usecols:
            default_usecols = False
        names_map = {}

        for k, v in PREDICTOR_FIELDNAMES.__dict__.items():
            if v:
                if default_usecols is True:
                    usecols.append(v)
                names_map[v] = k

        d_kwargs = {
            # "dtype": str,
            # "keep_default_na": False,
            "usecols": usecols,
            "dtype": {
                PREDICTOR_FIELDNAMES.pubmed_id: str,
            },
        }
        try:
            d_kwargs.update(kwargs)
            self.df = pd.read_csv(self.input_file_path, **d_kwargs).rename(
                columns=names_map
            )
        except UnicodeDecodeError:
            self.df = pd.read_excel(self.input_file_path, **d_kwargs).rename(
                columns=names_map
            )
        return self.df

    def get_europepmc_data(self) -> None:
        """
        Makes a single API call to the EuropePMC API to get data about all
        publications in AnnualReport instance.

        Limitations:
            This method will fail if report has more than 999 publications
        """
        pubmed_id_list = self.df.pubmed_id.loc[self.df.pubmed_id.notna()]
        query = f"ext_id:({' OR '.join(pubmed_id_list)})"
        query_len = len(pubmed_id_list)
        query_limit = 1400
        assert query_limit > query_len, (
            f"This method does not support pagination yet and "
            f"the number of IDs queried ({query_len}) exceeds"
            f"the limit of logical operators in an EPMC API query ({query_limit})."
            f"Also bear in mind that the EPMC API has a pageSize limit of 1000"
            f"results. If you are querying for more than 1000 publications, "
            f"you will need to split that in more than one call to this method"
        )
        r = self.epmc_client.search_post(query=query, pageSize=1000)
        self.logger.info(
            f"EuropePMC API response contains data for {r['hitCount']} publications of the {query_len} queried"
        )
        return r["resultList"]["result"]

    def get_doi(self):
        epmc_data = self.get_europepmc_data()
        epmc_df = pd.DataFrame(epmc_data)
        self.df = self.df.merge(
            epmc_df[["pmid", "doi"]], left_on=sn.pubmed_id, right_on="pmid"
        )

    def parse_df_data(self):
        if sn.pubmed_id in self.df.columns:
            self.df.pubmed_id = self.df.pubmed_id.apply(self.check_pubmed_id)
        self.get_doi()
