from pycountry import countries

# region standard data field names
AUTHORS = "authors"
DOI = "doi"
# endregion

member_state_names = [
    "Australia",
    "Austria",
    "Belgium",
    "Croatia",
    "Czechia",
    "Denmark",
    "Estonia",
    "Finland",
    "France",
    "Germany",
    "Greece",
    "Hungary",
    "Iceland",
    "Ireland",
    "Israel",
    "Italy",
    "Latvia",
    "Lithuania",
    "Luxembourg",
    "Malta",
    "Montenegro",
    "Netherlands",
    "Norway",
    "Poland",
    "Portugal",
    "Slovakia",
    "Spain",
    "Sweden",
    "Switzerland",
    "United Kingdom",
]

member_states = [countries.get(name=x) for x in member_state_names]
member_state_codes = [x.alpha_2 for x in member_states]


class Var:
    """
    Standard variable names to avoid hardcoding
    """

    pmc_ids = "pmc_ids"
    pubmed_ids = "pubmed_ids"
    dois = "dois"
