from __future__ import annotations
from dataclasses import dataclass, field
from datetime import date
from typing import Any, Optional, Type

from openalex.openalex_parser import OpenAlexParser


@dataclass
class Affiliation:
    """
    Represents an Author's affiliation to an institution
    """

    raw: Optional[str] = None
    city: Optional[str] = None
    country_code: Optional[str] = None
    country_name: Optional[str] = None
    embl_site: Optional[str] = None
    is_embl: bool = False
    openalex_id: Optional[str] = None
    ror_id: Optional[str] = None


@dataclass
class Author:
    """
    Represents an author of a Publication
    """

    name: Optional[str] = None
    alternative_names: list[str] = field(default_factory=list)
    affiliations: list[Affiliation] = field(default_factory=list)
    openalex_id: Optional[str] = None
    orcid: Optional[str] = None
    present_address: Optional[Type[Affiliation]] = None
    embl_author: Optional[bool] = None
    position_int: Optional[int] = None
    position_str: Optional[str] = None

    @classmethod
    def from_epmc_data(cls, epmc_data: dict) -> Author:
        """
        Initialises an Author instance from an authorship dictionary
        returned by the Europe PMC API. This method is not currently
        used in this project.

        Args:
            epmc_data: Dictionary representing an author in Europe PMC
                API results
        """
        author = cls()
        try:
            author.name = epmc_data["fullName"]
        except KeyError:
            try:
                author.name = epmc_data["lastName"]
            except KeyError:
                author.name = None

        try:
            affs = epmc_data["authorAffiliationDetailsList"]["authorAffiliation"]
        except KeyError:
            pass
        else:
            for aff in affs:
                author.affiliations.append(Affiliation(raw=aff["affiliation"]))

        return author


@dataclass
class Publication:
    """
    Represents a scholarly publication (article, book chapter, etc)
    """

    abstract: Optional[str] = None
    authors: list[Author] = field(default_factory=list)
    converis_id: Optional[str] = None
    doi: Optional[str] = None
    embl_paper: Optional[bool] = None
    embl_sites: set = field(default_factory=set)
    epmc_link: Optional[str] = None
    external_collaboration_bool: Optional[bool] = None
    in_epmc: Optional[bool] = None
    issn: set = field(default_factory=set)
    journal: Optional[str] = None
    keywords: Optional[str] = None
    licence: Optional[str] = None
    has_member_state_collaboration: Optional[bool] = None
    member_state_collaborations: dict = field(default_factory=dict)
    # month: Optional[str] = None
    open_alex_id: Optional[str] = None
    has_other_collaboration: Optional[bool] = None
    other_collaborations: dict = field(default_factory=dict)
    pmc_id: Optional[str] = None
    pubmed_id: Optional[str] = None
    publisher: Optional[str] = None
    short_reference: Optional[str] = None
    title: Optional[str] = None
    year: Optional[str] = None
    date: Optional[date] = None
    validation_error: Optional[str] = None

    @property
    def embl_authors(self):
        return "; ".join([a.name for a in self.authors if a.embl_author is True])

    @property
    def month(self):
        try:
            return self.date.month
        except AttributeError:
            return None

    @property
    def embl_only(self):
        if self.embl_paper is True:
            if (self.has_member_state_collaboration is False) and (
                self.has_other_collaboration is False
            ):
                return True
            elif (self.has_member_state_collaboration is True) or (
                self.has_other_collaboration is True
            ):
                return False
            else:
                return None

    @property
    def member_state_countries(self):
        """
        Total number of member state countries in self.affiliation_data
        """
        raise NotImplementedError

    @property
    def member_state_institutes(self):
        """
        Total number of member state institutes in self.affiliation_data
        """
        raise NotImplementedError

    def colab_count(self, country):
        try:
            return len(self.member_state_collaborations[country])
        except KeyError:
            return 0

    def ingest_openalex_data(self, openalex_result: dict[str, Any]) -> None:
        """
        Uses an instance of OpenAlexParser to merge data from
        the OpenAlex API with data already stored in this Publication
        object.

        Args:
            openalex_result: OpenAlex data for this publication
        """
        alex_parser = OpenAlexParser(self, openalex_result)
        alex_parser.parse_result()
