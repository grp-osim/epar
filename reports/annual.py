from __future__ import annotations
import csv
import os.path
import pandas as pd
from dataclasses import fields
from datetime import datetime
from osim_utils.clients.embl_data_warehouse import DataWareClient
from osim_utils.clients.epmc import EpmcClient
from osim_utils.clients.openalex import OpenAlexClient
from osim_utils.logger import get_logger
from typing import Any, Optional

from converis.converis_parser import ConverisExportParser
from data_warehouse.data_warehouse_parser import DataWarehouseParser
from freya_ror_predictor.freya_ror_parser import FreyaRorPredictorExportParser
from oaswitchboard.oaswitchboard_parser import OaswitchboardExportParser
from openalex.openalex_parser import OpenAlexParser

from pycountry import countries
from reports.base import BaseReport
from reports.constants import member_state_codes
from reports.entities import Publication
from ror.client import RorClient
from utils.data_merger import DataMerger
from utils.fieldset import StandardNames
from utils.utils import BASE_DIR
from utils.validators import ReValidator
from wos.wos_parser import WosExportParser


class AnnualReport(BaseReport):
    """
    Represents the annual report
    """

    def __init__(self, report_year: Optional[int] = None, **kwargs):
        if report_year is None:
            report_year = datetime.now().year
        kwargs.update(report_year=report_year)
        super().__init__(**kwargs)
        self.converis_input_file = kwargs.get("converis_input_file", None)
        self.df = kwargs.get("publications", None)
        self.freya_input_file = kwargs.get("freya_input_file", None)
        self.oaswitchboard_input_file = kwargs.get("oaswitchboard_input_file", None)
        self.openalex_client = OpenAlexClient()
        self.openalex_data = list()
        self.epmc_client = EpmcClient()
        self.epmc_data = None
        self.logger = get_logger()
        self.ror_client = RorClient()
        self.validator = ReValidator()
        self.problem_pubs = list()
        self.warehouse_client = DataWareClient()
        self.wos_input_file = kwargs.get("wos_input_file", None)

    @classmethod
    def from_converis_export(cls, *args, **kwargs) -> AnnualReport:
        """
        Initialises a annual report instance from a Converis export file
        Args:
            *args: positional arguments to be passed to ConverisExportParser
            **kwargs: keyword arguments to be passed to ConverisExportParser

        Returns:

        """
        cep = ConverisExportParser(*args, **kwargs)
        cep.sanitise_dataframe()
        cep.prune_dataframe()
        cep.parse_df_data()
        return cls(publications=cep.df)

    def query_openalex_publications(self, **kwargs) -> OpenAlexParser:
        ignore_cache = kwargs.get("ignore_openalex_cache", False)
        openalex_cache_path = os.path.join(BASE_DIR, "cache", "openalex_cache.csv")
        if ignore_cache is True:
            r = self.openalex_client.get_all_embl_works(
                filters={
                    "from_publication_date": f"{self.report_year}-01-01",
                    "to_publication_date": f"{self.report_year}-01-05",
                }
            )
            oap = OpenAlexParser(r)
            oap.parse_df_data()
            oap.df.to_csv(openalex_cache_path)
            return oap
        else:
            try:
                return OpenAlexParser(openalex_results=pd.read_csv(openalex_cache_path))
            except FileNotFoundError:
                return self.query_openalex_publications(ignore_openalex_cache=True)

    def query_warehouse(self, **kwargs):
        ignore_cache = kwargs.get("ignore_warehouse_cache", False)
        cache_path = os.path.join(BASE_DIR, "cache", "warehouse_cache.csv")
        if ignore_cache is True:
            r = self.warehouse_client.get_all_publications()
            dwp = DataWarehouseParser(r)
            dwp.df = dwp.df[dwp.df.year == str(self.report_year)]
            dwp.df.to_csv(cache_path)
            return dwp
        else:
            try:
                return DataWarehouseParser(warehouse_results=pd.read_csv(cache_path))
            except FileNotFoundError:
                return self.query_warehouse(ignore_warehouse_cache=True)

    def get_converis_publications(self):
        cep = ConverisExportParser(input_file_path=self.converis_input_file)
        cep.sanitise_dataframe()
        cep.prune_dataframe()
        cep.parse_df_data()
        return cep

    def get_wos_publications(self):
        wep = WosExportParser(input_file_path=self.wos_input_file)
        wep.check_for_duplicates()
        wep.parse_df_data()
        return wep

    def get_freya_publications(self):
        fep = FreyaRorPredictorExportParser(input_file_path=self.freya_input_file)
        return fep

    def get_oaswitchboard_publications(self):
        if self.orcid_export_parser is None:
            self.orcid_export_parser = self.get_orcid_publications()
        oep = OaswitchboardExportParser(
            input_file_path=self.oaswitchboard_input_file,
            orcid_export_parser=self.orcid_export_parser,
        )
        oep.parse_df_data()
        return oep

    def assemble_masterlist(self, **kwargs):
        sources = kwargs.pop(
            "sources",
            [
                "Converis",
                "Freya",
                "Oaswitchboard",
                "OpenAlex",
                "ORCID",
                # "Warehouse",
                "WoS",
            ],
        )
        data_containers = list()
        if "Converis" in sources:
            data_containers.append(self.get_converis_publications())
        if "Freya" in sources:
            data_containers.append(self.get_freya_publications())
        if "Oaswitchboard" in sources:
            data_containers.append(self.get_oaswitchboard_publications())
        if "OpenAlex" in sources:
            data_containers.append(self.query_openalex_publications(**kwargs))
        if "ORCID" in sources:
            data_containers.append(self.get_orcid_publications())
        if "Warehouse" in sources:
            data_containers.append(self.query_warehouse(**kwargs))
        if "WoS" in sources:
            data_containers.append(self.get_wos_publications())
        merger = DataMerger(data_containers, on=StandardNames.doi, strict=False)
        merger.merge_data()
        return merger.df

    def get_europepmc_data(self) -> None:
        """
        Makes a single API call to the EuropePMC API to get data about all
        publications in AnnualReport instance.

        Limitations:
            This method will fail if report has more than 999 publications
        """
        doi_list = [
            self.epmc_client.escape_brackets(x)
            for x in self.df.doi.loc[self.df.doi.notna()]
        ]
        pmc_id_list = self.df.pmc_id.loc[self.df.pmc_id.notna()]
        pubmed_id_list = self.df.pubmed_id.loc[self.df.pubmed_id.notna()]
        doi_subquery = f"doi:({' OR '.join(doi_list)})"
        pmc_id_subquery = f"pmcid:({' OR '.join(pmc_id_list)})"
        pm_id_subquery = f"ext_id:({' OR '.join(pubmed_id_list)})"

        query = f"({pmc_id_subquery} OR {pm_id_subquery} OR {doi_subquery})"
        query_len = len([*pmc_id_list, *pubmed_id_list, *doi_list])
        query_limit = 1400
        assert query_limit > query_len, (
            f"This method does not support pagination yet and "
            f"the number of IDs queried ({query_len}) exceeds"
            f"the limit of logical operators in an EPMC API query ({query_limit})."
            f"Also bear in mind that the EPMC API has a pageSize limit of 1000"
            f"results. If you are querying for more than 1000 publications, "
            f"you will need to split that in more than one call to this method"
        )
        r = self.epmc_client.search_post(query=query, pageSize=1000, resultType="core")
        self.logger.info(
            f"EuropePMC API response contains data for {r['hitCount']} publications of the {query_len} queried"
        )
        self.epmc_data = r["resultList"]["result"]

    def parse_europepmc_data(self):
        """
        This method was abandoned. It's just a draft
        to facilitate a future implementation if needed.
        """
        if self.epmc_data is None:
            self.get_europepmc_data()
        for p in self.epmc_data:
            for author in p["authorList"]["author"]:
                affiliation_list = list()

                try:
                    # NEW VERSION of multiple affiliations/author within EuropePMC
                    affiliation_list = [
                        x["affiliation"]
                        for x in author["authorAffiliationDetailsList"][
                            "authorAffiliation"
                        ]
                    ]
                except KeyError:
                    # OLD VERSION of affiliation within EuropePMC (one affiliation/author)
                    try:
                        affiliation_list.append(author["affiliation"])
                    except KeyError:
                        self.logger.info(
                            f"No affiliation info found for author ({author})",
                            extra={"publication": p},
                        )

                for aff in affiliation_list:
                    print(aff)
                    # do_something(aff)
        raise NotImplementedError

    def parse_affiliation(self, affiliation):
        r = self.ror_client.search_affiliation(affiliation)
        best_guess = r["items"][0]
        return {
            "query": affiliation,
            "score": best_guess["score"],
            "matching_type": best_guess["matching_type"],
            "chosen": best_guess["chosen"],
            "institution": best_guess["organization"]["name"],
            "country": best_guess["organization"]["country"]["country_name"],
        }

    def get_openalex_data(self) -> None:
        """
        Queries the OpenAlex API to get data about all publications in
        AnnualReport for which a DOI is available (no routine has been
        implemented yet to handle publications for which we don't know
        the DOI)

        Ingests OpenAlex results by merging returned data with existing
        data in each Publication object. If the ingestion process fails,
        moves Publication from AnnualReport.publications to a separate
        list of problematic publications AnnualReport.problem_pubs
        """
        dois = self.df.doi.loc[self.df.doi.notna()]
        r_gen = self.openalex_client.get_works_by_doi_list(dois=dois)
        for response in r_gen:
            results = response["results"]
            for r in results:
                doi = r["doi"].replace("https://doi.org/", "")
                pub_df = self.df.loc[self.df.doi == doi]
                assert (
                    1 == pub_df.shape[0]
                ), f"DOI in OpenAlex result ({doi}) not found in report dataframe"
                alex_parser = OpenAlexParser(publication=pub_df, openalex_result=r)
                merged_df = alex_parser.parse_result()
                self.df.loc[
                    merged_df.index, merged_df.columns
                ] = merged_df  # https://stackoverflow.com/a/71581385

                # try:
                #     pub.ingest_openalex_data(openalex_result=r)
                # except DataValidationError as err:
                #     pub.validation_error = err
                #     self.problem_pubs.append(pub)
                #     self.df.remove(pub)

    def output_csvs(self):
        """
        Outputs csv files for publications parsed successfully and
        for those where parsing raised exceptions.

        Columns in csv files are all attributes of Publication.
        """
        fieldnames = [f.name for f in fields(Publication)]
        with open("annual_report.csv", "w") as rf:
            writer = csv.DictWriter(rf, fieldnames=fieldnames)
            writer.writeheader()
            for pub in self.df:
                writer.writerow(pub.__dict__)
        with open("annual_report_issues.csv", "w") as rf:
            writer = csv.DictWriter(rf, fieldnames=fieldnames)
            writer.writeheader()
            for pub in self.problem_pubs:
                writer.writerow(pub.__dict__)

    def output_csvs_v2(self):
        """
        Same as output_csvs but columns in the csv of publications parsed
        successfully comply to the format required by strategy.
        """

        def get_country_columns(country_code):
            c = countries.get(alpha_2=country_code)
            return [
                c.name,
                f"{c.name} - Institute",
            ]

        country_fieldnames = list()
        for x in member_state_codes:
            country_fieldnames += get_country_columns(x)

        fieldnames = [
            "DOI",
            "PubMed ID",
            "Date (month)",
            "Date (year)",
            "EMBL author(s) name",
            "Paper title",
            "Full journal name",
            "License",
            "In EPMC?",
            "Short reference",
            "Keywords",
            "Collaboration with member state",
            "Collaboration with non-member state",
            "EMBL only publication",
            "Clickable link to the paper",
            *country_fieldnames,
            "Total Member State Countries",
            "Total Member state institutes",
            "Abstract",
        ]
        with open("annual_report.csv", "w") as rf:
            writer = csv.DictWriter(rf, fieldnames=fieldnames)
            writer.writeheader()
            for pub in self.df:
                writer.writerow(self.pub2row(pub))

        with open("annual_report_issues.csv", "w") as rf:
            fieldnames = [f.name for f in fields(Publication)]
            writer = csv.DictWriter(rf, fieldnames=fieldnames)
            writer.writeheader()
            for pub in self.problem_pubs:
                writer.writerow(pub.__dict__)

    def pub2row(self, pub: Publication) -> dict[str, Any]:
        """
        Converts Publication attributes to column names required
        by the annual report standard format

        Returns:
             Csv row in a dictionary format (for consumption by csv.DictWriter)
        """

        def get_country_data(country_code):
            c = countries.get(alpha_2=country_code)
            return {
                c.name: pub.colab_count(country_code),
                f"{c.name} - Institute": pub.member_state_collaborations.get(
                    country_code, ""
                ),
            }

        countries_dict = dict()
        for x in member_state_codes:
            country_dict = get_country_data(x)
            countries_dict.update(country_dict)

        row = {
            "DOI": pub.doi,
            "PubMed ID": pub.pubmed_id,
            "Date (month)": pub.month,
            "Date (year)": pub.year,
            "EMBL author(s) name": pub.embl_authors,
            "Paper title": pub.title,
            "Full journal name": pub.journal,
            "License": pub.licence,
            "In EPMC?": pub.in_epmc,
            "Short reference": pub.short_reference,
            "Keywords": pub.keywords,
            "Collaboration with member state": pub.has_member_state_collaboration,
            "Collaboration with non-member state": pub.has_other_collaboration,
            "EMBL only publication": pub.embl_only,
            "Clickable link to the paper": pub.epmc_link,
            **countries_dict,
            "Total Member State Countries": None,
            "Total Member state institutes": None,
            "Abstract": None,
        }
        return row

    def get_publication_by_doi(self, doi: str) -> Optional[Publication]:
        """
        Returns the publication that matches doi if one exists.
        This method is used to facilitate testing.

        Args:
            doi: The doi of the publication we are interested in
        """
        for p in self.df:
            if p.doi == doi.lower():
                return p
