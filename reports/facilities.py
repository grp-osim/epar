import numpy as np
import pandas as pd
from dataclasses import dataclass
from osim_utils.clients.openalex import OpenAlexClient
from typing import Optional

import local.config as conf
from export_parser.export_parser import ExportParser
from openalex.openalex_parser import OpenAlexParser
from reports.base import BaseReport
from utils.data_container import DataContainer
from utils.data_merger import DataMerger
from utils.fieldset import FieldNames, StandardNames as sn
from wos.wos_parser import WosExportParser


@dataclass
class AnnualReportFieldNames(FieldNames):
    australia: Optional[str] = None
    australia_inst: Optional[str] = None
    austria: Optional[str] = None
    austria_inst: Optional[str] = None
    belgium: Optional[str] = None
    belgium_inst: Optional[str] = None
    croatia: Optional[str] = None
    croatia_inst: Optional[str] = None
    czech_republic: Optional[str] = None
    czech_republic_inst: Optional[str] = None
    denmark: Optional[str] = None
    denmark_inst: Optional[str] = None
    estonia: Optional[str] = None
    estonia_inst: Optional[str] = None
    finland: Optional[str] = None
    finland_inst: Optional[str] = None
    france: Optional[str] = None
    france_inst: Optional[str] = None
    germany: Optional[str] = None
    germany_inst: Optional[str] = None
    greece: Optional[str] = None
    greece_inst: Optional[str] = None
    hungary: Optional[str] = None
    hungary_inst: Optional[str] = None
    iceland: Optional[str] = None
    iceland_inst: Optional[str] = None
    ireland: Optional[str] = None
    ireland_inst: Optional[str] = None
    israel: Optional[str] = None
    israel_inst: Optional[str] = None
    italy: Optional[str] = None
    italy_inst: Optional[str] = None
    latvia: Optional[str] = None
    latvia_inst: Optional[str] = None
    lithuania: Optional[str] = None
    lithuania_inst: Optional[str] = None
    luxembourg: Optional[str] = None
    luxembourg_inst: Optional[str] = None
    malta: Optional[str] = None
    malta_inst: Optional[str] = None
    montenegro: Optional[str] = None
    montenegro_inst: Optional[str] = None
    netherlands: Optional[str] = None
    netherlands_inst: Optional[str] = None
    norway: Optional[str] = None
    norway_inst: Optional[str] = None
    poland: Optional[str] = None
    poland_inst: Optional[str] = None
    portugal: Optional[str] = None
    portugal_inst: Optional[str] = None
    slovakia: Optional[str] = None
    slovakia_inst: Optional[str] = None
    spain: Optional[str] = None
    spain_inst: Optional[str] = None
    sweden: Optional[str] = None
    sweden_inst: Optional[str] = None
    switzerland: Optional[str] = None
    switzerland_inst: Optional[str] = None
    united_kingdom: Optional[str] = None
    united_kingdom_inst: Optional[str] = None
    total_member_states: Optional[str] = None
    total_member_states_institutes: Optional[str] = None


annual_report_fieldnames = AnnualReportFieldNames(
    doi="DOI",
    has_member_state_collaboration="Collaboration with member state",
    has_non_member_state_collaboration="Collaboration with non-member state",
    australia="Australia",
    australia_inst="Institute",
    austria="Austria",
    austria_inst="Institute.1",
    belgium="Belgium",
    belgium_inst="Institute.2",
    croatia="Croatia",
    croatia_inst="Institute.3",
    czech_republic="Czech Republic",
    czech_republic_inst="Institute.4",
    denmark="Denmark",
    denmark_inst="Institute.5",
    estonia="Estonia",
    estonia_inst="Institute.6",
    finland="Finland",
    finland_inst="Institute.7",
    france="France",
    france_inst="Institute.8",
    germany="Germany",
    germany_inst="Institute.9",
    greece="Greece",
    greece_inst="Institute.10",
    hungary="Hungary",
    hungary_inst="Institute.11",
    iceland="Iceland",
    iceland_inst="Institute.12",
    ireland="Ireland",
    ireland_inst="Institute.13",
    israel="Israel",
    israel_inst="Institute.14",
    italy="Italy",
    italy_inst="Institute.15",
    latvia="Latvia",
    latvia_inst="Institute.16",
    lithuania="Lithuania",
    lithuania_inst="Institute.17",
    luxembourg="Luxembourg",
    luxembourg_inst="Institute.18",
    malta="Malta",
    malta_inst="Institute.19",
    montenegro="Montenegro",
    montenegro_inst="Institute.20",
    netherlands="Netherlands",
    netherlands_inst="Institute.21",
    norway="Norway",
    norway_inst="Institute.22",
    poland="Poland",
    poland_inst="Institute.23",
    portugal="Portugal",
    portugal_inst="Institute.24",
    slovakia="Slovakia",
    slovakia_inst="Institute.25",
    spain="Spain",
    spain_inst="Institute.26",
    sweden="Sweden",
    sweden_inst="Institute.27",
    switzerland="Switzerland",
    switzerland_inst="Institute.28",
    united_kingdom="United Kingdom",
    united_kingdom_inst="Institute.29",
    total_member_states="Total Member State Countries",
    total_member_states_institutes="Total Member state institutes",
)

member_states = {
    "AU": "Australia",
    "AT": "Austria",
    "BE": "Belgium",
    "HR": "Croatia",
    "CZ": "Czech Republic",
    "DK": "Denmark",
    "EE": "Estonia",
    "FI": "Finland",
    "FR": "France",
    "DE": "Germany",
    "GR": "Greece",
    "HU": "Hungary",
    "IS": "Iceland",
    "IE": "Ireland",
    "IL": "Israel",
    "IT": "Italy",
    "LV": "Latvia",
    "LT": "Lithuania",
    "LU": "Luxembourg",
    "MT": "Malta",
    "ME": "Montenegro",
    "NL": "Netherlands",
    "NO": "Norway",
    "PL": "Poland",
    "PT": "Portugal",
    "SK": "Slovakia",
    "ES": "Spain",
    "SE": "Sweden",
    "CH": "Switzerland",
    "GB": "United Kingdom",
}

institutes_posfix = " Institutes"
member_state_column_name = "Collaboration with member state"
non_member_state_column_name = "Collaboration with non-member state"
total_member_state_column_name = "Total Member State Countries"
total_member_institutes_column_name = "Total Member state institutes"


def clickable_doi_link_in_excel(doi):
    url = f"https://doi.org/{doi}"
    return f'=HYPERLINK("{url}", "{url}")'


class AnnualReportParser(ExportParser):
    def __init__(self, **kwargs):
        input_file_path = kwargs.get("input_file_path")
        if input_file_path is None:
            input_file_path = conf.annual_publications_report
        super().__init__(input_file_path, annual_report_fieldnames, "_annual_report")
        self.df[sn.embl_collaborations] = self.df.apply(self.collaborations_to_dict, axis=1)

    @staticmethod
    def collaborations_to_dict(row: pd.Series) -> dict:
        """
        Translates collaboration columns to a dictionary of collaborations in the
        format {country_code: [institute_1, institute_2]}
        """
        def name2column(n):
            return n.lower().replace(" ", "_")
        collaborations_dict = dict()
        for code, name in member_states.items():
            has_collab = row[name2column(name)]
            if has_collab:
                try:
                    institutes = row[name2column(name) + "_inst"].split("; ")
                except AttributeError:
                    institutes = list()
                collaborations_dict[code] = institutes
        return collaborations_dict


class FacilitiesReport(BaseReport, DataContainer):
    """
    This report class parses a list of DOIs representing publications acknowledging
    EMBL facilities. It attempts to obtain author affiliation data from several
    sources in order to compute collaborations
    """

    def __init__(self, **kwargs):
        BaseReport.__init__(self, **kwargs)
        self.doi_fieldname = kwargs.get("doi_fieldname", "DOI")
        self.input_file_path = kwargs.get("input_file_path")
        if self.input_file_path is None:
            self.input_file_path = conf.facilities_xlsx
        self.df = pd.read_excel(self.input_file_path)
        name_map = {self.doi_fieldname: sn.doi}
        self.df.rename(columns=name_map, inplace=True)
        self.openalex_client = OpenAlexClient()
        self.oap = None
        DataContainer.__init__(self, df=self.df, data_label="_facilities", **kwargs)
        self.df[sn.doi] = self.df[sn.doi].apply(self.check_doi)
        wos_input_file = kwargs.get("wos_input_file")
        self.wep = WosExportParser(input_file_path=wos_input_file)
        self.sources = kwargs.pop(
            "sources",
            [
                "AnnualReport",
                "OpenAlex",
                "WoS",
            ],
        )
        self.data_merger = None
        self.arp = None

    def query_openalex_publications(self, **kwargs) -> OpenAlexParser:
        r = self.openalex_client.get_works_by_doi_list(list(self.df[sn.doi]))
        self.oap = OpenAlexParser(r, orcid_export_parser=self.orcid_export_parser)
        self.oap.parse_df_data()

    def parse_wos_data(self):
        self.wep.check_for_duplicates()
        self.wep.parse_df_data()

    def output_dois_for_wos_search(self):
        """
        Prints list of DOIs in self.df separated by a white space
        for use in a Web of Science search
        """
        dois = list(self.df[sn.doi])
        print(
            f"Here are the {len(dois)} DOIs in the facilities report input file, separated by a whitespace:"
        )
        print(" ".join(dois))

    def match_embl_publication(self, row: pd.Series) -> tuple:
        if isinstance(row[sn.doi + self.arp.data_label], str):
            return True, "Annual report identifies this as an EMBL paper"

        alex_embl_pub = row[sn.embl_pub + self.oap.data_label]
        wos_embl_pub = row[sn.embl_pub + self.wep.data_label]

        if isinstance(alex_embl_pub, bool) and isinstance(wos_embl_pub, bool):
            if alex_embl_pub == wos_embl_pub:
                match_note = (
                    "OpenAlex and WoS agree on whether or not this is an EMBL paper"
                )
                return alex_embl_pub, match_note
            else:
                match_note = (
                    "OpenAlex and WoS disagree on whether or not this is an EMBL paper; "
                    "OpenAlex result was used"
                )
                return alex_embl_pub, match_note
        elif isinstance(alex_embl_pub, bool):
            match_note = "OpenAlex data used because WoS data not available"
            return alex_embl_pub, match_note
        elif isinstance(wos_embl_pub, bool):
            match_note = "WoS data used because OpenAlex data not available"
            return wos_embl_pub, match_note
        else:
            return np.nan, np.nan

    def match_embl_collaborations(self, row: pd.Series) -> tuple:
        def countries_match(collab_1: dict, collab_2: dict) -> bool:
            if sorted(collab_1.keys()) == sorted(collab_2.keys()):
                return True
            return False

        if isinstance(row[sn.doi + self.arp.data_label], str):
            # use annual report collaborations
            return row[sn.embl_collaborations + self.arp.data_label], "Collaboration data taken from annual report"

        alex_collab = row[sn.embl_collaborations + self.oap.data_label]
        alex_collab_qual = row[sn.embl_collaborations_quality + self.oap.data_label]

        wos_collab = row[sn.embl_collaborations + self.wep.data_label]
        wos_collab_qual = row[sn.embl_collaborations_quality + self.wep.data_label]

        if isinstance(alex_collab, dict) and isinstance(wos_collab, dict):
            if countries_match(alex_collab, wos_collab):
                match_note = (
                    "Collaboration countries match in OpenAlex and WoS datasets; "
                    "OpenAlex collaboration data used"
                )
                return alex_collab, match_note
            else:
                match_note = (
                    "Collaboration countries mismatch between OpenAlex and WoS datasets"
                )
                if alex_collab_qual == "high" and wos_collab_qual == "high":
                    if len(alex_collab.keys()) > len(wos_collab.keys()):
                        match_note += (
                            "; OpenAlex data used because it lists more collaborations"
                        )
                        return alex_collab, match_note
                    elif len(wos_collab.keys()) > len(alex_collab.keys()):
                        match_note += (
                            "; WoS data used because it lists more collaborations"
                        )
                        return wos_collab, match_note
                    else:
                        match_note += (
                            f"; OpenAlex data used. WoS data suggests the following alternative "
                            f"collaboration countries: {wos_collab.keys()}"
                        )
                        return alex_collab, match_note
                elif alex_collab_qual == "high":
                    match_note += (
                        "; OpenAlex data used because it is marked as high quality"
                    )
                    return alex_collab, match_note
                elif wos_collab_qual == "high":
                    match_note += "; WoS data used because it is marked as high quality"
                    return wos_collab, match_note
                else:
                    match_note += (
                        f"; OpenAlex data used. WoS data suggests the following alternative "
                        f"collaboration countries: {wos_collab.keys()}"
                    )
                    return alex_collab, match_note
        elif isinstance(alex_collab, dict):
            return (
                alex_collab,
                "OpenAlex collaboration data used because WoS data not available",
            )
        elif isinstance(wos_collab, dict):
            return (
                wos_collab,
                "WoS collaboration data used because OpenAlex data not available",
            )
        else:
            return np.nan, np.nan

    def get_data_merger(self):
        data_containers = list()
        while self.sources:
            pref_source = self.sources.pop(0)
            if pref_source == "OpenAlex":
                self.query_openalex_publications()
                pref_data_container = self.oap
            elif pref_source == "WoS":
                self.parse_wos_data()
                pref_data_container = self.wep
            elif pref_source == "AnnualReport":
                self.arp = AnnualReportParser()
                pref_data_container = self.arp
            else:
                raise NotImplementedError(
                    f"Data source '{pref_source}' is not supported"
                )
            data_containers.append(pref_data_container)
            # if len(pref_data_container.df) == len(self.df):
            #     # no need to use any other data source
            #     break

        data_containers.append(self)
        self.data_merger = DataMerger(
            data_containers=data_containers, on=sn.doi, strict=False
        )
        self.data_merger.merge_data()
        return self.data_merger

    def resolve_collaborations(self):
        self.data_merger.df[
            [sn.embl_collaborations, sn.embl_collaborations_quality_note]
        ] = self.data_merger.df.apply(
            self.match_embl_collaborations, axis=1, result_type="expand"
        )

    def resolve_is_embl(self):
        self.data_merger.df[
            [sn.embl_pub, sn.embl_pub_note]
        ] = self.data_merger.df.apply(
            self.match_embl_publication, axis=1, result_type="expand"
        )

    def unpack_collaborations(self, row):
        """
        Unpacks the collaborations dictionary into the separate country columns
        preferred by strategy.
        """
        # set default values
        row[member_state_column_name] = False
        row[non_member_state_column_name] = False
        for country_name in member_states.values():
            row[country_name] = False
            row[f"{country_name}{institutes_posfix}"] = np.nan

        if isinstance(row[sn.doi + self.arp.data_label], str):
            # use annual report non_member_state column
            if row["has_non_member_state_collaboration_annual_report"]:
                row[non_member_state_column_name] = True

        collab_dict = row[sn.embl_collaborations]
        member_state_counter = 0
        member_institute_counter = 0
        if isinstance(collab_dict, dict):
            for k, v in collab_dict.items():
                try:
                    country_name = member_states[k]
                except KeyError:
                    row[non_member_state_column_name] = True
                else:
                    row[member_state_column_name] = True
                    member_state_counter += 1
                    member_institute_counter += len(v)
                    row[country_name] = True
                    row[f"{country_name}{institutes_posfix}"] = "; ".join(v)

        row[total_member_state_column_name] = member_state_counter
        row[total_member_institutes_column_name] = member_institute_counter

        return row

    def output_report(self, output_file_path=None):
        if output_file_path is None:
            output_file_path = conf.facilities_report
        self.data_merger.df["Clickable link"] = self.data_merger.df[sn.doi].apply(
            clickable_doi_link_in_excel
        )
        member_state_columns = list()
        for v in member_states.values():
            member_state_columns.append(v)
            member_state_columns.append(f"{v}{institutes_posfix}")
        report_columns = [
            *[f"{x}{self.data_label}" for x in self.df.columns],
            "title_alex",
            "Clickable link",
            member_state_column_name,
            non_member_state_column_name,
            *member_state_columns,
            total_member_state_column_name,
            total_member_institutes_column_name,
            sn.embl_pub,
            sn.embl_pub_note,
            sn.embl_collaborations,
            sn.embl_collaborations_quality_note,
        ]

        with pd.ExcelWriter(output_file_path, engine="xlsxwriter") as writer:
            self.data_merger.df.to_excel(
                writer, sheet_name="Curation", columns=report_columns, index=False
            )
            self.data_merger.df.to_excel(writer, sheet_name="Details", index=False)

    def main(self):
        self.get_data_merger()
        # prune merged df to retain only rows present in facilities dataset
        self.data_merger.df = self.data_merger.df[pd.notnull(self.data_merger.df[sn.doi + self.data_label])]
        self.resolve_collaborations()
        self.resolve_is_embl()
        self.data_merger.df = self.data_merger.df.apply(
            self.unpack_collaborations, axis=1
        )
        self.output_report()


if __name__ == "__main__":
    fr = FacilitiesReport()
    # fr.df.to_excel(conf.facilities_report, index=False)
    # fr.parse_wos_data()
    # fr.output_dois_for_wos_search()
    fr.main()
