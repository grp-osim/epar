from orcid.parkin_parser import OrcidExportParser


class BaseReport:
    """
    Represents a report
    """

    def __init__(self, **kwargs):
        self.start_date = kwargs.get("start_date")
        self.end_date = kwargs.get("end_date")
        self.report_year = kwargs.get("report_year")
        self.orcid_input_file = kwargs.get("orcid_raw_input_file", None)
        self.orcid_pubs_file_path = kwargs.get("orcid_pubs_file_path", None)
        self.orcid_export_parser = kwargs.get("orcid_export_parser", None)

    def get_orcid_publications(self):
        self.orcid_export_parser = OrcidExportParser(
            raw_file_path=self.orcid_input_file,
            year=self.report_year,
            pubs_file_path=self.orcid_pubs_file_path,
        )
        return self.orcid_export_parser
