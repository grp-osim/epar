from __future__ import annotations
from orcid.parkin_parser import OrcidExportParser
from reports.base import BaseReport
from utils.fieldset import StandardNames


class AuthorsReport(BaseReport):
    def __init__(self, authors: list[dict[str:str]], **kwargs):
        super().__init__()
        self.authors = authors
        self.oep = OrcidExportParser(
            raw_file_path=kwargs.pop("orcid_raw_input_file", None), **kwargs
        )

    def get_authors_orcids(self):
        for author in self.authors:
            if not author.get(StandardNames.orcid):
                author[StandardNames.orcid] = self.oep.get_orcid(**author)
