import json
import os.path
import re

import numpy as np
import pandas as pd
from osim_utils.clients.ror import RorClient
from typing import Optional

import local.config as conf
from oaswitchboard.oaswitchboard_fields import (
    oaswitchboard_fieldnames,
    oaswitchboard_author_fieldnames,
    oaswitchboard_institution_fieldnames,
)
from export_parser.export_parser import ExportParser
from utils.countries import CountryDetector
from utils.fieldset import StandardNames as sn


class OaswitchboardExportParser(ExportParser):
    def __init__(self, input_file_path: Optional[str] = None, **kwargs):
        """
        Initialises the parser by reading a JSON export from Oaswitchboard.
        If input_file_path is not passed as an argument,
        an input path must be specified in a
        variable (oaswitchboard_json) in config.py

        Args:
            input_file_path: path to the Oaswitchboard export file. If not passed,
                this is loaded from config.py
            kwargs:
                orcid_export_parser: instance of OrcidExportParser to be used

        """
        self.fn = oaswitchboard_fieldnames
        self.data_label = "_oaswitchboard"
        self.ror_client = RorClient()
        self.country_detector = CountryDetector()

        if input_file_path is None:
            input_file_path = conf.oaswitchboard_json

        with open(input_file_path) as f:
            # remove trailing comma to ensure valid JSON
            json_data = re.sub(r"},\n?]\n?$", "}]", f.read())

        df = pd.json_normalize(json.loads(json_data))
        df.to_csv(os.path.join(conf.debug_folder, "oaswitchboard_parser_debug_0.csv"))
        names_map, provenance_dict, d_kwargs = self.get_names_map(**kwargs)
        df = df[d_kwargs["usecols"]]
        df = df.rename(columns=names_map)
        df[sn.provenance] = json.dumps(provenance_dict)
        df.to_csv(os.path.join(conf.debug_folder, "oaswitchboard_parser_debug_1.csv"))

        super(ExportParser, self).__init__(df, self.data_label, **kwargs)

    def parse_institutions(self, row: pd.Series) -> pd.DataFrame:
        """
        Translates institutions (affiliation) data from each
        author dataframe row into a dataframe of institutions

        Args:
            row: a row of the author dataframe (i.e. an author)
        """
        inst_df = self.rename_df_columns(
            fieldnames=oaswitchboard_institution_fieldnames, df=row[sn.institutions]
        )
        return inst_df

    def parse_authors(self, row: pd.Series) -> pd.DataFrame:
        """
        Translates authorship data from each dataframe row
        into a dataframe of authors

        Args:
            row: a row of the dataframe (i.e. a switchboard message)
        """
        authors_df = pd.json_normalize(row[sn.authors])
        names_map = oaswitchboard_author_fieldnames.names_map
        authors_df = authors_df.rename(columns=names_map)

        names_map = self.append_names_map_with_calculated_author_attributes(names_map)
        authors_df = self.set_default_values_of_calculated_fields_in_author_dataframe(
            authors_df
        )

        # parse institutions data
        normalize_if_not_null = (
            lambda x: pd.json_normalize(x) if json.dumps(x) != "NaN" else pd.DataFrame()
        )
        authors_df.institutions = authors_df.institutions.apply(normalize_if_not_null)
        authors_df.institutions = authors_df.apply(self.parse_institutions, axis=1)
        authors_df = self.detect_embl_authors_and_institution_countries(authors_df)

        # return only columns of interest
        try:
            return authors_df[names_map.values()]
        except KeyError:
            return authors_df

    def is_embl_publication(self, row: pd.Series) -> tuple[bool, str, int]:
        embl_authors_df = row.authors[row.authors.embl_author == True]
        if embl_authors_df.empty:
            return False, np.nan, np.nan
        else:
            # returning the source and confidence of the first identified EMBL author for simplicity;
            # it would be better to return data from the entry with the highest confidence score
            return (
                True,
                embl_authors_df.iloc[0][sn.source_embl_author],
                embl_authors_df.iloc[0][sn.confidence_embl_author],
            )

    def parse_df_data(self):
        if sn.authors in self.df.columns:
            self.df.authors = self.df.apply(self.parse_authors, axis=1)
        if sn.doi in self.df.columns:
            self.df.doi = self.df.doi.apply(self.convert_doi_url)
        self.df[sn.embl_pub] = self.df.apply(self.is_embl_publication, axis=1)
        self.df[
            [
                sn.embl_collaborations,
                sn.embl_collaborations_quality,
                sn.embl_collaborations_quality_note,
            ]
        ] = self.df.apply(self.get_collaborations, axis=1, result_type="expand")
