from utils.fieldset import FieldNames


oaswitchboard_fieldnames = FieldNames(
    acceptance_date="data.article.manuscript.dates.acceptance",
    authors="data.authors",
    doi="data.article.doi",
    issn="data.journal.id",
    journal_name="data.journal.name",
    journal_oa_status="data.article.vor.publication",
    licence="data.article.vor.license",
    preprint_url="data.article.preprint.url",
    publication_date="data.article.manuscript.dates.publication",
    publication_type="data.article.type",
    submission_date="data.article.manuscript.dates.submission",
    title="data.article.title",
)

oaswitchboard_author_fieldnames = FieldNames(
    affiliation="affiliation",
    authorship_position="listingorder",
    first_name="firstName",
    institutions="institutions",
    is_corresponding="isCorrespondingAuthor",
    last_name="lastName",
    orcid="ORCID",
)

oaswitchboard_institution_fieldnames = FieldNames(
    name="name",
    ror_id="ror",
)
