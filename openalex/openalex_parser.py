from __future__ import annotations
from difflib import SequenceMatcher
from typing import Any, Callable, Optional, Union, TYPE_CHECKING

import dateutil
import pandas as pd
from dataclasses import fields
from osim_utils.logger import get_logger
from pycountry import countries

from openalex.openalex_fields import (
    openalex_fieldnames,
    openalex_author_fieldnames,
    openalex_institution_fieldnames,
)
from reports.constants import member_states
from ror.embl_ids import ror_id2site
from utils.data_container import DataContainer
from utils.exceptions import DataValidationError
from utils.fieldset import StandardNames as sn
from utils.validators import are_strings_similar

if TYPE_CHECKING:
    from reports.entities import Author


class OpenAlexParser(DataContainer):
    """
    Data container for OpenAlex API results
    """

    def __init__(self, openalex_results: Union[dict[str, Any], pd.DataFrame], **kwargs):
        self.fn = openalex_fieldnames
        if isinstance(openalex_results, pd.DataFrame):
            df = openalex_results
        else:
            df = pd.json_normalize(openalex_results["results"], max_level=1)
        super().__init__(df, "_alex", **kwargs)
        self.df = self.rename_df_columns(fieldnames=self.fn)
        fieldnames = [f.name for f in fields(self.fn)]
        drop_list = [x for x in self.df.columns if x not in fieldnames]
        self.df.drop(columns=drop_list, inplace=True)

    def parse_institutions(self, row: pd.Series) -> pd.DataFrame:
        """
        Translates institutions (affiliation) data from each
        author dataframe row into a dataframe of institutions

        Args:
            row: a row of the author dataframe (i.e. an author)
        """
        inst_df = self.rename_df_columns(
            fieldnames=openalex_institution_fieldnames,
            df=pd.json_normalize(row[sn.institutions], max_level=1),
        )
        return inst_df

    def parse_authors(self, row: pd.Series) -> pd.DataFrame:
        """
        Translates authorship data from each dataframe row
        into a dataframe of authors

        Args:
            row: a row of the dataframe (i.e. a publication)
        """
        authors_df = self.rename_df_columns(
            fieldnames=openalex_author_fieldnames,
            df=pd.json_normalize(row[sn.authors], max_level=1),
        )
        authors_df = self.set_default_values_of_calculated_fields_in_author_dataframe(
            authors_df
        )

        # parse institutions data
        authors_df.institutions = authors_df.apply(self.parse_institutions, axis=1)
        authors_df = self.detect_embl_authors_and_institution_countries(authors_df)

        return authors_df

    def parse_df_data(self):
        if sn.authors in self.df.columns:
            self.df.authors = self.df.apply(self.parse_authors, axis=1)
        if sn.doi in self.df.columns:
            self.df.doi = self.df.doi.apply(self.convert_doi_url)
        self.df[
            [
                sn.embl_collaborations,
                sn.embl_collaborations_quality,
                sn.embl_collaborations_quality_note,
            ]
        ] = self.df.apply(self.get_collaborations, axis=1, result_type="expand")
        self.df[[sn.embl_pub, sn.embl_pub_confidence]] = self.df.apply(
            self.is_embl_publication, axis=1, result_type="expand"
        )

