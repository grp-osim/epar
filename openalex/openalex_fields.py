from utils.fieldset import FieldNames


openalex_fieldnames = FieldNames(
    authors="authorships",
    doi="doi",
    issn="host_venue.issn",
    journal_name="host_venue.display_name",
    licence="host_venue.license",
    open_alex_id="id",
    pubmed_id="ids.pmid",
    publication_date="publication_date",
    publication_type="type",
    title="title",
    year="publication_year",
)


openalex_author_fieldnames = FieldNames(
    affiliation="raw_affiliation_string",
    authorship_position="author_position",
    institutions="institutions",
    name="author.display_name",
    open_alex_id="author.id",
    orcid="author.orcid",
)


openalex_institution_fieldnames = FieldNames(
    country_code="country_code",
    name="display_name",
    open_alex_id="id",
    ror_id="ror",
)
