import json
import numpy as np
import os.path
import pandas as pd
from abc import ABCMeta
from osim_utils.logger import get_logger
from typing import Union

from utils.data_container import DataContainer
from utils.fieldset import FieldNames, StandardNames


class ExportParser(DataContainer, metaclass=ABCMeta):
    """
    Abstract spreadsheet parser class with common methods
    """

    def __init__(
        self, input_file_path: str, fieldnames: FieldNames, data_label: str, /, **kwargs
    ):
        """
        Initialises the parser by reading an export file
        (csv, Excel)

        Args:
            input_file_path: path to the export file.
            fieldnames: mapping standardising export column names
            data_label: tag indicating data provenance (e.g. _converis)
            kwargs: keyword arguments for load_dataframe method

        Returns: Pandas dataframe
        """
        self.input_file_path = input_file_path
        self.fn = fieldnames
        self.data_label = data_label
        super().__init__(df=self.load_dataframe(**kwargs), data_label=data_label)
        self.output_filepath = f"{os.path.splitext(self.input_file_path)[0]}_parsed.csv"
        self.logger = get_logger()

    def get_names_map(self, **kwargs) -> tuple[dict, dict, dict]:
        usecols = kwargs.pop("usecols", list())
        default_usecols = True
        if usecols:
            default_usecols = False
        names_map = {}
        provenance_dict = {}

        for k, v in self.fn.__dict__.items():
            if v:
                if default_usecols is True:
                    usecols.append(v)
                names_map[v] = k
                provenance_dict[k] = f"{self.data_label[1:]} - {v}"

        d_kwargs = {
            "usecols": usecols,
        }
        if dtype_dict := kwargs.pop("dtype_dict", None):
            d_kwargs.update(dtype=dtype_dict)
        return names_map, provenance_dict, d_kwargs

    def load_dataframe(self, **kwargs) -> pd.DataFrame:
        """
        Reads an export file. Creates and returns a Pandas dataframe.

        Args:
            kwargs:
                dtype_dict: dictionary specifying data types for fields in self.fn
                    (Example: {self.fn.pubmed_id: str, self.fn.year: str})
                usecols: columns to read from input file. Defaults to values in self.fn.__dict__.items()
                additional keyword arguments: keyword arguments for pandas.read_csv method

        Returns: Pandas dataframe
        """
        names_map, provenance_dict, d_kwargs = self.get_names_map(**kwargs)
        kwargs.pop("usecols", None)
        kwargs.pop("dtype_dict", None)

        try:
            d_kwargs.update(kwargs)
            self.df = pd.read_csv(self.input_file_path, **d_kwargs)
        except UnicodeDecodeError:
            self.df = pd.read_excel(self.input_file_path, **d_kwargs)

        self.df = self.df.rename(columns=names_map)
        self.df[StandardNames.provenance] = json.dumps(provenance_dict)
        return self.df

    def sanitise_dataframe(self) -> None:
        """
        Remove all the annoying new line characters from the end of every cell
        (https://stackoverflow.com/a/45270483)

        Note: new line characters at the end of every cell appears to be a problem
            in Converis csv exports, but not in Excel exports. Thus, this method is
            not used at present. If it is still not used when this project is in
            production, it can/should be deleted.

        Returns: None
        """
        self.df = self.df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    def check_pmc_id(self, row: pd.Series) -> Union[str, np.float64]:
        """
        Checks that pmc_id value is valid
        Args:
            row: a row of the dataframe (i.e. a publication)
        """
        if self.validator.pmc_id_valid((pmc_id := row), row):
            return pmc_id
        return np.nan

    def check_pubmed_id(self, row: pd.Series) -> Union[str, np.float64]:
        """
        Checks that pubmed_id value is valid
        Args:
            row: a row of the dataframe (i.e. a publication)
        """
        if self.validator.pubmed_id_valid((pubmed_id := row), row):
            return pubmed_id
        return np.nan

    def output_csv(self) -> None:
        """
        Outputs dataframe to csv file in the same folder as input file.
        At present, this method is only used for testing/debugging
        """
        with open(self.output_filepath, "w") as f:
            self.df.to_csv(f)
