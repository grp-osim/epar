# EPAR

Automates the generation of EMBL publications annual reports

## Installation

This project uses poetry to manage dependencies, so follow the instructions 
at https://python-poetry.org/docs/#installation if poetry is not already 
available in your system.

Then install this project dependencies by running
```
$ poetry install
```

## Configuration

Create a ```local/config.py``` file at the root of this project's directory.
Define a variable ```converis_xlsx``` in that file, which resolves to the
path of the input file you would like to use. For example:

```
import os

home = os.path.expanduser("~")
converis_xlsx = os.path.join(home, "Converis_all_info_2022_11_24.xlsx")
```

## Usage

Simply run
```
$ python main.py
```
The output csv file will be saved in the same directory as the input file.
The name of the output file is that of the input file with the substring 
"_parsed" appended to the end. 
For example, ```Converis_all_info_2022_11_24_parsed.csv```
